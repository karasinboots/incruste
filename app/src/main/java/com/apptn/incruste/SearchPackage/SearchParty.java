package com.apptn.incruste.SearchPackage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.apptn.incruste.CreationNewParty.CustomAutocompletationAdapter;
import com.apptn.incruste.General.FootMenu;
import com.apptn.incruste.General.LocationDetails;
import com.apptn.incruste.GolbalUtilities.Utiles;
import com.apptn.incruste.Models.Party;
import com.apptn.incruste.Models.User;
import com.apptn.incruste.R;
import com.apptn.incruste.SingletonVolley.MySingleton;
import com.apptn.incruste.myPartys.PartyDetails;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.annotation.Nullable;

import static com.apptn.incruste.GolbalUtilities.Utiles.DOCUMENT_PARTY;
import static com.apptn.incruste.GolbalUtilities.Utiles.DOCUMENT_PROFILE;

public class SearchParty extends AppCompatActivity {

    FrameLayout calendarClickToClose, aproxFrameClickToClose, checksClickToClose, openCalendar;
    RelativeLayout searchTable;
    MaterialCalendarView calendar;
    Calendar currentCalender;
    RadioGroup rgTaille, rgPlace;
    SearchAdapterApprox nearSearchAdapter;
    SearchAdapterPop populaireSearchAdapterPop;
    ImageView arrowBack;
    TextView dateee, placeSearch, people, month;
    CustomAutocompletationAdapter customAdapter;
    RecyclerView populaireRecycler, approxRecycle, rcySearch;
    FrameLayout placeIcon;
    LinearLayout triggerSearch, searchProfond, calendarHolder, layoutChecks, tabLinear;
    AutoCompleteTextView editWhere;
    ArrayList<String> events = new ArrayList<>();
    //ArrayList<String> events = new ArrayList();
    JSONArray dates = new JSONArray();
    ArrayList<String> places = new ArrayList<>();
    HashMap<String, Integer> participants = new HashMap<>();


    boolean isCalendarShown = false;
    boolean isSearchShown = false;
    SharedPreferences preferences;
    double lat, lng;
    MySearchAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_party);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        populaireRecycler = findViewById(R.id.populaireRecycler);
        approxRecycle = findViewById(R.id.nearRecycler);
        RecyclerView.LayoutManager managerPop = new GridLayoutManager(this, 3);
        RecyclerView.LayoutManager managerApp = new GridLayoutManager(this, 3);
        approxRecycle.setLayoutManager(managerApp);
        populaireRecycler.setLayoutManager(managerPop);
        populaireSearchAdapterPop = new SearchAdapterPop();
        nearSearchAdapter = new SearchAdapterApprox(this);
        populaireRecycler.setAdapter(populaireSearchAdapterPop);
        approxRecycle.setAdapter(nearSearchAdapter);
        //  emptyImg = findViewById(R.id.emptyImg);
        //  emptyImg.setColorFilter(ContextCompat.getColor(SearchParty.this,R.color.iconsColor));

        init();
    }

    void init() {

        FootMenu footMenu = FootMenu.newInstance(0);
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.searchPartyFooterCon, footMenu);
        ft.commit();
        calendar = findViewById(R.id.calendar);
        month = findViewById(R.id.month);
        Log.i("current date", String.valueOf(Calendar.getInstance().getTime()));
        calendar.setCurrentDate(Calendar.getInstance().getTime());
        // month.setText(Utiles.getDateFromMs(Calendar.getInstance().getTimeInMillis(),"MMMMM"));
        rgPlace = findViewById(R.id.rgPlaceLibre);
        rgTaille = findViewById(R.id.rgTailleSoire);
        rcySearch = findViewById(R.id.rcySearch);
        searchTable = findViewById(R.id.linearHide);
        searchTable.bringToFront();

        LinearLayoutManager manager = new LinearLayoutManager(this);
        rcySearch.setLayoutManager(manager);
        adapter = new MySearchAdapter();
        rcySearch.setAdapter(adapter);

        calendarClickToClose = findViewById(R.id.calendarClickToClose);
        aproxFrameClickToClose = findViewById(R.id.aproxFrameClickToClose);
        checksClickToClose = findViewById(R.id.checksClickToClose);

        calendar.setSelectionMode(MaterialCalendarView.SELECTION_MODE_MULTIPLE);
        //  calendar.setTopbarVisible(false);
        customAdapter = new CustomAutocompletationAdapter(this);
        PreferenceManager.getDefaultSharedPreferences(this);
        calendarClickToClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calendarHolder.setVisibility(View.GONE);
            }
        });
        aproxFrameClickToClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                searchProfond.setVisibility(View.GONE);

            }
        });
        checksClickToClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                layoutChecks.setVisibility(View.GONE);
            }
        });

        rgTaille.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rgTaille.clearCheck();
            }
        });
        rgTaille.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = findViewById(group.getCheckedRadioButtonId());
                participants.remove("size_min");
                participants.remove("size_max");
                if (rb != null)
                    switch (Integer.parseInt(rb.getTag().toString())) {
                        case 1:
                            participants.put("size_min", 5);
                            participants.put("size_max", 9);
                            break;
                        case 2:
                            participants.put("size_min", 10);
                            participants.put("size_max", 14);
                            break;
                        case 3:
                            participants.put("size_min", 15);
                            participants.put("size_max", 999);
                            break;
                    }

                webServiceSearchPartys();
            }
        });

        rgPlace.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = findViewById(checkedId);
                participants.remove("remain_min");
                participants.remove("remain_max");
                if (rb != null)
                    switch (Integer.parseInt(rb.getTag().toString())) {
                        case 1:
                            participants.put("remain_min", 1);
                            participants.put("remain_max", 5);
                            break;
                        case 2:
                            participants.put("remain_min", 6);
                            participants.put("remain_max", 10);
                            break;
                        case 3:
                            participants.put("remain_min", 11);
                            participants.put("remain_max", 999);
                            break;
                    }
                webServiceSearchPartys();
            }
        });

        currentCalender = Calendar.getInstance(Locale.getDefault());
        searchProfond = findViewById(R.id.searchProfond);
        searchProfond.setVisibility(View.GONE);
        calendarHolder = findViewById(R.id.calendarHolder);
        layoutChecks = findViewById(R.id.layoutChecks);

        placeIcon = findViewById(R.id.clickToOpen);
        tabLinear = findViewById(R.id.tabLinear);
        arrowBack = findViewById(R.id.arrowBack);
        triggerSearch = findViewById(R.id.triggerSearch);

        dateee = findViewById(R.id.searchTime);

        placeSearch = findViewById(R.id.searchPlace);

        people = findViewById(R.id.searchPeople);

        calendarHolder.setVisibility(View.GONE);

        openCalendar = findViewById(R.id.openCalendar);

        editWhere = findViewById(R.id.editWhere);

        arrowBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchTable.setVisibility(View.GONE);
                tabLinear.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) rcySearch.getLayoutParams();
                params.addRule(RelativeLayout.BELOW, R.id.tabLinear);
                rcySearch.setLayoutParams(params);
            }
        });

        calendarHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        editWhere.setAdapter(customAdapter);
        editWhere.setThreshold(0);

        editWhere.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                webServiceAutoCmplete(s.toString());
                editWhere.showDropDown();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editWhere.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                LocationDetails location = (LocationDetails) parent.getItemAtPosition(position);
                editWhere.setText(location.getVille());
                lng = location.getaLong();
                lat = location.getLat();
                webServiceGetNear(lng, lat);
            }
        });

        calendar.setOnMonthChangedListener(new OnMonthChangedListener() {
            @Override
            public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
                Log.i("dateeeeee", "" + date);
            }
        });

        calendar.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {

                Log.i("dateeeeee", "" + date);
                String date_selected = "" + date.getDay() + "-" + (date.getMonth() + 1) + "-" + date.getYear() + "";

                if (events.contains(date_selected)) {
                    events.remove(date_selected);
                } else {
                    events.add(date_selected);
                }

                webServiceSearchPartys();
                Log.i("selecteddates %@", events.toString());
                if (events.size() > 0) {
                    String ssss = events.get(0).toString();
                    if (events.size() == 1) {
                        dateee.setText(ssss);
                    } else {
                        for (int j = 1; j < events.size(); j++) {
                            ssss = ssss + ", " + events.get(j);
                            dateee.setText(ssss);
                        }
                    }
                } else {
                    dateee.setText("");
                }

            }
        });

        openCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!isCalendarShown) {
                    calendarHolder.setVisibility(View.VISIBLE);
                    calendarHolder.bringToFront();
                    searchProfond.setVisibility(View.GONE);
                    isCalendarShown = true;
                    dateee.setText("");
                } else {
                    calendarHolder.setVisibility(View.GONE);
                    isCalendarShown = false;
                }

                calendarHolder.bringToFront();
            }
        });

        triggerSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                searchProfond.setVisibility(View.GONE);
                calendarHolder.setVisibility(View.GONE);
                tabLinear.setVisibility(View.GONE);
                searchTable.setVisibility(View.VISIBLE);
                searchTable.bringToFront();

            }
        });

        placeIcon.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                if (!isSearchShown) {
                    searchProfond.setVisibility(View.VISIBLE);
                    searchProfond.bringToFront();
                    tabLinear.setVisibility(View.GONE);
                    calendarHolder.setVisibility(View.GONE);
                    isSearchShown = true;

                } else {

                    searchProfond.setVisibility(View.GONE);
                    isSearchShown = false;
                }

            }
        });


        people.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutChecks.setVisibility(View.VISIBLE);
                layoutChecks.bringToFront();
            }
        });


        //   approxRecycle.addItemDecoration(new ItemOffsetDecoration(10));

    }

    public void doNothing(View view) {

    }

    public void webServiceAutoCmplete(final String address) {
        String addressURL = "";
        try {
            addressURL = "https://www.waze.com/SearchServer/mozi?q=" + URLEncoder.encode(address, "UTF-8") + "&lang=fr&lon=2.344465&lat=48.852513&origin=livemap";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        JsonArrayRequest request = new JsonArrayRequest(addressURL, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Utiles.showMessageLog("responseAddress", response.toString());
                customAdapter.RempliresultList(response);
                customAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("errorAddress", error.toString());
            }
        });

        MySingleton.getInstance(this).addToRequestQueue(request);
    }

   /* public void clearPlaces(View view) {

        if (places.size()>0){

            for (int i = 0; i < places.size(); i++) {
                places.remove(i);
            }

            editWhere.setText("");
            placeSearch.setText(getString(R.string.where));
            emptyImg.setVisibility(View.GONE);
            webServiceAutoCmplete("");
            places.remove(0);
            Utiles.showMessageLog("length",""+places.size());
        }

    }*/

    @Override
    public void onBackPressed() {

    }

    private class MySearchAdapter extends RecyclerView.Adapter<MySearchAdapter.MyViewHolder> {

        List<Party> data;

        public MySearchAdapter() {

            this.data = new ArrayList<>();
        }

        void fetchData(List<Party> array) {

            this.data = array;
            notifyDataSetChanged();
        }

        void clear() {
            int size = this.data.size();
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    this.data.remove(0);
                }

                this.notifyItemRangeRemoved(0, size);
            }
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.inflation_my_partys, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, int position) {

            try {
                holder.layoutPersons.bringToFront();
                holder.gradient.bringToFront();
                holder.gradient.setBackground(Utiles.getColor());
                holder.dateParty.bringToFront();
                holder.timeParty.bringToFront();
                holder.layoutPersons.bringToFront();

                Typeface neutraDemi = Typeface.createFromAsset(SearchParty.this.getAssets(), "NeutraText-Demi.otf");
                Typeface Neutraface = Typeface.createFromAsset(SearchParty.this.getAssets(), "Neutraface2Text-Book.otf");
                Typeface proLigh = Typeface.createFromAsset(SearchParty.this.getAssets(), "SourceSansPro-Light.otf");
                Typeface proDemi = Typeface.createFromAsset(SearchParty.this.getAssets(), "SourceSansPro-Semibold.otf");

                holder.by.setTypeface(proLigh);

                String partyName = data.get(position).getName();
                holder.partyName.setTypeface(neutraDemi);
                holder.partyName.setText(partyName.substring(0, 1).toUpperCase() + partyName.substring(1));
                holder.dateParty.setTypeface(Neutraface);
                holder.timeParty.setTypeface(neutraDemi);
                holder.dateParty.setText(Utiles.getTimeAndDate(data.get(position).getDateSoiree()).get(0));
                holder.timeParty.setText(Utiles.getTimeAndDate(data.get(position).getDateSoiree()).get(1));
                holder.partyCreator.setTypeface(proDemi);

                FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
                firebaseFirestore
                        .collection(DOCUMENT_PROFILE)
                        .document(data.get(position).getUser())
                        .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                            @Override
                            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                                User user = documentSnapshot.toObject(User.class);
                                String nameCreator = user.getNom();
                                String lastNameCreator = user.getPrenom();
                                holder.partyCreator.setText(nameCreator.substring(0, 1).toUpperCase() + nameCreator.substring(1) + " " +
                                        lastNameCreator);
                            }
                        });

                String partyLocation = data.get(position).getVille();
                holder.partyLocation.setTypeface(proLigh);
                holder.partyLocation.setText(partyLocation.substring(0, 1).toUpperCase() + partyLocation.substring(1));
                // holder.participant.setText(String.valueOf(data.getJSONObject(position).getJSONArray("incruster").length()));
                holder.participant.setText(data.get(position).getRemain() + "");

                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(SearchParty.this);
                JSONArray ambiances = new JSONArray(sharedPreferences.getString("JsonAmb", ""));
                Glide.with(SearchParty.this).load(ambiances.getJSONObject(Integer.parseInt(data.get(position).getAmbiance()) - 1)
                        .getString("description")).into(holder.ambImg);
                Glide.with(SearchParty.this).load(data
                        .get(position).getPhoto()).into(holder.imgParty);
            } catch (JSONException jsx) {
                jsx.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView partyName, dateParty, timeParty, partyCreator, partyLocation, participant, by;
            ImageView imgParty, ambImg;
            FrameLayout layoutPersons;
            View gradient;

            public MyViewHolder(View itemView) {
                super(itemView);

                by = itemView.findViewById(R.id.by);

                layoutPersons = itemView.findViewById(R.id.layoutPersonsMy);
                participant = itemView.findViewById(R.id.participant);
                ambImg = itemView.findViewById(R.id.ambImg);
                gradient = itemView.findViewById(R.id.gradientMyPartys);
                partyName = itemView.findViewById(R.id.partyName);
                dateParty = itemView.findViewById(R.id.dateParty);
                timeParty = itemView.findViewById(R.id.timeParty);
                partyCreator = itemView.findViewById(R.id.partyCreator);
                partyLocation = itemView.findViewById(R.id.partyLocation);
                imgParty = itemView.findViewById(R.id.imgParty);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(SearchParty.this, PartyDetails.class);
                        intent.putExtra("extraInfo", new Gson().toJson(data.get(getAdapterPosition())));
                        intent.putExtra("cameFrom", "myParty");
                        startActivity(intent);

                    }
                });
            }
        }

    }

    List<Party> finded = new ArrayList<>();

    private void webServiceSearchPartys() {
        FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseFirestore.collection(DOCUMENT_PARTY).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                finded.clear();
                List<Party> allParties = task.getResult().toObjects(Party.class);
                int remainMin;
                int remainMax;
                if (participants.get("remain_min") != null)
                    remainMin = participants.get("remain_min");
                else remainMin = 0;
                if (participants.get("remain_max") != null)
                    remainMax = participants.get("remain_max");
                else remainMax = 999;
                int size_min;
                int size_max;
                if (participants.get("size_min") != null)
                    size_min = participants.get("size_min");
                else size_min = 0;
                if (participants.get("size_max") != null)
                    size_max = participants.get("size_max");
                else size_max = 999;
                if (allParties.size() > 0)
                    for (Party party : allParties) {
                        for (String date : events) {
                            if (places.size() > 0)
                                for (String place : places) {
                                    if (formatDate(party.getDateSoiree()).contains(date)
                                            && party.getVille().toLowerCase().contains(place.toLowerCase())
                                            && party.getRemain() >= remainMin
                                            && party.getRemain() <= remainMax
                                            && party.getNbPlaces() >= size_min
                                            && party.getNbPlaces() <= size_max)
                                        finded.add(party);
                                }
                            else if (formatDate(party.getDateSoiree()).contains(date)
                                    && party.getRemain() >= remainMin
                                    && party.getRemain() <= remainMax
                                    && party.getNbPlaces() >= size_min
                                    && party.getNbPlaces() <= size_max)
                                finded.add(party);
                        }
                    }
                Log.e("finded size", finded.size() + "");
                if (finded.size() > 0) {
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) rcySearch.getLayoutParams();
                    params.addRule(RelativeLayout.BELOW, R.id.linearHide);
                    rcySearch.setLayoutParams(params);
                    adapter.fetchData(finded);
                } else {
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) rcySearch.getLayoutParams();
                    params.addRule(RelativeLayout.BELOW, R.id.tabLinear);
                    rcySearch.setLayoutParams(params);
                    adapter.clear();
                }
            }
        });


    }

    private String formatDate(String date) {
        SimpleDateFormat inputFmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        TimeZone timeZone = TimeZone.getTimeZone("GMT");
        inputFmt.setTimeZone(timeZone);
        String newDate = "";
        try {
            Date date1 = inputFmt.parse(date);
            SimpleDateFormat output = new SimpleDateFormat("dd-M-yyyy");
            newDate = output.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    private class SearchAdapterApprox extends RecyclerView.Adapter<SearchAdapterApprox.MyViewHolder> {

        Context context;

        JSONArray dataRecieved;

        SearchAdapterApprox(Context ctx) {

            this.context = ctx;
            dataRecieved = new JSONArray();
        }

        public void fetchData(JSONArray data) {

            this.dataRecieved = data;
            notifyDataSetChanged();
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.inflation_single_row_search, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {

            try {
                holder.place.setText(dataRecieved.getJSONObject(holder.getAdapterPosition()).getString("name"));

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return dataRecieved.length();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {

            TextView place;
            Boolean state = false;

            MyViewHolder(final View itemView) {
                super(itemView);
                place = itemView.findViewById(R.id.place);
                // emptyImg.setVisibility(View.GONE);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            //   emptyImg.setVisibility(View.VISIBLE);
                            state = !state;
                            customiseCell(itemView, state);

                            if (state && !places.contains(dataRecieved.getJSONObject(getAdapterPosition()).getString("name"))) {
                                places.add("" + dataRecieved.getJSONObject(getAdapterPosition()).getString("name"));
                            } else {
                                places.remove("" + dataRecieved.getJSONObject(getAdapterPosition()).getString("name"));
                            }
                            webServiceSearchPartys();
                            Utiles.showMessageLog("arrayPlaces", "" + places.size());

                            if (places.size() > 0) {
                                String ssss = (String) places.get(0);
                                if (places.size() == 1) {
                                    placeSearch.setText(ssss);
                                } else {
                                    for (int i = 1; i < places.size(); i++) {
                                        ssss = ssss + "," + places.get(i);
                                        placeSearch.setText(ssss);
                                    }
                                }
                            } else {
                                placeSearch.setText("");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }

            @SuppressLint("ResourceAsColor")
            void customiseCell(final View itemView, Boolean state) {

                GradientDrawable stateListDrawable = (GradientDrawable) itemView.getBackground();
                if (state) {

                    stateListDrawable.setColor(Color.WHITE);

                    place.setTextColor(R.color.blue);
                    //  itemView.layer.borderColor = UIColor.customBlue.cgColor

                } else {
                    stateListDrawable.setColor(Color.TRANSPARENT);
                    place.setTextColor(Color.WHITE);
                }
            }

        }

    }

    public class ItemOffsetDecoration extends RecyclerView.ItemDecoration {

        private int mItemOffset;

        public ItemOffsetDecoration(int itemOffset) {
            mItemOffset = itemOffset;
        }

        public ItemOffsetDecoration(@NonNull Context context, @DimenRes int itemOffsetId) {
            this(context.getResources().getDimensionPixelSize(itemOffsetId));
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            outRect.set(mItemOffset, mItemOffset, mItemOffset, mItemOffset);
        }
    }

    private class SearchAdapterPop extends RecyclerView.Adapter<SearchAdapterPop.MyViewHolder> {


        JSONArray dataRecieved;

        SearchAdapterPop() {

            dataRecieved = new JSONArray();
        }

        private void fetchData(JSONArray data) {

            this.dataRecieved = data;
            notifyDataSetChanged();
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.inflation_single_row_search, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {

            try {
                holder.place.setText(dataRecieved.getJSONObject(position).getString("ville"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return dataRecieved.length();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {

            TextView place;
            Boolean state = false;

            private MyViewHolder(final View itemView) {
                super(itemView);
                place = itemView.findViewById(R.id.place);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        // emptyImg.setVisibility(View.VISIBLE);

                        state = !state;
                        customiseCell(itemView, state);

                        try {

                         /*   if selectedPopular.count==0 {
                                selectedPopular.setObject(cell.title.text as Any, forKey: cell.tag as NSCopying)
                            }else if !(selectedPopular.allValues as NSArray).contains(cell.title.text as Any) {
                                selectedPopular.setObject(cell.title.text as Any, forKey: cell.tag as NSCopying)
                            }else if (selectedPopular.allKeys as NSArray).contains(cell.tag){
                                selectedPopular.removeObject(forKey: cell.tag as NSCopying)
                            }else{

                            }
*/
                            if (state && !places.contains(dataRecieved.getJSONObject(getAdapterPosition()).getString("ville"))) {
                                places.add(dataRecieved.getJSONObject(getAdapterPosition()).getString("ville"));
                            } else {
                                places.remove(dataRecieved.getJSONObject(getAdapterPosition()).getString("ville"));
                            }

                            webServiceSearchPartys();
                            Utiles.showMessageLog("arrayPlaces", "" + places.size());

                            if (places.size() > 0) {
                                String ssss = (String) places.get(0);
                                if (places.size() == 1) {
                                    placeSearch.setText(ssss);
                                } else {
                                    for (int i = 1; i < places.size(); i++) {
                                        ssss = ssss + "," + places.get(i);
                                        placeSearch.setText(ssss);
                                    }
                                }

                            } else {
                                placeSearch.setText("");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }

            @SuppressLint("ResourceAsColor")
            void customiseCell(final View itemView, Boolean state) {

                GradientDrawable stateListDrawable = (GradientDrawable) itemView.getBackground();
                if (state) {

                    stateListDrawable.setColor(Color.WHITE);

                    place.setTextColor(R.color.blue);
                    //  itemView.layer.borderColor = UIColor.customBlue.cgColor

                } else {
                    stateListDrawable.setColor(Color.TRANSPARENT);
                    place.setTextColor(Color.WHITE);
                }
            }
        }

        private void webServiceGetPopulaire(JSONArray array) {

          /*  HashMap<String, String> params = new HashMap();
            try {
                JSONObject user = new JSONObject(preferences.getString("user", "{}"));
                params.put("user_id", user.getString("id"));
                params.put("arrayVille", "" + array.toString());


            } catch (JSONException e) {
                e.printStackTrace();
            }
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Utiles.GLOBAL_LINK_URL + "getPopulaire", new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(final JSONObject response) {

                    Utiles.showMessageLog("responsePopulaire", "" + response.toString());
                    try {
                        Object object = response.get("result");
                        if (object instanceof JSONArray) {

                            fetchData(response.getJSONArray("result"));

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utiles.showMessageLog("ErrorSearch", "" + error.toString());
                }

            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> header = new HashMap<>();

                    header.put("Authorization", "Bearer " + preferences.getString("token", ""));
                    return header;
                }
            };

            MySingleton.getInstance(SearchParty.this).addToRequestQueue(request);*/
        }
    }

    void webServiceGetNear(double lang, double lat) {
        Log.i("url", "http://api.geonames.org/findNearbyPlaceNameJSON?lat=" + lat + "&lng=" + lang + "" +
                "&style=short&cities=cities15000&radius=15&maxRows=5&username=leadershift");

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, "http://api.geonames.org/findNearbyPlaceNameJSON?lat=" + lat + "&lng=" + lang + "" +
                "&style=short&cities=cities15000&radius=15&maxRows=5&username=leadershift", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Utiles.showMessageLog("responseNEar", response.toString());
                try {
                    editWhere.setText("");
                    nearSearchAdapter.fetchData(response.getJSONArray("geonames"));
                    JSONArray arrayVille = new JSONArray();
                    for (int i = 0; i < response.getJSONArray("geonames").length(); i++) {
                        arrayVille.put(response.getJSONArray("geonames").getJSONObject(i).getString("name"));
                    }
                    populaireSearchAdapterPop.webServiceGetPopulaire(arrayVille);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        MySingleton.getInstance(SearchParty.this).addToRequestQueue(request);
    }

}