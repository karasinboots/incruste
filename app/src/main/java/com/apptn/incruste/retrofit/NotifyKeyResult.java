package com.apptn.incruste.retrofit;

public interface NotifyKeyResult {
    void onSuccess(String notifyKey);

    void onFailure(String errorMsg);
}
