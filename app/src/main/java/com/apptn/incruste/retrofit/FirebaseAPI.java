package com.apptn.incruste.retrofit;

import com.apptn.incruste.Models.FirebaseMessage;
import com.apptn.incruste.Models.Group;
import com.apptn.incruste.Models.NotifyId;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public interface FirebaseAPI {

    @POST("/fcm/send")
    Call<FirebaseMessage> sendMessage(@Body FirebaseMessage message);

    @POST("/fcm/notification")
    Call<NotifyId> createGroup(@Body Group group);

    @POST("/fcm/notification")
    Call<NotifyId> joinGroup(@Body Group group);

    @POST("/fcm/notification")
    Call<Void> leaveGroup(@Body Group group);

    @GET("/fcm/notification")
    Call<NotifyId> getNotifyId(@QueryMap Map<String, String> notifyKeyName);
}