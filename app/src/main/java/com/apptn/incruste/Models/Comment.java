package com.apptn.incruste.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Comment {
    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("rate")
    @Expose
    private Integer rate;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("datecreation")
    @Expose
    private Long datecreation;
    @SerializedName("dateupdate")
    @Expose
    private String dateupdate;
    @SerializedName("user_from")
    @Expose
    private String userFrom;
    @SerializedName("user_to")
    @Expose
    private String userTo;
    @SerializedName("soiree")
    @Expose
    private String soiree;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(Long datecreation) {
        this.datecreation = datecreation;
    }

    public String getDateupdate() {
        return dateupdate;
    }

    public void setDateupdate(String dateupdate) {
        this.dateupdate = dateupdate;
    }

    public String getUserFrom() {
        return userFrom;
    }

    public void setUserFrom(String userFrom) {
        this.userFrom = userFrom;
    }

    public String getUserTo() {
        return userTo;
    }

    public void setUserTo(String userTo) {
        this.userTo = userTo;
    }

    public String getSoiree() {
        return soiree;
    }

    public void setSoiree(String soiree) {
        this.soiree = soiree;
    }

}

