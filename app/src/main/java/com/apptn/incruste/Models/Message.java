
package com.apptn.incruste.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Message {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("is_read")
    @Expose
    private Integer isRead;
    @SerializedName("datecreation")
    @Expose
    private Long datecreation;
    @SerializedName("dateupdate")
    @Expose
    private String dateupdate;
    @SerializedName("user_from")
    @Expose
    private String userFrom;
    @SerializedName("user_to")
    @Expose
    private String userTo;
    @SerializedName("mtype")
    @Expose
    private Integer mtype;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getIsRead() {
        return isRead;
    }

    public void setIsRead(Integer isRead) {
        this.isRead = isRead;
    }

    public Long getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(Long datecreation) {
        this.datecreation = datecreation;
    }

    public String getDateupdate() {
        return dateupdate;
    }

    public void setDateupdate(String dateupdate) {
        this.dateupdate = dateupdate;
    }

    public String getUserFrom() {
        return userFrom;
    }

    public void setUserFrom(String userFrom) {
        this.userFrom = userFrom;
    }

    public String getUserTo() {
        return userTo;
    }

    public void setUserTo(String userTo) {
        this.userTo = userTo;
    }

    public Integer getMtype() {
        return mtype;
    }

    public void setMtype(Integer mtype) {
        this.mtype = mtype;
    }
}
