
package com.apptn.incruste.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Party {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("date_soiree")
    @Expose
    private String dateSoiree;
    @SerializedName("adr_public")
    @Expose
    private String adrPublic;
    @SerializedName("adr_complete")
    @Expose
    private String adrComplete;
    @SerializedName("ville")
    @Expose
    private String ville;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("nb_places")
    @Expose
    private Integer nbPlaces;
    @SerializedName("remain")
    @Expose
    private Integer remain;
    @SerializedName("confidentiale")
    @Expose
    private String confidentiale;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("datecreation")
    @Expose
    private String datecreation;
    @SerializedName("dateupdate")
    @Expose
    private String dateupdate;
    @SerializedName("ambiance")
    @Expose
    private String ambiance;
    @SerializedName("extra")
    @Expose
    private List<Extra> extra = new ArrayList<>();
    @SerializedName("incruster")
    @Expose
    private List<Incruster> incruster = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDateSoiree() {
        return dateSoiree;
    }

    public void setDateSoiree(String dateSoiree) {
        this.dateSoiree = dateSoiree;
    }

    public String getAdrPublic() {
        return adrPublic;
    }

    public void setAdrPublic(String adrPublic) {
        this.adrPublic = adrPublic;
    }

    public String getAdrComplete() {
        return adrComplete;
    }

    public void setAdrComplete(String adrComplete) {
        this.adrComplete = adrComplete;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Integer getNbPlaces() {
        return nbPlaces;
    }

    public void setNbPlaces(Integer nbPlaces) {
        this.nbPlaces = nbPlaces;
    }

    public Integer getRemain() {
        return remain;
    }

    public void setRemain(Integer remain) {
        this.remain = remain;
    }

    public String getConfidentiale() {
        return confidentiale;
    }

    public void setConfidentiale(String confidentiale) {
        this.confidentiale = confidentiale;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(String datecreation) {
        this.datecreation = datecreation;
    }

    public String getDateupdate() {
        return dateupdate;
    }

    public void setDateupdate(String dateupdate) {
        this.dateupdate = dateupdate;
    }

    public String getAmbiance() {
        return ambiance;
    }

    public void setAmbiance(String ambiance) {
        this.ambiance = ambiance;
    }

    public List<Extra> getExtra() {
        return extra;
    }

    public void setExtra(List<Extra> extra) {
        this.extra = extra;
    }

    public List<Incruster> getIncruster() {
        return incruster;
    }

    public void setIncruster(List<Incruster> incruster) {
        this.incruster = incruster;
    }

}
