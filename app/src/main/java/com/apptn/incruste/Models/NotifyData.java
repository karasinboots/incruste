package com.apptn.incruste.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotifyData {
    @SerializedName("title")
    @Expose
    String title;
    @SerializedName("body")
    @Expose
    String body;

    public NotifyData(String title, String body) {
        this.title = title;
        this.body = body;
    }
}
