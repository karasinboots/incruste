
package com.apptn.incruste.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Extra_ {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("date_creation")
    @Expose
    private String dateCreation;
    @SerializedName("extras_id")
    @Expose
    private ExtrasId extrasId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    public ExtrasId getExtrasId() {
        return extrasId;
    }

    public void setExtrasId(ExtrasId extrasId) {
        this.extrasId = extrasId;
    }

}
