package com.apptn.incruste.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FirebaseMessage {
    @SerializedName("to")
    @Expose
    String to;
    @SerializedName("data")
    @Expose
    NotifyData data;

    public FirebaseMessage(String to, NotifyData notification) {
        this.to = to;
        this.data = notification;
    }
}
