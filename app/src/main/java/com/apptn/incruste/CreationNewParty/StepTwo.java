package com.apptn.incruste.CreationNewParty;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.apptn.incruste.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Administrateur on 16/11/2017.
 */
public class StepTwo extends Fragment {
    ImageView backArrow;
    private static final int SELECT_PICTURE = 1;
    private static final int REQUEST_IMAGE_CAPTURE = 2;
    public ImageView imageView;
    Bitmap bitmap;
    RelativeLayout deletePic;


    SendPictureInterface anInterface;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            anInterface = (SendPictureInterface) context;
        } catch (ClassCastException cce) {
            cce.printStackTrace();
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = getLayoutInflater().inflate(R.layout.second_creation, container, false);
        backArrow = view.findViewById(R.id.backArrow);
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        imageView = view.findViewById(R.id.image);

         deletePic = view.findViewById(R.id.deletePic);
        deletePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageView.setImageDrawable(null);
                deletePic.setVisibility(View.GONE);
            }
        });
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("clicked", "clicked");
                if (getActivity() != null)
                getActivity().onBackPressed();
            }
        });


        RelativeLayout selectPic = view.findViewById(R.id.selectPic);
        selectPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CharSequence[] items = {getString(R.string.takePic), getString(R.string.gallery),
                        getString(R.string.Cancel)};
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(getString(R.string.addPhoto));
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {


                        if (items[item].equals(getString(R.string.takePic))) {


                            if (ContextCompat.checkSelfPermission(getActivity(),
                                    Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                                        Manifest.permission.CAMERA)) {

                                    Log.d("from first if", "if first");

                                } else {

                                    Log.d("from second if", "if second");

                                    ActivityCompat.requestPermissions(getActivity(),
                                            new String[]{Manifest.permission.CAMERA},
                                            REQUEST_IMAGE_CAPTURE);


                                }
                            } else {
                                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {

                                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                                }
                            }
                        } else if (items[item].equals(getString(R.string.gallery))) {


                            int permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
                                    Manifest.permission.READ_EXTERNAL_STORAGE);
                            if (permissionCheck == PackageManager.PERMISSION_DENIED) {
                                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                                        Manifest.permission.READ_EXTERNAL_STORAGE)) {


                                } else {

                                    ActivityCompat.requestPermissions(getActivity(),
                                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                            SELECT_PICTURE);


                                }
                            } else {
                                Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                                getIntent.setType("image/*");

                                Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                pickIntent.setType("image/*");

                                Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
                                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});

                                startActivityForResult(chooserIntent, SELECT_PICTURE);
                            }


                        } else if (items[item].equals(getString(R.string.Cancel))) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            }
        });

        RelativeLayout toNext = view.findViewById(R.id.nextLayout);

        toNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imageView.getDrawable() == null) {
                    Toast.makeText(getActivity(), "Insert an image", Toast.LENGTH_LONG).show();
                } else {
                    anInterface.sendBitmap(bitmap);
                    StepThree stepThree = new StepThree();
                    FragmentTransaction ft3 = getActivity().getSupportFragmentManager().beginTransaction();
                    ft3.replace(R.id.container, stepThree);
                    ft3.addToBackStack("");
                    ft3.commit();
                }
            }
        });
        return view;


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == 2) {

                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                bitmap = imageBitmap;
                imageView.setImageBitmap(imageBitmap);


            } else if (requestCode == 1) {
                deletePic.setVisibility(View.VISIBLE);
                deletePic.bringToFront();
                Uri selectedImageUri = data.getData();
                Glide.with(this).asBitmap().load(selectedImageUri).into(new SimpleTarget<Bitmap>(500, 300) {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {

                        bitmap = resource;
                        imageView.setImageBitmap(bitmap);
                    }

                });

            }


        }
    }
}
