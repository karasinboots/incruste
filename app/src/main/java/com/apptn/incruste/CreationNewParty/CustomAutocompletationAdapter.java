package com.apptn.incruste.CreationNewParty;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.apptn.incruste.General.LocationDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrateur on 20/11/2017.
 */

public class CustomAutocompletationAdapter extends BaseAdapter implements Filterable {




    private Context mContext;
    private List<LocationDetails> resultList = new ArrayList<>();

    public CustomAutocompletationAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {

        return resultList.size();

    }

    @Override
    public Object getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
        }

        ((TextView) convertView.findViewById(android.R.id.text1)).setText(resultList.get(position).getFullAdress());
        return convertView;
    }


    public void RempliresultList(JSONArray obj_address){

        resultList.clear();

        for(int i = 0; i< obj_address.length(); i++)
        {
            try {

                JSONObject jsonObject = obj_address.getJSONObject(i);
                resultList.add(new LocationDetails(jsonObject));

            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {

                    // Assign the data to the FilterResults
                    filterResults.values =resultList;
                    filterResults.count = resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    resultList = (List<LocationDetails>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }};
        return filter;
    }
}