package com.apptn.incruste.CreationNewParty;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.apptn.incruste.R;

/**
 * Created by Administrateur on 16/11/2017.
 */

public class StepOne extends Fragment {

    ImageView backArrow;
    public EditText partyName, partyDesc;
    FirstCreationSendData activity;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (FirstCreationSendData) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = getLayoutInflater().inflate(R.layout.first_creation, container, false);
        partyName = view.findViewById(R.id.name);
        partyDesc = view.findViewById(R.id.desc);
        RelativeLayout toNext = view.findViewById(R.id.nextLayout);

        toNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (partyName.getText().toString().equals("")|| partyDesc.getText().toString().equals("")){
                    Log.i("empty","empty");
                    partyName.setError(getString(R.string.error1));
                    partyDesc.setError(getString(R.string.error1));
                }else {
                    activity.sendDataFirst(partyName.getText().toString(), partyDesc.getText().toString());
                    StepTwo stepTwo = new StepTwo();
                    FragmentTransaction ft3 = getActivity().getSupportFragmentManager().beginTransaction();
                    ft3.replace(R.id.container, stepTwo, "secondFrag");
                    ft3.addToBackStack("secondFrag");
                    ft3.commit();

                }
            }
        });
        backArrow = view.findViewById(R.id.backArrow);
        activity = (FirstCreationSendData) getActivity();
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("clicked", "clicked");
                if (getActivity()!=null)
                getActivity().onBackPressed();
            }
        });

        return view;
    }





}
