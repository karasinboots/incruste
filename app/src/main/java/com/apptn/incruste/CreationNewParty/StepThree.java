package com.apptn.incruste.CreationNewParty;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.apptn.incruste.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.seatgeek.placesautocomplete.DetailsCallback;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.AddressComponent;
import com.seatgeek.placesautocomplete.model.AddressComponentType;
import com.seatgeek.placesautocomplete.model.Place;
import com.seatgeek.placesautocomplete.model.PlaceDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.seatgeek.placesautocomplete.model.AddressComponentType.LOCALITY;

/**
 * Created by Administrateur on 16/11/2017.
 */

public class StepThree extends Fragment {
    ImageView backArrow;
    FrameLayout datePicker, timePicker;
    public String dateParty;
    public String timeParty;
    public EditText placePublic;
    double lang, lat;
    String ville;
    ThirdSendData activity;
    CustomAutocompletationAdapter adapter;
    boolean isToday = false;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            activity = (ThirdSendData) context;
        } catch (ClassCastException cce) {
            cce.printStackTrace();
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = getLayoutInflater().inflate(R.layout.third_creation, container, false);
        backArrow = view.findViewById(R.id.backArrow);
        //InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        //imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
        final PlacesAutocompleteTextView searchView = view.findViewById(R.id.searchView);
        final TextView showTime = view.findViewById(R.id.showTime);
        final TextView showDate = view.findViewById(R.id.showDate);
        placePublic = view.findViewById(R.id.placePublic);
        timePicker = view.findViewById(R.id.timePicker);
        datePicker = view.findViewById(R.id.datePicker);
        RelativeLayout toNext = view.findViewById(R.id.nextLayout);
        //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        adapter = new CustomAutocompletationAdapter(getContext());
        toNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (showDate.getText().toString().equals("") || showTime.getText().toString().equals("")
                        || placePublic.getText().toString().equals("") || searchView.getText().toString().equals("")) {

                    showDate.setError(getString(R.string.error1));
                    showTime.setError(getString(R.string.error1));
                    placePublic.setError(getString(R.string.error1));
                    searchView.setError(getString(R.string.error1));

                } else {
                    if (lang == 0.0 || lat == 0.0) {

                        searchView.setError(getString(R.string.errorZip));

                    } else {
                        activity.sendData(showDate.getText().toString(), showTime.getText().toString(), placePublic.getText().toString(), searchView.getText().toString(), lang, lat, ville);
                        StepFor stepFor = new StepFor();
                        FragmentTransaction ft3 = getActivity().getSupportFragmentManager().beginTransaction();
                        ft3.replace(R.id.container, stepFor);
                        ft3.addToBackStack("");
                        ft3.commit();
                    }
                }
            }
        });

        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                myCalendar.set(Calendar.YEAR, i);
                myCalendar.set(Calendar.MONTH, i1);
                myCalendar.set(Calendar.DAY_OF_MONTH, i2);
                String myFormat = "dd-MM-yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat);

                Calendar now = Calendar.getInstance();
                Date dateUser = null;
                try {
                    dateUser = sdf.parse(sdf.format(myCalendar.getTime()));
                    Date todaysDate = sdf.parse(sdf.format(now.getTime()));

                    if (dateUser.compareTo(todaysDate) > 0) {
                        System.out.println("DateUser is after Today");
                        showDate.setText(sdf.format(myCalendar.getTime()));
                        dateParty = showDate.getText().toString();
                        isToday = false;
                    } else if (dateUser.compareTo(todaysDate) < 0) {
                        System.out.println("DateUser is before Today");
                        Toast.makeText(getActivity(), getString(R.string.selectFutreDate), Toast.LENGTH_LONG).show();
                        isToday = false;
                    } else if (dateUser.compareTo(todaysDate) == 0) {
                        System.out.println("DateUser is equal to Today");
                        showDate.setText(sdf.format(myCalendar.getTime()));
                        dateParty = showDate.getText().toString();
                        isToday = true;
                    } else {
                        System.out.println("How to get here?");
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        };

        datePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DatePickerDialog(getActivity(), listener, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        timePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar myCalendar = Calendar.getInstance();
                TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        myCalendar.set(Calendar.MINUTE, minute);

                        Calendar datetime = Calendar.getInstance();
                        Calendar c = Calendar.getInstance();
                        datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        datetime.set(Calendar.MINUTE, minute);
                        if (isToday) {
                            if (datetime.getTimeInMillis() >= c.getTimeInMillis()) {
                                //it's after current

                                showTime.setText(String.format("%02d:%02d", hourOfDay,
                                        minute/*, hourOfDay < 12 ? "am" : "pm"*/));
                            } else {
                                //it's before current'
                                Toast.makeText(getActivity(), "Invalid Time", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            showTime.setText(String.format("%02d:%02d", hourOfDay,
                                    minute/*, hourOfDay < 12 ? "am" : "pm"*/));
                        }
                    }
                };
                new TimePickerDialog(getActivity(), mTimeSetListener, myCalendar.get(Calendar.HOUR), myCalendar.get(Calendar.MINUTE), true).show();
            }

        });

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("clicked", "clicked");

                getActivity().onBackPressed();
            }
        });

        /*searchView.setAdapter(adapter);
        searchView.setThreshold(1);
        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                webServiceAutoCmplete(charSequence.toString());
                searchView.showDropDown();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                searchView.showDropDown();
            }
        });*/
/*

        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                LocationDetails location = (LocationDetails) adapterView.getItemAtPosition(i);
                ville = location.getVille();
                searchView.setText(location.getFullAdress());
                lang = location.getaLong();
                lat = location.getLat();
            }
        });
*/
        searchView.setOnPlaceSelectedListener(
                new OnPlaceSelectedListener() {
                    @Override
                    public void onPlaceSelected(final Place place) {
                        searchView.getDetailsFor(place, new DetailsCallback() {
                            @Override
                            public void onSuccess(final PlaceDetails details) {
                                Log.d("test", "details " + details);

                                getLocationFromAddress(details.formatted_address);
                                for (AddressComponent component : details.address_components) {
                                    for (AddressComponentType type : component.types) {
                                        if (type == LOCALITY) {
                                            Log.d("test", "ville " + component.long_name);
                                            ville = component.long_name;
                                        }
                                    }
                                }

                            }

                            @Override
                            public void onFailure(final Throwable failure) {
                                Log.d("test", "failure " + failure);
                            }

                        });
                    }
                }
        );

        //   48.860474, 2.342223

        return view;
    }


    void getLocationFromAddress(String address) {

        String addressURL = "";
        try {
            addressURL = "https://maps.googleapis.com/maps/api/geocode/json?address=" + URLEncoder.encode(address, "UTF-8") + "&key=AIzaSyDy_OTjkyz2RhuooBGcZBs3UVo3jB2vVjk";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient client = new AsyncHttpClient();

        client.get(addressURL, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                String s = null;
                try {
                    s = new String(responseBody, "UTF-8");
                    Log.i("Message from Server", s);
                    JSONObject obj = new JSONObject(s);

                    JSONObject location = obj.getJSONArray("results").getJSONObject(0).getJSONObject("geometry").getJSONObject("location");
                    lat = location.getDouble("lat");
                    lang = location.getDouble("lng");


                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                Log.i("status code", "" + statusCode);

            }
        });

    }

}
