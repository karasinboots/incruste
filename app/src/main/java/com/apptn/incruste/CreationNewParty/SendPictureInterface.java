package com.apptn.incruste.CreationNewParty;

import android.graphics.Bitmap;

/**
 * Created by Administrateur on 23/11/2017.
 */

public interface SendPictureInterface {

    void sendBitmap (Bitmap bitmap);
}
