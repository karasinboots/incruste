package com.apptn.incruste.CreationNewParty;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apptn.incruste.GolbalUtilities.Utiles;
import com.apptn.incruste.Models.ExtrasId;
import com.apptn.incruste.R;
import com.bumptech.glide.Glide;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.apptn.incruste.GolbalUtilities.Utiles.COLLECTION_PUBLIC;


public class StepFor extends Fragment {
    ImageView backArrow;

    int participant = 0;
    TextView partyTotal, txtViewConf;
    // Spinner spinnerConf;
    boolean isUserLerned = false;
    Map<Integer, Boolean> userBools = new HashMap();
    Map<Integer, String> userDesc = new HashMap();
    JSONObject extraDescTemp = new JSONObject();
    AdapterRecyclerExtra adapterRecyclerExtra;
    AutoCompleteTextView confidentiality;
    public static SnackView snackView;


    ForthSendData activity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            activity = (ForthSendData) context;
        } catch (ClassCastException cce) {
            cce.printStackTrace();
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = getLayoutInflater().inflate(R.layout.forth_creation, container, false);
        backArrow = view.findViewById(R.id.backArrow);
        RelativeLayout partPlus = view.findViewById(R.id.partPlus);
        RelativeLayout partMinus = view.findViewById(R.id.partMinus);
        snackView = new SnackView();

        partyTotal = view.findViewById(R.id.partyTotal);
        confidentiality = view.findViewById(R.id.conf);

        //  spinnerConf = view.findViewById(R.id.spinnerConf);
        //  txtViewConf = view.findViewById(R.id.txtViewConf);
        RecyclerView recArray = view.findViewById(R.id.extraRecycler);
        adapterRecyclerExtra = new AdapterRecyclerExtra(getActivity());
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        recArray.setLayoutManager(manager);
        adapterRecyclerExtra.webServicegetExtra();
        recArray.setAdapter(adapterRecyclerExtra);


        final ArrayAdapter arrayAdapter = new ArrayAdapter<>(
                getActivity(), R.layout.list_item, getResources()
                .getStringArray(R.array.conf));
        final String[] selection = new String[1];
        confidentiality.setAdapter(arrayAdapter);
        confidentiality.setDropDownHeight(260);

        confidentiality.setCursorVisible(false);
        confidentiality.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                confidentiality.showDropDown();
                selection[0] = (String) parent.getItemAtPosition(position);

            }
        });

        confidentiality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                confidentiality.showDropDown();
            }
        });


        partPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                plus();
                partyTotal.setText(String.valueOf(participant));
            }
        });

        partMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                minus();
                partyTotal.setText(String.valueOf(participant));
            }
        });

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() != null)
                    getActivity().onBackPressed();
            }
        });


       /* spinnerConf.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        txtViewConf.setText(getString(R.string.confPublic));
                        break;
                    case 1:
                        txtViewConf.setText(getString(R.string.confPrivate));
                        break;
                    default:
                        txtViewConf.setText(getString(R.string.confPublic));
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        RelativeLayout toNext = view.findViewById(R.id.nextLayout);

        toNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (partyTotal.getText().equals("0"))
                    Toast.makeText(getActivity(), getString(R.string.mustSelectParticipant), Toast.LENGTH_LONG).show();
                else {
                    activity.sendData(userBools, userDesc, partyTotal.getText().toString(), confidentiality.getText().toString());
                    StepFive stepFive = new StepFive();
                    if (getActivity() != null) {
                        FragmentTransaction ft3 = getActivity().getSupportFragmentManager().beginTransaction();
                        ft3.replace(R.id.container, stepFive);
                        ft3.addToBackStack("");
                        ft3.commit();
                    }
                }


            }
        });
        return view;
    }

    private void minus() {

        if (participant > 0)
            participant--;

    }

    private void plus() {
        participant++;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == 1) {

            userDesc.put(requestCode, StringEscapeUtils.escapeJava(data.getExtras().getString("text")));
            try {
                extraDescTemp.put(String.valueOf(data.getExtras().getInt("position")), data.getExtras().getString("text"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Utiles.showMessageLog("userDesc", userDesc.toString());
            adapterRecyclerExtra.showEditWihtoutPen(data.getExtras().getInt("position"));
        }

    }


    private class AdapterRecyclerExtra extends RecyclerView.Adapter<AdapterRecyclerExtra.ViewHoler> {

        JSONArray extraData;
        Context ctx;
        boolean update = false;

        private void showEditWihtoutPen(int index) {

            update = true;
            notifyItemChanged(index);
        }

        AdapterRecyclerExtra(Context context) {

            this.extraData = new JSONArray();
            this.ctx = context;

        }

        private void fetchData(JSONArray array) {

            this.extraData = array;
            notifyDataSetChanged();

        }


        @Override
        public ViewHoler onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.inflation_row_extra, parent, false);
            return new ViewHoler(view);
        }

        @Override
        public void onBindViewHolder(ViewHoler holder, int position) {
            try {

                if (update && getActivity() != null) {
                    holder.imgDescExtra.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.edit_no_circle));
                    Glide.with(ctx).load(extraData.getJSONObject(position).getString("description"))
                            .into(holder.imgExtra);
                    holder.nameExtraRow.setText(extraData.getJSONObject(position).getString("name"));
                    holder.holderRow.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.bgSelectedExtra));
                    holder.checkExtra.setChecked(true);
                    holder.imgDescExtra.setVisibility(View.VISIBLE);
                    holder.imgDescExtra.bringToFront();
                    update = false;
                } else {
                    Glide.with(ctx).load(extraData.getJSONObject(position).getString("description"))
                            .into(holder.imgExtra);
                    Log.e("extras", extraData.toString());
                    holder.nameExtraRow.setText(extraData.getJSONObject(position).getString("name"));
                    holder.imgDescExtra.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.edit_circle));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return extraData.length();
        }

        class ViewHoler extends RecyclerView.ViewHolder {

            FrameLayout holderRow;
            ImageView imgExtra;
            CircleImageView imgDescExtra;
            TextView nameExtraRow;
            CheckBox checkExtra;

            ViewHoler(View itemView) {
                super(itemView);

                holderRow = itemView.findViewById(R.id.holderRow);
                imgExtra = itemView.findViewById(R.id.imgExtra);
                imgDescExtra = itemView.findViewById(R.id.imgDescExtra);
                nameExtraRow = itemView.findViewById(R.id.nameExtraRow);
                checkExtra = itemView.findViewById(R.id.checkExtra);

                imgDescExtra.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        DialogFragment newFragment = null;
                        String text = "";

                        try {
                            if (extraDescTemp.has(String.valueOf(getAdapterPosition())))
                                text = extraDescTemp.getString(String.valueOf(getAdapterPosition()));
                            newFragment = NewParty.DescriptionDialog.newInstance(extraData.getJSONObject(getAdapterPosition()), text, getAdapterPosition());
                            if (getFragmentManager() != null) {
                                newFragment.setTargetFragment(StepFor.this, extraData.getJSONObject(getAdapterPosition()).getInt("id"));
                                newFragment.show(getFragmentManager(), "dialog");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                checkExtra.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {

                            if (checkExtra.isChecked()) {
                                imgDescExtra.setVisibility(View.VISIBLE);
                                userBools.put(extraData.getJSONObject(getAdapterPosition()).getInt("id"), true);
                                Utiles.showMessageLog("extraHashMap", "" + userBools.toString());
                                if (getActivity() != null) {

                                    if (!isUserLerned) {
                                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                        transaction.add(R.id.containerSnack, snackView);
                                        transaction.commit();
                                        isUserLerned = true;

                                    }
                                    holderRow.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.bgSelectedExtra));

                                }

                            } else if (!checkExtra.isChecked()) {
                                userBools.put(extraData.getJSONObject(getAdapterPosition()).getInt("id"), false);
                                Utiles.showMessageLog("extraHashMap", "" + userBools.toString());
                                imgDescExtra.setVisibility(View.GONE);
                                if (getActivity() != null)
                                    holderRow.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.whiteDefault));
                            }
                        } catch (JSONException jsx) {
                            jsx.printStackTrace();
                        }
                    }
                });
            }
        }

        private void webServicegetExtra() {
            FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
            final List<ExtrasId> extra = new ArrayList<>();
            firebaseFirestore.collection(COLLECTION_PUBLIC).document("themes")
                    .collection("themes").addSnapshotListener(getActivity(), new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                    extra.addAll(queryDocumentSnapshots.toObjects(ExtrasId.class));
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    try {
                        fetchData(new JSONArray(new Gson().toJson(extra)));
                        JSONArray array = new JSONArray(new Gson().toJson(extra));
                        Log.d("array", "" + array.toString());
                        JSONObject extra = new JSONObject();
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject temp = array.getJSONObject(i);
                            extra.put(temp.getString("name"), temp.getString("id"));
                        }
                        editor.putString("extraIDs", extra.toString());
                        editor.putString("arrayExtra", array.toString());
                        editor.apply();
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }
            });

        }
    }


    public static class SnackView extends Fragment {

        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

            View view = getLayoutInflater().inflate(R.layout.snack_view, container, false);
            ImageView imageView = view.findViewById(R.id.hideSnack);


            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if (getActivity() != null) {
                        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                        ft.remove(snackView);
                        ft.commit();
                    }

                }
            });
            return view;
        }
    }

}
