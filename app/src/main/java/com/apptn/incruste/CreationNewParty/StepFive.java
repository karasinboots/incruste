package com.apptn.incruste.CreationNewParty;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apptn.incruste.GolbalUtilities.Utiles;
import com.apptn.incruste.Models.Ambiance;
import com.apptn.incruste.R;
import com.bumptech.glide.Glide;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.apptn.incruste.GolbalUtilities.Utiles.COLLECTION_PUBLIC;


public class StepFive extends Fragment {
    ImageView backArrow;

    RelativeLayout btnValidation;
    FithSendData activity;
    RecyclerView ambRecycler;
    public String ambiantParty = "";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (FithSendData) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = getLayoutInflater().inflate(R.layout.fith_creation, container, false);
        backArrow = view.findViewById(R.id.backArrow);
        final RelativeLayout layout = view.findViewById(R.id.containerProgress);
        final ProgressBar progresss_bar = view.findViewById(R.id.progresss_bar);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("clicked", "clicked");

                if (getActivity() != null)
                    getActivity().onBackPressed();
            }
        });


        ambRecycler = view.findViewById(R.id.ambianceRecycler);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        ambRecycler.setLayoutManager(manager);
        AdapterAmb adapter = new AdapterAmb(getActivity());
        adapter.getAmbiance();
        ambRecycler.setAdapter(adapter);

        btnValidation = view.findViewById(R.id.btnOK);
        btnValidation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!ambiantParty.equals("null")) {
                    activity.sendData(ambiantParty);
                    layout.setVisibility(View.VISIBLE);
                    progresss_bar.setVisibility(View.VISIBLE);
                    layout.bringToFront();
                    progresss_bar.bringToFront();
                } else
                    Toast.makeText(getActivity(), getResources().getString(R.string.ambiantMsg), Toast.LENGTH_LONG).show();


            }

        });

        return view;
    }


    private class AdapterAmb extends RecyclerView.Adapter<AdapterAmb.ViewHolder> {


        JSONArray ambArray;
        Context context;

        public AdapterAmb(Context ctx) {

            ambArray = new JSONArray();
            this.context = ctx;

        }

        public void fetchData(JSONArray array) {

            this.ambArray = array;
            notifyDataSetChanged();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = getLayoutInflater().inflate(R.layout.inflation_row_amb, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            try {
                holder.nameAmb.setText(ambArray.getJSONObject(position).getString("nom"));
                Glide.with(context).load(ambArray.getJSONObject(position).getString("description"))
                        .into(holder.imgAmb);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return ambArray.length();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            TextView nameAmb;
            FrameLayout containerAmb;
            ImageView imgAmb;
            CheckBox checkAmb;

            public ViewHolder(View itemView) {
                super(itemView);

                nameAmb = itemView.findViewById(R.id.nameAmbiance);
                imgAmb = itemView.findViewById(R.id.imageAmb);
                checkAmb = itemView.findViewById(R.id.boxAmb);
                containerAmb = itemView.findViewById(R.id.containerAmb);

                checkAmb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

//                        for (int i = 0; i <ambRecycler.getChildCount() ; i++) {
//                            FrameLayout frameLayout = ambRecycler.getChildAt(i).findViewById(R.id.containerAmb);
//                            frameLayout.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.whiteDef));
//                        }
                        if (checkAmb.isChecked()) {
                            try {
                                ambiantParty = ambArray.getJSONObject(getAdapterPosition()).getString("id");
                                Utiles.showMessageLog("ambiantParty", "" + ambiantParty);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            for (int i = 0; i < ambRecycler.getChildCount(); i++) {

                                CheckBox checkBox = ambRecycler.getChildAt(i).findViewById(R.id.boxAmb);
                                FrameLayout containerAmb = ambRecycler.getChildAt(i).findViewById(R.id.containerAmb);
                                if (i != getAdapterPosition() && checkBox.isChecked()) {
                                    checkBox.setChecked(false);
                                    if (getActivity() != null)
                                        containerAmb.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.whiteDefault));
                                }

                            }

                            if (getActivity() != null)
                                containerAmb.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.bgSelectedExtra));
                        } else if (!checkAmb.isChecked()) {
                            ambiantParty = "null";
                            if (getActivity() != null)
                                containerAmb.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.whiteDefault));
                        }
                    }
                });

            }
        }


        private void getAmbiance() {
            FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
            final List<Ambiance> ambiances = new ArrayList<>();
            firebaseFirestore.collection(COLLECTION_PUBLIC).document("activities")
                    .collection("ambiance").addSnapshotListener(getActivity(), new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                    ambiances.addAll(queryDocumentSnapshots.toObjects(Ambiance.class));
                    SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getContext()).edit();
                    try {
                        JSONArray array = new JSONArray(new Gson().toJson(ambiances));
                        fetchData(array);
                        List<JSONObject> list = new ArrayList<>();
                        for (int i = 0; i < array.length(); i++) {
                            list.add(array.getJSONObject(i));
                        }
                        JSONObject ThemeIds = new JSONObject();
                        for (int i = 0; i < list.size(); i++) {
                            ThemeIds.put(list.get(i).getString("nom"), list.get(i).getString("id"));
                        }
                        editor.putString("arrayIDs", ThemeIds.toString());
                        editor.putString("JsonAmb", array.toString());
                        editor.apply();
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }
            });
        }
    }
}
