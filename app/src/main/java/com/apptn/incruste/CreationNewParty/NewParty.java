package com.apptn.incruste.CreationNewParty;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apptn.incruste.FireBase.FirebaseMessageSender;
import com.apptn.incruste.GolbalUtilities.Utiles;
import com.apptn.incruste.Models.Extra;
import com.apptn.incruste.Models.Group;
import com.apptn.incruste.Models.Incruster;
import com.apptn.incruste.Models.Party;
import com.apptn.incruste.Models.User;
import com.apptn.incruste.R;
import com.apptn.incruste.explorer.FirstActivty;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.apptn.incruste.GolbalUtilities.Utiles.CHILD_IMAGES;
import static com.apptn.incruste.GolbalUtilities.Utiles.DOCUMENT_PARTY;
import static com.apptn.incruste.GolbalUtilities.Utiles.formatDateNew;

public class NewParty extends AppCompatActivity implements FirstCreationSendData, SendPictureInterface, ThirdSendData, ForthSendData, FithSendData {

    Context ctx;
    public FrameLayout container;
    Bitmap bitmap;
    String nameParty, descParty, partyHour, partyDate, placePublic, placePrivate, ville, partyTotal, conf = "";
    double aLong, lat;
    JSONArray extraParty = new JSONArray();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_party);
        ctx = getApplicationContext();

        StepOne stepOne = new StepOne();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, stepOne, "stepOne");
        ft.commit();

        container = findViewById(R.id.containerSnack);
//        FootMenu menu = FootMenu.newInstance(2);
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.add(R.id.containerCreate,menu);
//        transaction.commit();


    }


    public void createExtra(Map<Integer, Boolean> extras, Map<Integer, String> desc) {


        for (Integer key : extras.keySet()) {
            if (extras.get(key)) {
                try {
                    JSONObject tempObj = new JSONObject();
                    tempObj.put("type", "" + key);

                    if (desc.get(key) != null)
                        tempObj.put("description", "" + desc.get(key));
                    else tempObj.put("description", "");
                    extraParty.put(tempObj);
                } catch (JSONException jsx) {
                    jsx.printStackTrace();
                }
            }
        }

    }


 /*   public static String encodeEmoji (String message) {
        try {
            return URLEncoder.encode(message,
                    "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return message;
        }
    }


    public static String decodeEmoji (String message) {
        String myString= null;
        try {
            return URLDecoder.decode(
                    message, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return message;
        }
    }*/


    private void wscreateParty(final String ambId) {
        final FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();
        final String id = String.valueOf(System.currentTimeMillis());
        final Party party = new Party();
        party.setId(id);
        party.setAmbiance(ambId);
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        party.setUser(new Gson().fromJson(sharedPreferences.getString("user", ""), User.class).getId());
        party.setName(nameParty);
        party.setDescription(descParty);
        //todo make format converter
        party.setDateSoiree(formatDateNew(partyDate + " " + partyHour));
        party.setAdrPublic(placePublic);
        party.setVille(ville);
        List<Extra> extras = new Gson().fromJson(extraParty.toString(), new TypeToken<List<Extra>>() {
        }.getType());
        party.setExtra(extras);
        party.setAdrComplete(placePrivate);
        party.setLongitude(aLong);
        party.setLatitude(lat);
        party.setNbPlaces(Integer.valueOf(partyTotal));
        party.setRemain(Integer.valueOf(partyTotal) - 1);
        party.setConfidentiale(conf);
        Incruster incruster = new Incruster();
        incruster.setIsAccepted(1);
        incruster.setParty(id);
        incruster.setTime(Utiles.getTimeAndDate(party.getDateSoiree()).get(1));
        incruster.setExtras(extras);
        incruster.setUser(new Gson().fromJson(sharedPreferences.getString("user", ""), User.class).getId());
        incruster.setId(String.valueOf(System.currentTimeMillis()));
        List<Incruster> incrusters = new ArrayList<>();
        incrusters.add(incruster);
        party.setIncruster(incrusters);
        firebaseStorage.getReference().child(CHILD_IMAGES).child(party.getId())
                .putBytes(getByteArray(bitmap)).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                task.getResult().getMetadata().getReference().getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        party.setPhoto(task.getResult().toString());
                        firebaseFirestore.collection(DOCUMENT_PARTY)
                                .document(id).set(party).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                FirebaseMessageSender firebaseMessageSender = new FirebaseMessageSender();
                                Group group = new Group();
                                List<String> notifyIds = new ArrayList<>();
                                notifyIds.add(new Gson().fromJson(sharedPreferences.getString("user", ""), User.class).getToken());
                                group.setNotificationKeyName("party_" + id);
                                group.setRegistrationIds(notifyIds);
                                firebaseMessageSender.createGroup(group);
                                AlertDialog.Builder builder = new AlertDialog.Builder(NewParty.this);
                                builder.setTitle(getString(R.string.success))
                                        .setMessage(getString(R.string.succParty))
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                startActivity(new Intent(NewParty.this, FirstActivty.class));
                                                finish();
                                            }
                                        });
                                AlertDialog dialog = builder.create();
                                dialog.setCancelable(false);
                                dialog.show();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(NewParty.this);
                                builder.setTitle("Something goes wrong")
                                        .setMessage("Something goes wrong! Try later.")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                startActivity(new Intent(NewParty.this, FirstActivty.class));
                                                finish();
                                            }
                                        });
                                AlertDialog dialog = builder.create();
                                dialog.setCancelable(false);
                                dialog.show();
                            }
                        });

                    }
                });
            }
        });
    }


    private byte[] getByteArray(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();

        return byteArray;
    }

    @Override
    public void onBackPressed() {
        Fragment myFragment = getSupportFragmentManager().findFragmentByTag("stepOne");
        if (myFragment != null && myFragment.isVisible()) {
            startActivity(new Intent(NewParty.this, FirstActivty.class));
            finish();
        } else {
            super.onBackPressed();
        }


    }


    //firstCreation
    @Override
    public void sendDataFirst(String name, String desc) {
        nameParty = name;
        descParty = desc;
        Utiles.showMessageLog("arrived", "" + name + " and " + desc);
    }

    //StepFive
    @Override
    public void sendData(String wichID) {

        // webServiceCreateParty(wichID);
        wscreateParty(wichID);


    }
//SecondFragment

    @Override
    public void sendBitmap(Bitmap aBitmap) {
        Utiles.showMessageLog("bitmaaaaaaap", "" + aBitmap);
        bitmap = aBitmap;
    }


    //StepFor
    @Override
    public void sendData(Map extra, Map desc, String particip, String confidens) {

        createExtra(extra, desc);
        this.partyTotal = particip;
        if (confidens.equals("0"))
            this.conf = "1";
        else if (confidens.equals("1"))
            this.conf = "0";
        Utiles.showMessageLog("arrivedFor", "" + particip + "  " + partyTotal);

    }


    @Override
    public void sendData(String date, String hour, String publicPlace, String privatePlace, double longetude, double altetude, String village) {
        partyDate = date;
        partyHour = hour;
        placePublic = publicPlace;
        placePrivate = privatePlace;
        aLong = longetude;
        lat = altetude;
        ville = village;
        Utiles.showMessageLog("arrived", "" + publicPlace + " and " + hour + "and " + date + " and " + privatePlace + " " + longetude + " " + altetude);
    }

    public static class DescriptionDialog extends DialogFragment {

        @Override
        public void onStart() {
            super.onStart();
            Dialog dialog = getDialog();
            if (dialog != null) {
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        }

        static DescriptionDialog newInstance(JSONObject object, String text, int position) {

            DescriptionDialog fragment = new DescriptionDialog();
            Bundle bundle = new Bundle();
            bundle.putString("data", object.toString());
            bundle.putString("text", text);
            bundle.putInt("position", position);
            fragment.setArguments(bundle);
            return fragment;
        }

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.dialog_desc, container, false);
            TextView title = v.findViewById(R.id.textDialog);
            ImageView imageView = v.findViewById(R.id.dialogImg);

            JSONObject object;
            try {
                object = new JSONObject(getArguments().getString("data"));
                title.setText(object.getString("name"));
                imageView.setVisibility(View.VISIBLE);
                Glide.with(getActivity()).load(object.getString("description")).into(imageView);


            } catch (JSONException e) {
                e.printStackTrace();
            }


            final EditText description = v.findViewById(R.id.description);
            description.setVisibility(View.VISIBLE);
            if (!getArguments().getString("text").equals(""))
                description.setText(getArguments().getString("text"));
            RelativeLayout btnValider = v.findViewById(R.id.btnValider);

            btnValider.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent();
                    intent.putExtra("text", description.getText().toString());
                    intent.putExtra("position", getArguments().getInt("position"));

                    try {
                        if (getTargetFragment() != null)
                            getTargetFragment().onActivityResult(getTargetRequestCode(), 1, intent);

                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                    dismiss();
                }
            });
            return v;
        }


    }
}

