package com.apptn.incruste.Settings;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apptn.incruste.R;

import java.util.Arrays;
import java.util.List;

public class Gratitude extends AppCompatActivity {



    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gratitude);
        Typeface font = Typeface.createFromAsset(getAssets(),"Neutraface2Text-Book.otf");
        TextView textView = findViewById(R.id.ty);
        TextView titlePage = findViewById(R.id.titlePage);
        textView.setTypeface(font,Typeface.BOLD);
        ImageView imageView = findViewById(R.id.imageBack);
        imageView.setColorFilter(Color.WHITE);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        titlePage.setTypeface(font);
        List<String> lines = Arrays.asList(getResources().getStringArray(R.array.names));
        RecyclerView listView = findViewById(R.id.list_names);
        AdapterGrat adapter = new AdapterGrat(this,lines);
        listView.setLayoutManager(new LinearLayoutManager(this));
        listView.setAdapter(adapter);





    }


    private class AdapterGrat extends RecyclerView.Adapter<AdapterGrat.ViewHolder>{

        List<String> names;
        Context ctx;

        AdapterGrat (Context context,List<String> names){

            this.ctx = context;
            this.names = names;
        }
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = getLayoutInflater().inflate(R.layout.inflation_single_row_grat,parent,false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

        holder.textView.setText(names.get(position));



        }

        @Override
        public int getItemCount() {
            return names.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder{

            Typeface font = Typeface.createFromAsset(getAssets(),"NeutraText-Demi.otf");
            TextView textView ;
            public ViewHolder(View itemView) {
                super(itemView);
                textView = itemView.findViewById(R.id.nameToThank);
                textView.setTypeface(font);
            }
        }
    }

}

