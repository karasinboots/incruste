package com.apptn.incruste.Settings;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.apptn.incruste.General.LoginActivity;
import com.apptn.incruste.R;

public class Settings extends AppCompatActivity {


    float x1,x2;
    float y1, y2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        TextView logout = findViewById(R.id.logOut);
        ImageView back = findViewById(R.id.backArrow);



        FrameLayout gratitudeLayout = findViewById(R.id.gratitudeLayout);

        gratitudeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Settings.this,Gratitude.class));
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(Settings.this).edit();
                editor.clear();
                editor.apply();
                startActivity(new Intent(Settings.this, LoginActivity.class));
            }
        });
    }


    public void openPartnerClass(View view) {
        startActivity(new Intent(Settings.this,OfficialPartner.class));
    }
}



