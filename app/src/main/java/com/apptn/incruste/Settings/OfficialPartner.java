package com.apptn.incruste.Settings;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.apptn.incruste.R;

public class OfficialPartner extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_official_partner);
    }

    public void openWebBrowser(View view) {

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://frenchcoco-box.com"));
        startActivity(browserIntent);
    }
}
