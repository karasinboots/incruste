package com.apptn.incruste.FireBase;

import android.util.Log;

import com.apptn.incruste.Models.FirebaseMessage;
import com.apptn.incruste.Models.Group;
import com.apptn.incruste.Models.NotifyId;
import com.apptn.incruste.retrofit.FirebaseAPI;
import com.apptn.incruste.retrofit.NotifyKeyResult;
import com.apptn.incruste.retrofit.RetrofitClient;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FirebaseMessageSender {

    public static final String API_KEY = "AAAAKRn6ixs:APA91bGawDQC0si1bBNrjE1Q98ViM3eihWPrP587DXCNMx" +
            "cGuTbPHrH-CpSUcvsV33e-x48lhA24_uO0agKSbEmkGQpxxXhPWbD4iOWrMhDUbzo6ALNUmoXif5z2QmvYqESya" +
            "wa0tKoRaa9_zJJRgaLrYornhMbWBQ";
    public static final String PROJECT_ID = "176529509147";
    public static final String OPERATION_CREATE = "create";
    public static final String OPERATION_ADD = "add";
    public static final String OPERATION_REMOVE = "remove";

    public void sendPush(FirebaseMessage firebaseMessage) {
        FirebaseAPI firebaseAPI = RetrofitClient.getRetrofit();
        Call<FirebaseMessage> call2 = firebaseAPI.sendMessage(firebaseMessage);
        call2.enqueue(new Callback<FirebaseMessage>() {
            @Override
            public void onResponse(Call<FirebaseMessage> call, Response<FirebaseMessage> response) {
                Log.e("onSendResponse ", "onResponse");
            }

            @Override
            public void onFailure(Call<FirebaseMessage> call, Throwable t) {
                Log.e("onSendResponse ", "onFailure");
            }
        });
    }

    public void createGroup(Group group) {
        group.setOperation(FirebaseMessageSender.OPERATION_CREATE);
        FirebaseAPI firebaseAPI = RetrofitClient.getGroupRetrofit();
        Call<NotifyId> call2 = firebaseAPI.createGroup(group);
        call2.enqueue(new Callback<NotifyId>() {
            @Override
            public void onResponse(Call<NotifyId> call, Response<NotifyId> response) {
                Log.e("onCreateGroupResponse ", "onCreateGroupResponse");
            }

            @Override
            public void onFailure(Call<NotifyId> call, Throwable t) {
                Log.e("onCreateGroupResponse ", "onFailure");
            }
        });
    }

    public void joinGroup(Group group, final NotifyKeyResult notifyKeyResult) {
        group.setOperation(FirebaseMessageSender.OPERATION_ADD);
        FirebaseAPI firebaseAPI = RetrofitClient.getGroupRetrofit();
        Call<NotifyId> call2 = firebaseAPI.joinGroup(group);
        call2.enqueue(new Callback<NotifyId>() {
            @Override
            public void onResponse(Call<NotifyId> call, Response<NotifyId> response) {
                Log.e("onJoinResponse ", "onCreateGroupResponse");
                notifyKeyResult.onSuccess("");
            }

            @Override
            public void onFailure(Call<NotifyId> call, Throwable t) {
                Log.e("onJoinResponse ", "onFailure");
                notifyKeyResult.onFailure(t.getMessage());
            }
        });
    }

    public void leaveGroup(Group group) {
        group.setOperation(FirebaseMessageSender.OPERATION_REMOVE);
        FirebaseAPI firebaseAPI = RetrofitClient.getGroupRetrofit();
        Call<Void> call2 = firebaseAPI.leaveGroup(group);
        call2.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.e("Response ", "onCreateGroupResponse");
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e("Response ", "onFailure");
            }
        });
    }

    public void getGroupKey(String keyName, final NotifyKeyResult notifyKeyResult) {
        FirebaseAPI firebaseAPI = RetrofitClient.getGroupRetrofit();
        Map<String, String> keyNameMap = new HashMap<>();
        keyNameMap.put("notification_key_name", keyName);
        Call<NotifyId> call2 = firebaseAPI.getNotifyId(keyNameMap);
        call2.enqueue(new Callback<NotifyId>() {
            @Override
            public void onResponse(Call<NotifyId> call, Response<NotifyId> response) {
                Log.e("getGroupResponse ", "onCreateGroupResponse");
                if (response.isSuccessful() && response.body() != null)
                    notifyKeyResult.onSuccess(response.body().getNotificationKey());
                else notifyKeyResult.onFailure(String.valueOf(response.code()));
            }

            @Override
            public void onFailure(Call<NotifyId> call, Throwable t) {
                Log.e("getGroupResponse ", "onFailure");
                notifyKeyResult.onFailure(t.getMessage());
            }
        });
    }
}
