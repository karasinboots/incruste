package com.apptn.incruste.FireBase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.apptn.incruste.Messages.Messages;
import com.apptn.incruste.Models.Message;
import com.apptn.incruste.Models.User;
import com.apptn.incruste.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by Administrateur on 09/12/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    final int REQUEST = 1;
    final int ACCEPT = 2;
    final int MESSAGE = 5;

    Message incomingMessage;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e("called", "called");
        if (remoteMessage.getData().size() > 0) {
            Log.e("PayloadFromHAykel", "Message data payload: " + remoteMessage.getData());
            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
            } else {
                // Handle message within 10 seconds
            }
            sendNotification(remoteMessage.getData());
        }
        if (remoteMessage.getNotification() != null) {
            Log.e("TAG", "Message Notification Body: " + remoteMessage.getNotification().getBody());
            Log.e("TAG", "Message Notification Title: " + remoteMessage.getNotification().getTitle());
            Log.e("TAG", "Message Notification Text: " + remoteMessage.getNotification().getBodyLocalizationKey());
            Log.e("Notification", "Data: " + remoteMessage.getData().toString());
        }
    }

    private void sendNotification(Map<String, String> data) {

        JSONObject object = new JSONObject();
        for (Map.Entry<String, String> entry : data.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            Log.e("the_key_is", "" + key);
            Log.e("the_value_is", "" + value);
            Log.e("key_Value_paire", "" + key + ": " + value);
            try {
                object.put(key, value);
                incomingMessage = new Gson().fromJson(object.getString("body"), Message.class);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Log.e("thisIsTheJson", "" + object.toString());
        initChannels(this);
        try {
            Message message = new Gson().fromJson(object.getString("body"), Message.class);
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            User currentUser = new Gson().fromJson(sharedPreferences.getString("user", "{}"), User.class);
            if (message.getUserFrom().equals(currentUser.getId())) return;
            int mtype = message.getMtype();
          /*  if (PartyDetails.active) {
                if (mtype == REQUEST) {
                    Log.e("msgType", "request");
                    LocalBroadcastManager broadcaster = LocalBroadcastManager.getInstance(getBaseContext());
                    Intent intent = new Intent("notification");
                    intent.putExtra("Key", REQUEST);
                    intent.putExtra("type", "request");
                    intent.putExtra("data", object.toString());
                    broadcaster.sendBroadcast(intent);
                } else if (mtype == ACCEPT) {
                    Log.e("msgType", "accept");
                    LocalBroadcastManager broadcaster = LocalBroadcastManager.getInstance(getBaseContext());
                    Intent intent = new Intent("notification");
                    intent.putExtra("Key", ACCEPT);
                    intent.putExtra("type", "accept");
                    intent.putExtra("data", object.toString());
                    broadcaster.sendBroadcast(intent);
                } else if (mtype == MESSAGE) {
                    Log.e("msgType", "message");
                    Intent intent = new Intent(getApplicationContext(), Messages.class);
                    intent.putExtra("cameFrom", "notification");
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("id", incomingMessage.getUserFrom());
                    PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "1")
                            .setSmallIcon(R.drawable.notification)
                            .setContentTitle(getString(R.string.newMessage))
                            .setContentText(getString(R.string.descNewMessage))
                            .setContentIntent(contentIntent)
                            .setAutoCancel(true)
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(3, mBuilder.build());
                }
            } else*/
            {
                if (mtype == REQUEST) {
                    Log.e("msgType", "request");
                    Intent intent = new Intent(getApplicationContext(), Messages.class);
                    intent.putExtra("cameFrom", "notification");
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("id", incomingMessage.getUserFrom());
                    PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, getString(R.string.channel_request))
                            .setSmallIcon(R.drawable.ic_notification)
                            .setContentTitle(getString(R.string.RequestIncrust))
                            .setContentText(getString(R.string.youHaveRequestIncrust))
                            .setContentIntent(contentIntent)
                            .setAutoCancel(true)
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(1, mBuilder.build());
                } else if (mtype == ACCEPT) {
                    Log.e("msgType", "accept");
                    Intent intent = new Intent(getApplicationContext(), Messages.class);
                    intent.putExtra("cameFrom", "notification");
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("id", incomingMessage.getUserFrom());
                    PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, getString(R.string.channel_accept))
                            .setSmallIcon(R.drawable.ic_notification)
                            .setContentTitle(getString(R.string.requestAccepted))
                            .setContentText(getString(R.string.descRequestAccepted))
                            .setAutoCancel(true)
                            .setContentIntent(contentIntent)
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(2, mBuilder.build());
                } else if (mtype == MESSAGE) {
                    Log.e("msgType", "message");
                    Intent intent = new Intent(getApplicationContext(), Messages.class);
                    intent.putExtra("cameFrom", "notification");
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("id", incomingMessage.getUserFrom());
                    PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, getString(R.string.channel_messages))
                            .setSmallIcon(R.drawable.ic_notification)
                            .setContentTitle(getString(R.string.newMessage))
                            .setContentText(getString(R.string.descNewMessage))
                            .setAutoCancel(true)
                            .setContentIntent(contentIntent)
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(3, mBuilder.build());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void initChannels(Context context) {
        if (Build.VERSION.SDK_INT < 26) {
            return;
        }
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel channelAccept = new NotificationChannel(getString(R.string.channel_accept),
                getString(R.string.channel_accept),
                NotificationManager.IMPORTANCE_DEFAULT);
        channelAccept.setDescription(getString(R.string.acceptation));
        notificationManager.createNotificationChannel(channelAccept);

        NotificationChannel channelRequest = new NotificationChannel(getString(R.string.channel_request),
                getString(R.string.channel_request),
                NotificationManager.IMPORTANCE_DEFAULT);
        channelRequest.setDescription(getString(R.string.requests));
        notificationManager.createNotificationChannel(channelRequest);

        NotificationChannel channelMessages = new NotificationChannel(getString(R.string.channel_messages),
                getString(R.string.channel_messages),
                NotificationManager.IMPORTANCE_DEFAULT);
        channelMessages.setDescription(getString(R.string.messaging));
        notificationManager.createNotificationChannel(channelMessages);
    }
}
