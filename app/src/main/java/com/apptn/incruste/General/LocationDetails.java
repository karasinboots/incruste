package com.apptn.incruste.General;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrateur on 22/11/2017.
 */

public  class LocationDetails {
    private String fullAdress;
    private double aLong;
    private double lat;
    private String ville;

    public  LocationDetails (JSONObject location){



        try {
            this.aLong = location.getJSONObject("location").getDouble("lon");
            this.lat = location.getJSONObject("location").getDouble("lat");
            this.fullAdress = location.getString("name") + location.getString("state") ;
            this.ville = location.getString("city");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getVille() {
        return ville;
    }

    public String getFullAdress() {
        return fullAdress;
    }

    public double getaLong() {
        return aLong;
    }

    public double getLat() {
        return lat;
    }

}
