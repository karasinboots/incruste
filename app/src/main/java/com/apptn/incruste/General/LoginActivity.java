package com.apptn.incruste.General;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apptn.incruste.GolbalUtilities.Utiles;
import com.apptn.incruste.Models.User;
import com.apptn.incruste.R;
import com.apptn.incruste.SingUp.SingUp;
import com.apptn.incruste.explorer.FirstActivty;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.crashlytics.android.Crashlytics;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;

import io.fabric.sdk.android.Fabric;

import static com.apptn.incruste.GolbalUtilities.Utiles.DOCUMENT_PROFILE;

public class LoginActivity extends AppCompatActivity {
    public static final int MY_PERMISSIONS_REQUEST_COARSE_LOCATION = 1999;

    EditText etLogin, etPass;
    JSONObject user;
    SharedPreferences sharedPreferences;
    CallbackManager callbackManager;

    FirebaseAuth firebaseAuth;
    FirebaseFirestore firebaseFirestore;
    FirebaseUser firebaseUser;
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_login);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        LinearLayout fb = findViewById(R.id.btnFbLoging);
        firebaseAuth = FirebaseAuth.getInstance();
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST_COARSE_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }


        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        Log.d("Success", "" + loginResult.getAccessToken().toString());
                        handleFacebookAccessToken(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(LoginActivity.this, "Login Cancel", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Utiles.showMessageLog("errorException", exception.getMessage());
                        //Toast.makeText(LoginActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });


        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this,
                        Arrays.asList("public_profile", "user_birthday", "email", "user_location", "user_photos"));

            }
        });

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        etLogin = findViewById(R.id.loginMail);
        etPass = findViewById(R.id.loginPass);
        try {
            user = new JSONObject(sharedPreferences.getString("user", "{}"));
            if (firebaseAuth.getCurrentUser() != null && !user.toString().equals("{}")) {

                //  Utiles.getToken(getApplicationContext());
                Utiles.getAmbiance(getApplicationContext());
                Utiles.getExtraType(getApplicationContext());
                TextView text1 = findViewById(R.id.text1);
                TextView textbs = findViewById(R.id.textbs);
                TextView text2 = findViewById(R.id.text2);
                TextView text3 = findViewById(R.id.text3);
                TextView text4 = findViewById(R.id.text4);


                Typeface font = Typeface.createFromAsset(getAssets(), "Neutraface2Text-Book.otf");
                text1.setTypeface(font);
                text2.setTypeface(font);
                text3.setTypeface(font);
                textbs.setTypeface(font);
                text4.setTypeface(font);
                startActivity(new Intent(LoginActivity.this, FirstActivty.class));
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_COARSE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                            MY_PERMISSIONS_REQUEST_COARSE_LOCATION);

                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void login(View view) {
        webServiceLogin();
    }

    public void singUp(View view) {
        startActivity(new Intent(LoginActivity.this, SingUp.class));
    }

    private void handleFacebookAccessToken(final AccessToken token) {
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.e("firebase login", "success");
                            firebaseFirestore = FirebaseFirestore.getInstance();
                            firebaseFirestore.collection(DOCUMENT_PROFILE)
                                    .document(firebaseAuth.getCurrentUser().getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    try {
                                        task.getResult().exists();
                                    } catch (Exception e) {
                                        Toast.makeText(LoginActivity.this, "FIREBASE NOT ACTIVE", Toast.LENGTH_LONG).show();
                                        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().clear().commit();
                                        finish();
                                        return;
                                    }
                                    if (task.getResult().exists()) {
                                        Log.e("profile", "exist");
                                        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
                                        editor.putString("user", new Gson().toJson(task.getResult().toObject(User.class)));
                                        editor.apply();
                                        Utiles.webServiceAddDevice(LoginActivity.this, firebaseAuth.getCurrentUser().getUid(), sharedPreferences);
                                        startActivity(new Intent(LoginActivity.this, FirstActivty.class));
                                        finish();
                                    } else {
                                        new GraphRequest(
                                                AccessToken.getCurrentAccessToken(),
                                                "/me?fields=location{location},id,first_name,last_name,email,birthday,age_range,picture.type(large),gender",
                                                null,
                                                HttpMethod.GET,
                                                new GraphRequest.Callback() {
                                                    public void onCompleted(final GraphResponse response) {
                                                        Utiles.showMessageLog("GrapheData", "" + response.getJSONObject().toString());

                                                        try {

                                                            if (!response.getJSONObject().getJSONObject("picture").getJSONObject("data").getBoolean("is_silhouette")) {

                                                                String imageLink = response.getJSONObject().getJSONObject("picture").getJSONObject("data").getString("url");
                                                                String newLink = "";
                                                                for (int i = 0; i < imageLink.length(); i++) {

                                                                    if (imageLink.charAt(i) == '/') {
                                                                        if (imageLink.charAt(i + 1) == '/')
                                                                            newLink = imageLink.substring(i);
                                                                    }
                                                                }

                                                                Glide.with(LoginActivity.this).asBitmap().load("https:" + newLink).into(new SimpleTarget<Bitmap>() {
                                                                    @Override
                                                                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {

                                                                        Log.e("LoginActivity", response.toString());
                                                                        // Application code
                                                                        Intent i = new Intent(LoginActivity.this, SingUp.class);
                                                                        i.putExtra("fb_dict", response.getJSONObject().toString());
                                                                        startActivity(i);
                                                                    }
                                                                });

                                                            }
                                                        } catch (JSONException jsx) {
                                                            jsx.printStackTrace();
                                                        }

                                                    }
                                                }
                                        ).executeAsync();
                                    }
                                }
                            });
                        } else {
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void webServiceLogin() {
        if (TextUtils.getTrimmedLength(etPass.getText()) < 6) {
            etPass.setError(getString(R.string.passError));
            return;
        }
        if (TextUtils.getTrimmedLength(etLogin.getText()) < 6) {
            etLogin.setError(getString(R.string.noValidEmail));
            return;
        }
        firebaseAuth.signInWithEmailAndPassword(etLogin.getText().toString(),etPass.getText().toString())
                .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                            // there was an error
                            Toast.makeText(LoginActivity.this, getString(R.string.errorLogin), Toast.LENGTH_LONG).show();
                        } else {
                            FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
                            firebaseFirestore.collection(DOCUMENT_PROFILE)
                                    .document(firebaseAuth.getCurrentUser().getUid())
                                    .get()
                                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                            User user = task.getResult().toObject(User.class);
                                            if (user == null) {
                                                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().clear().commit();
                                                startActivity(new Intent(LoginActivity.this, LoginActivity.class));
                                                return;
                                            }
                                            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
                                            editor.putString("user", new Gson().toJson(task.getResult().toObject(User.class)));
                                            editor.apply();
                                            Utiles.webServiceAddDevice(LoginActivity.this, firebaseAuth.getCurrentUser().getUid(), sharedPreferences);
                                            Log.d("responseShared", "" + task.getResult().toString());
                                            startActivity(new Intent(LoginActivity.this, FirstActivty.class));
                                            finish();
                                        }
                                    });
                        }
                    }
                });
    }

    private byte[] getByteArray(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

}
