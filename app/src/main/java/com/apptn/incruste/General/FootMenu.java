package com.apptn.incruste.General;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apptn.incruste.CreationNewParty.NewParty;
import com.apptn.incruste.Messages.Messages;
import com.apptn.incruste.ProfilePac.Profile;
import com.apptn.incruste.R;
import com.apptn.incruste.explorer.FirstActivty;
import com.apptn.incruste.myPartys.MyPartys;


/**
 * A simple {@link Fragment} subclass.
 */
public class FootMenu extends Fragment {

    TabLayout tabFoot;

    static FootMenu mInstance;
    public FootMenu() {
        // Required empty public constructor
    }


    public static FootMenu getInstance(){
        if (mInstance == null)
        mInstance = new FootMenu();

        return mInstance;
    }

    public static FootMenu newInstance(int index){
        FootMenu footMenu = new FootMenu();
        Bundle bundle = new Bundle();
        bundle.putInt("pageIndex",index);
        footMenu.setArguments(bundle);
        return footMenu;
    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_foot_menu, container, false);


        tabFoot = view.findViewById(R.id.tabFoot);

        View expView = getTabView(R.drawable.explorer_selector, getString(R.string.tab1));
        View myPartys = getTabView(R.drawable.mypartys_selector, getString(R.string.tab2));
        View addParty = getTabView(R.drawable.addparty_selector, getString(R.string.tab3));
        View profile = getTabView(R.drawable.profile_selector, getString(R.string.tab4));
        View message = getTabView(R.drawable.msg_selector, getString(R.string.tab5));
        tabFoot.setTabTextColors(Color.GRAY,Color.BLUE);
        tabFoot.addTab(tabFoot.newTab().setCustomView(expView));
        tabFoot.addTab(tabFoot.newTab().setCustomView(myPartys));
        tabFoot.addTab(tabFoot.newTab().setCustomView(addParty));
        tabFoot.addTab(tabFoot.newTab().setCustomView(profile));
        tabFoot.addTab(tabFoot.newTab().setCustomView(message));



        try {
            tabFoot.getTabAt(getArguments().getInt("pageIndex")).select();

        }catch (NullPointerException nullExc){
            nullExc.printStackTrace();
        }


        tabFoot.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        if (getActivity() != null){
                            Intent intent = new Intent(getActivity(), FirstActivty.class);
                            getActivity().startActivity(intent);
                            getActivity().finish();
                        }

                        break;
                    case 1:
                        if (getActivity() != null){
                            Intent intent = new Intent(getActivity(), MyPartys.class);
                            getActivity().startActivity(intent);
                            getActivity().finish();

                        }

                        break;
                    case 2:
                        if (getActivity() != null){
                            getActivity().startActivity(new Intent(getActivity(), NewParty.class));
                            getActivity().finish();
                        }

                        break;
                    case 3:
                        if (getActivity() != null) {

                            Intent  intent = new Intent(getActivity(), Profile.class);
                            intent.putExtra("cameFrom","selfProfile");
                            intent.putExtra("permission","write");
                            getActivity().startActivity(intent);
                            getActivity().finish();
                        }
                        break;
                    case 4:
                        if (getActivity() != null) {
                            Intent  intent = new Intent(getActivity(), Messages.class);
                            intent.putExtra("cameFrom","footer");
                            getActivity().startActivity(intent);
                            getActivity().finish();
                        }
                        break;

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });




        return view;
    }


    private View getTabView(int imgDrawable, String title) {
        @SuppressLint("InflateParams") View view = getLayoutInflater().inflate(R.layout.tab_view, null);
        ImageView imgTab = view.findViewById(R.id.imgTab);
        TextView textView = view.findViewById(R.id.txtTab);
        Typeface font = null;
        if (getActivity()!=null){
          font   = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Regular.ttf");
        }

        textView.setTypeface(font);
        textView.setText(title);
        imgTab.setImageDrawable(getResources().getDrawable(imgDrawable));

        return view;
    }

}
