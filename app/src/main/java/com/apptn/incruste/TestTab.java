package com.apptn.incruste;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.apptn.incruste.explorer.FragmentCategorys;
import com.apptn.incruste.explorer.FragmentPartys;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

public class TestTab extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_tab);



        ViewPager viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(new SectionPagerAdapter(getSupportFragmentManager()));

        SmartTabLayout viewPagerTab =  findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);
    }


    private class SectionPagerAdapter extends FragmentPagerAdapter {


        SectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            Fragment fragment = new Fragment();

            if (position == 0) {
                fragment = new FragmentPartys();
            } else if (position == 1) {
                fragment = new FragmentCategorys();
            }


            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Recommandées";
                case 1:
                    return "Ambiance";
            }
            return "";
        }
    }
}
