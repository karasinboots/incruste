package com.apptn.incruste.SingUp;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.apptn.incruste.GolbalUtilities.Utiles;
import com.apptn.incruste.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import org.json.JSONException;
import org.json.JSONObject;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Administrateur on 22/11/2017.
 */

public  class Forth extends android.support.v4.app.Fragment {
    public ImageView imageView;
    Bitmap bitmap;
    private static final int SELECT_PICTURE = 1;
    private static final int REQUEST_IMAGE_CAPTURE = 2;
    RelativeLayout deletePic ;
    ForthInterface anInterface;
    JSONObject facebookDict;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            anInterface = (ForthInterface)context;
        }catch (ClassCastException cce){
            cce.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.forth, null);

        deletePic = view.findViewById(R.id.deletePic);
        imageView = view.findViewById(R.id.addPic);

        if (getArguments().getString("fbDict") != null){
            try {
                facebookDict = new JSONObject(getArguments().getString("fbDict"));

                String imageUrl = facebookDict.getJSONObject("picture").getJSONObject("data").getString("url");
                Log.i("url", imageUrl);
                Glide.with(this).asBitmap().load(imageUrl).into(new SimpleTarget<Bitmap>(500, 500) {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        bitmap = resource;
                        imageView.setImageBitmap(bitmap);
                        deletePic.bringToFront();
                        deletePic.setVisibility(View.VISIBLE);
                    }

                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        deletePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utiles.showMessageLog("supp","supp");

                    imageView.setImageDrawable(null);
                    deletePic.setVisibility(View.GONE);

            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CharSequence[] items = {getString(R.string.takePic), getString(R.string.gallery),
                        getString(R.string.Cancel)};
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(getString(R.string.addPhoto));
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {

                        if (items[item].equals(getString(R.string.takePic))) {

                            if (ContextCompat.checkSelfPermission(getActivity(),
                                    Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                                        Manifest.permission.CAMERA)) {

                                    Log.d("from first if", "if first");

                                } else {

                                    Log.d("from second if", "if second");

                                    ActivityCompat.requestPermissions(getActivity(),
                                            new String[]{Manifest.permission.CAMERA},
                                            REQUEST_IMAGE_CAPTURE);


                                }
                            } else {
                                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {

                                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                                }
                            }
                        } else if (items[item].equals(getString(R.string.gallery))) {


                            int permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
                                    Manifest.permission.READ_EXTERNAL_STORAGE);
                            if (permissionCheck == PackageManager.PERMISSION_DENIED) {
                                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                                        Manifest.permission.READ_EXTERNAL_STORAGE)) {


                                } else {

                                    ActivityCompat.requestPermissions(getActivity(),
                                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                            SELECT_PICTURE);


                                }
                            } else {
                                Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                                getIntent.setType("image/*");

                                Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                pickIntent.setType("image/*");

                                Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
                                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});

                                startActivityForResult(chooserIntent, SELECT_PICTURE);
                            }


                        } else if (items[item].equals(getString(R.string.Cancel))) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            }
        });
        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == 2) {
                deletePic.setVisibility(View.VISIBLE);
                deletePic.bringToFront();
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                bitmap = imageBitmap;
                imageView.setImageBitmap(imageBitmap);


            } else if (requestCode == 1) {
                deletePic.setVisibility(View.VISIBLE);
                deletePic.bringToFront();

                Uri selectedImageUri = data.getData();
                Glide.with(this).asBitmap().load(selectedImageUri).into(new SimpleTarget<Bitmap>(500, 500) {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        bitmap = resource;
                        imageView.setImageBitmap(bitmap);
                    }

                });


            }


        }

    }

    public  void sendPic(){

        if (bitmap != null) {
            anInterface.sendForthInfo(bitmap);
        } else {

            Snackbar.make(getActivity().findViewById(android.R.id.content),
                    "Veuillez entrer une photo de profil pour continuer.", Snackbar.LENGTH_LONG).show();
        }
    }
}
