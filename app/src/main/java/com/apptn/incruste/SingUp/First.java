package com.apptn.incruste.SingUp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.apptn.incruste.R;

import org.json.JSONException;
import org.json.JSONObject;


public class First extends android.support.v4.app.Fragment {


    public LinearLayout container;
    EditText name, lastName;
    Spinner gender;
    View view;
    AutoCompleteTextView sexe;
    JSONObject facebookDict;

    FirstInterFace interFace;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            interFace = (FirstInterFace) context;
        } catch (ClassCastException cce) {
            cce.printStackTrace();
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.first, container, false);

        name = view.findViewById(R.id.inscriName);
        lastName = view.findViewById(R.id.inscriLastName);
        // gender = view.findViewById(R.id.inscriGender);
        sexe = view.findViewById(R.id.gender);

        if (getArguments().getString("fbDict") != null){
            try {
                facebookDict= new JSONObject(getArguments().getString("fbDict"));
                name.setText(facebookDict.getString("first_name"));
                name.setEnabled(false);
                lastName.setText(facebookDict.getString("last_name"));
                lastName.setEnabled(false);
                if (facebookDict.has("gender")){
                    if (facebookDict.getString("gender").equals("female"))
                        sexe.setText("Femme");
                    else
                        sexe.setText("Homme");
                    sexe.setEnabled(false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        final ArrayAdapter arrayAdapter = new ArrayAdapter<>(
                getActivity(), R.layout.list_item, getResources()
                .getStringArray(R.array.sexe));
        final String[] selection = new String[1];
        sexe.setAdapter(arrayAdapter);

        sexe.setCursorVisible(false);
        sexe.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                sexe.showDropDown();
                selection[0] = (String) parent.getItemAtPosition(position);

            }
        });

        sexe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                sexe.showDropDown();
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }
        });

       /* sexe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               gender.performClick();
            }
        });*/

       /* gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        if (getActivity() != null)
                            sexe.setText(getActivity().getString(R.string.man));
                        break;
                    case 1:
                        if (getActivity() != null)
                            sexe.setText(getActivity().getString(R.string.woman));
                        break;
                   *//* default:
                        if (getActivity() != null)
                            sexe.setText(getActivity().getString(R.string.man));
                        break;*//*
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

                adapterView.setSelection(0);
            }
        });*/
        return view;
    }

    public void send() {
        interFace.sendDataFirst(name.getText().toString(), lastName.getText().toString(), sexe.getText().toString());
    }

    public void showNameError() {
        name.setError(getString(R.string.error1));
    }

    public void showLastnameError() {
        lastName.setError(getString(R.string.error1));
    }
}
