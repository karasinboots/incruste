package com.apptn.incruste.SingUp;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.apptn.incruste.CreationNewParty.CustomAutocompletationAdapter;
import com.apptn.incruste.GolbalUtilities.Utiles;
import com.apptn.incruste.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.seatgeek.placesautocomplete.DetailsCallback;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.Place;
import com.seatgeek.placesautocomplete.model.PlaceDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Administrateur on 22/11/2017.
 */

public class Second extends android.support.v4.app.Fragment{

  //  String dateBirthDay = "";
    EditText phone;
    PlacesAutocompleteTextView zip;
    EditText birth_day;
    double lang,lat = 0.0;
    JSONObject facebookDict;
    CustomAutocompletationAdapter adapter;


    SecondInterface anInterface;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            anInterface = (SecondInterface) context;

        }catch (ClassCastException cce){
            cce.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.second,container,false);
        zip = view.findViewById(R.id.inscriZip);
        phone = view.findViewById(R.id.inscriPhone);

       // final ImageView dateBirth = view.findViewById(R.id.dateBirth);
        birth_day = view.findViewById(R.id.txtBirthday);

        if (getArguments().getString("fbDict") != null){
            try {
                facebookDict= new JSONObject(getArguments().getString("fbDict"));

                if (facebookDict.has("birthday")){
                    birth_day.setText(facebookDict.getString("birthday").replace("/","-"));
                    birth_day.setEnabled(false);
                }

                if (facebookDict.has("location")){
                    JSONObject obj = facebookDict.getJSONObject("location").getJSONObject("location");
                    zip.setText(obj.getString("city")+", "+obj.getString("country"));
                    zip.setEnabled(false);
                    lang=obj.getDouble("longitude");
                    lat=obj.getDouble("latitude");

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                myCalendar.set(Calendar.YEAR, i);
                myCalendar.set(Calendar.MONTH, i1);
                myCalendar.set(Calendar.DAY_OF_MONTH, i2);

                String myFormat = "dd-MM-yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
                String ss=""+i2+"-"+i1+"-"+""+i;

                Log.i("age", ""+Utiles.getAge(ss) );
                    if (Utiles.getAge(ss) < 18){
                        Toast.makeText(getActivity(),getString(R.string.underAge),Toast.LENGTH_LONG).show();
                       // birth_day.setError(getString(R.string.underAge));
                    }else {
                        birth_day.setText(sdf.format(myCalendar.getTime()));
                        //dateBirthDay = birth_day.getText().toString();
                    }


            }
        };

        birth_day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DatePickerDialog(getActivity(), listener, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        //adapter = new CustomAutocompletationAdapter(getActivity());

//        zip.setAdapter(adapter);
//        zip.setThreshold(0);
        zip.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                int before = i + i1;
                int after = i + i2;

                if (before != after) {
                    lang = 0.0;
                    lat = 0.0;
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

//        zip.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                LocationDetails location = (LocationDetails) adapterView.getItemAtPosition(i);
//                zip.setText(location.getFullAdress());
//                lang = location.getaLong();
//                lat = location.getLat();
//            }
//        });



        zip.setOnPlaceSelectedListener(
                new OnPlaceSelectedListener() {
                    @Override
                    public void onPlaceSelected(final Place place) {
                        zip.getDetailsFor(place, new DetailsCallback() {
                            @Override
                            public void onSuccess(final PlaceDetails details) {
                                Log.d("test", "details " + details);

                                getLocationFromAddress(details.formatted_address);

                            }

                            @Override
                            public void onFailure(final Throwable failure) {
                                Log.d("test", "failure " + failure);
                            }

                        });
                    }
                }
        );
        return view;
    }

    void getLocationFromAddress(String address){

        String addressURL = "";
        try {
            addressURL = "https://maps.googleapis.com/maps/api/geocode/json?address=" + URLEncoder.encode(address, "UTF-8") + "&key=AIzaSyDy_OTjkyz2RhuooBGcZBs3UVo3jB2vVjk";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient client = new AsyncHttpClient();

        client.get(addressURL,new AsyncHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                String s = null;
                try {
                    s = new String(responseBody, "UTF-8");
                    Log.i("Message from Server", s);
                    JSONObject obj = new JSONObject(s);

                    JSONObject location = obj.getJSONArray("results").getJSONObject(0).getJSONObject("geometry").getJSONObject("location");
                    lat = location.getDouble("lat");
                    lang = location.getDouble("lng");


                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                Log.i("status code",""+statusCode);

            }
        });

    }

//    public void webServiceAutoCmplete(final String address) {
//        String addressURL = "";
//        try {
//            addressURL = "https://www.waze.com/SearchServer/mozi?q=" + URLEncoder.encode(address, "UTF-8") + ",France&lang=fr&lon=2.344465&lat=48.852513&origin=livemap";
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//
//
//        JsonArrayRequest request = new JsonArrayRequest(addressURL, new Response.Listener<JSONArray>() {
//            @Override
//            public void onResponse(JSONArray response) {
//                Log.i("responseAddress",response.toString());
//                adapter.RempliresultList(response);
//                adapter.notifyDataSetChanged();
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.i("errorAddress",error.toString());
//            }
//        });
//
//        MySingleton.getInstance(getActivity()).addToRequestQueue(request);
//    }

    public void sendData(){
        anInterface.sendSecondData(birth_day.getText().toString(),zip.getText().toString(),phone.getText().toString(),lang,lat);
    }
    public void showBirthdayError(){
        birth_day.setError(getString(R.string.error1));
    }

    public void showPhoneError() {
        phone.setError(getString(R.string.error1));
    }

    public void showZipError(){

        zip.setError(getString(R.string.errorZip));
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        Snackbar mSnackbar = Snackbar.make(getActivity().findViewById(android.R.id.content),
                R.string.errorZip, Snackbar.LENGTH_LONG);
        mSnackbar.setAction(R.string.choose_adress, new MyChoiceListener());
        mSnackbar.show();

    }

    private class MyChoiceListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {

            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            zip.requestFocus();
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        }
    }
}
