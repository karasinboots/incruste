package com.apptn.incruste.SingUp;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.apptn.incruste.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrateur on 22/11/2017.
 */

public class Third extends android.support.v4.app.Fragment {

    EditText email, password, bisPass;

    JSONObject facebookDict;

    ThirdInterface anInterface;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            anInterface = (ThirdInterface) context;
        } catch (ClassCastException cce) {
            cce.printStackTrace();
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.third, null);

        email = view.findViewById(R.id.inscriEmail);
        password = view.findViewById(R.id.insriPassword);
        bisPass = view.findViewById(R.id.bisPass);

        if (getArguments().getString("fbDict") != null){
            try {
                facebookDict= new JSONObject(getArguments().getString("fbDict"));

                if (facebookDict.has("email")){
                    email.setText(facebookDict.getString("email"));
                    email.setEnabled(false);
                }

                view.findViewById(R.id.lay_pass).setVisibility(View.INVISIBLE);
                view.findViewById(R.id.lay_confirm).setVisibility(View.INVISIBLE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        bisPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.equals(password.getText().toString()))
                    Toast.makeText(getActivity(), "Identiqueeeeee", Toast.LENGTH_LONG).show();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (bisPass.equals(password.getText().toString()))
                    Toast.makeText(getActivity(), "Identique", Toast.LENGTH_LONG).show();

            }
        });


        return view;
    }

    public void sendData() {

        anInterface.sendThirdData(email.getText().toString(), password.getText().toString(),bisPass.getText().toString());
    }
    public void showError(){
        email.setError(getString(R.string.error1));
        password.setError(getString(R.string.error1));
    }

    public void notValidEmail (){

        email.setError(getString(R.string.noValidEmail));
    }

    public void showPassError() {
        bisPass.setError(getString(R.string.passError));
    }

    @Override
    public void onStop() {
        super.onStop();
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }
}



