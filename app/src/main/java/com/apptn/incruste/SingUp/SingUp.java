package com.apptn.incruste.SingUp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apptn.incruste.GolbalUtilities.Utiles;
import com.apptn.incruste.Models.User;
import com.apptn.incruste.R;
import com.apptn.incruste.explorer.FirstActivty;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.apptn.incruste.GolbalUtilities.Utiles.CHILD_IMAGES;
import static com.apptn.incruste.GolbalUtilities.Utiles.DOCUMENT_PROFILE;

public class SingUp extends AppCompatActivity implements FirstInterFace, SecondInterface, ThirdInterface, ForthInterface {

    int counter = 1;
    String birth_day = "";
    String name = "";
    String password = "";
    String bisPassword = "";
    String gender = "";
    String email = "";
    String lastName = "";
    String zip = "";
    String phone = "";
    double aLang, aAlt;
    RelativeLayout containerProgress;
    Bitmap bitmapFromForth = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_up);
        containerProgress = findViewById(R.id.containerPorog);
        Typeface font = Typeface.createFromAsset(getAssets(), "Neutraface2Text-Book.otf");
        TextView text1 = findViewById(R.id.text1);
        TextView text2 = findViewById(R.id.text2);
        text1.setTypeface(font);
        text2.setTypeface(font);
        RelativeLayout next = findViewById(R.id.next);
        final First first = new First();
        final Second second = new Second();
        final Third third = new Third();
        final Forth forth = new Forth();

//        if (getIntent().getStringExtra("fb_dict") != null){
//            try {
//                facebookDict=new JSONObject(getIntent().getStringExtra("fb_dict"));
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
        Bundle bundle = new Bundle();

        if (getIntent().getStringExtra("fb_dict") != null) {
            bundle.putString("fbDict", getIntent().getStringExtra("fb_dict"));
        }

        first.setArguments(bundle);
        second.setArguments(bundle);
        third.setArguments(bundle);
        forth.setArguments(bundle);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.container, first, "first");

        transaction.addToBackStack("first");
        transaction.commit();

        containerProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (counter) {
                    case 1:
                        first.send();
                        if (name.length() <= 1) {
                            first.showNameError();
                        } else if (lastName.length() <= 1) {
                            first.showLastnameError();
                        } else {
                            FragmentTransaction transaction2 = getSupportFragmentManager().beginTransaction();
                            transaction2.replace(R.id.container, second, "second");
                            transaction2.addToBackStack("second");
                            transaction2.commit();
                            counter++;
                        }
                        break;
                    case 2:
                        aAlt= 0.0;
                        aLang = 0.0;
                        second.sendData();

                        if (birth_day.equals("") || zip.equals("") || phone.equals("")) {
                           if(birth_day.equals("")) second.showBirthdayError();
                           if(zip.equals("")) second.showZipError();
                           if(phone.length() < 3) second.showPhoneError();

                        } else {
                            if (aAlt == 0.0 || aLang == 0.0) {
                                second.showZipError();

                            } else {
                                FragmentTransaction transaction3 = getSupportFragmentManager().beginTransaction();
                                transaction3.replace(R.id.container, third, "third");
                                transaction3.addToBackStack("second");
                                transaction3.commit();
                                counter++;
                            }

                        }
                        break;
                    case 3:

                        third.sendData();
                        if (email.equals("") || getIntent().getStringExtra("fb_dict") == null && (password.equals("") || bisPassword.equals(""))) {
                            third.showError();
                            //third.showPassError();

                        }

                        if (!password.equals(bisPassword) || bisPassword.length() <= 5) {
                            third.showPassError();

                        } else {

                            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                                third.notValidEmail();
                            } else {
                                FragmentTransaction transaction4 = getSupportFragmentManager().beginTransaction();
                                transaction4.replace(R.id.container, forth, "forth");
                                transaction4.addToBackStack("second");
                                transaction4.commit();
                                counter++;
                            }

                        }

                        break;

                    case 4:
                        forth.sendPic();

                        break;

                }
            }
        });
    }

    @Override
    public void onBackPressed() {

        if (counter > 1) {
            counter--;
            super.onBackPressed();
        } else {
            finish();
        }

    }

    private void wscreateUser() {
        final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        final FirebaseStorage storage = FirebaseStorage.getInstance();
        final FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        if (getIntent().getStringExtra("fb_dict") == null)
            firebaseAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(SingUp.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            //Toast.makeText(SingUp.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();
                            if (!task.isSuccessful()) {
                                Snackbar.make(findViewById(android.R.id.content), "Authentication failed:" + task.getException().getMessage() , Snackbar.LENGTH_LONG).show();
                                Log.d("TAG" , "Authentication failed " + task.getException());
                                containerProgress.setVisibility(View.GONE);
                            } else {
                                final User user = new User();
                                user.setId(firebaseAuth.getCurrentUser().getUid());
                                user.setEmail(email);
                                user.setNom(name);
                                user.setPrenom(lastName);
                                user.setSexe(gender);
                                try {
                                    DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.FRANCE);
                                    Date date = format.parse(birth_day);
                                    DateFormat ff = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.FRANCE);
                                    String new_sstr = ff.format(date);
                                    user.setDateNaissance(new_sstr);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                user.setTelephone(phone);
                                user.setAdress(zip);
                                user.setLongitude(aLang);
                                user.setLatitude(aAlt);
                                if (bitmapFromForth != null)
                                    storage.getReference().child(CHILD_IMAGES).child(firebaseAuth.getUid()).putBytes(getByteArray(bitmapFromForth))
                                            .addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                                                    task.getResult().getMetadata().getReference().getDownloadUrl()
                                                            .addOnCompleteListener(new OnCompleteListener<Uri>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<Uri> task) {
                                                                    user.setAvatar(task.getResult().toString());
                                                                    firestore.collection(DOCUMENT_PROFILE).document(firebaseAuth.getCurrentUser().getUid())
                                                                            .set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                        @Override
                                                                        public void onComplete(@NonNull Task<Void> task) {
                                                                            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                                                            SharedPreferences.Editor editor = sharedPreferences.edit();
                                                                            editor.putString("user", new Gson().toJson(user));
                                                                            editor.apply();
                                                                            Utiles.webServiceAddDevice(SingUp.this, firebaseAuth.getCurrentUser().getUid(), sharedPreferences);
                                                                            startActivity(new Intent(SingUp.this, FirstActivty.class));
                                                                            finish();
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                }
                                            });
                            }
                        }
                    });
        else {
            final User user = new User();
            user.setEmail(email);
            user.setId(firebaseAuth.getCurrentUser().getUid());
            user.setNom(name);
            user.setPrenom(lastName);
            user.setSexe(gender);
            try {
                DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.FRANCE);
                Date date = format.parse(birth_day);
                DateFormat ff = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.FRANCE);
                String new_sstr = ff.format(date);
                user.setDateNaissance(new_sstr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            user.setTelephone(phone);
            user.setAdress(zip);
            user.setLongitude(aLang);
            user.setLatitude(aAlt);
            try {
                user.setFacebookId(new JSONObject(getIntent().getStringExtra("fb_dict")).getString("id"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            user.setAvatar("facebook");
            firestore.collection(DOCUMENT_PROFILE).document(firebaseAuth.getCurrentUser().getUid())
                    .set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("user", new Gson().toJson(user));
                    editor.apply();
                    Utiles.webServiceAddDevice(SingUp.this, firebaseAuth.getCurrentUser().getUid(), sharedPreferences);
                    startActivity(new Intent(SingUp.this, FirstActivty.class));
                    finish();
                }
            });
        }

/*
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        //
        String ss = getIntent().getStringExtra("fb_dict") != null ? "inscritFB" : "inscrit";
        String addressURL = Utiles.GLOBAL_LINK_URL + ss;

        AsyncHttpClient client = new AsyncHttpClient();

        client.addHeader("Authorization", "Bearer " + sharedPreferences.getString("token", ""));

        ByteArrayInputStream bInput = null;

        if (bitmapFromForth != null) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmapFromForth.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            byte[] data = bos.toByteArray();
            bInput = new ByteArrayInputStream(data);
        }

        RequestParams params = new RequestParams();
        JSONObject userObject = null;

        params.put("mail", "" + email);
        params.put("password", "" + password);
        params.put("nom", "" + name);
        params.put("prenom", "" + lastName);
        params.put("sexe", "" + gender);
        params.put("date_naissance", "" + birth_day);
        params.put("telephone", "" + phone);
        params.put("adress", "" + zip);
        params.put("longitude", aLang);
        if (bitmapFromForth != null)
            params.put("imageexist", "" + 1);
        else
            params.put("imageexist", "" + 0);
        Log.i("aaaallaaa", "" + aLang);
        params.put("latitude", "" + aAlt);
        Log.i("aaaallaaa", "" + aAlt);

        if (bInput != null)
            params.put("avatar", bInput);


        if (getIntent().getStringExtra("fb_dict") != null)
            try {
                params.put("id_fb", new JSONObject(getIntent().getStringExtra("fb_dict")).getString("id"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        Log.i("inscritretour", params.toString());
        Log.i("URL add", addressURL);

        client.post(addressURL, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                String resultResponse = new String(response.toString());

                Log.i("inscritretour", resultResponse);
                try {
                    final JSONObject result = new JSONObject(resultResponse);
                    Object object = result.get("result");
                    if (object instanceof JSONObject) {
                        containerProgress.setVisibility(View.GONE);

                        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SingUp.this);
                        alertDialogBuilder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
                                try {
                                    editor.putString("user", result.getJSONObject("result").toString());
                                    editor.apply();
                                    Utiles.webServiceAddDevice(SingUp.this, result.getJSONObject("result"), sharedPreferences);
                                    startActivity(new Intent(SingUp.this, FirstActivty.class));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }).setTitle(getString(R.string.success)).setMessage(R.string.alertSingUp);
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } else if (result.getString("result").equals("mail exist")) {

                        containerProgress.setVisibility(View.GONE);
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SingUp.this);

                        alertDialogBuilder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                SingUp.this.finish();
                            }
                        }).setPositiveButton(getString(R.string.retry), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).setTitle(getString(R.string.oups)).setMessage(R.string.emailExsist);
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                    } else {

                        containerProgress.setVisibility(View.GONE);
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SingUp.this);

                        alertDialogBuilder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                SingUp.this.finish();
                            }
                        }).setPositiveButton(getString(R.string.retry), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).setTitle(getString(R.string.oups)).setMessage(R.string.error);
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                    Log.i("UploadImageResponse", "" + new JSONObject(resultResponse).toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                // Log.i("erroioor inscri", errorResponse.toString());
                // if ()
                // Toast.makeText(getApplicationContext(), "Error : " + errorResponse.toString(), Toast.LENGTH_LONG).show();
                if (statusCode == 401) {
                    Utiles.getToken(SingUp.this);
                    wscreateUser();
                } else {
                    Utiles.showVolleyError(SingUp.this);
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
*/
    }

    private byte[] getByteArray(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();

        return byteArray;
    }


    @Override
    public void sendDataFirst(String aName, String lastname, String aGender) {
//        Log.i("data from first",""+name+lastname+gender);
        name = aName;
        lastName = lastname;
        gender = aGender;

    }

    @Override
    public void sendForthInfo(Bitmap bitmap) {
        containerProgress.setVisibility(View.VISIBLE);
        containerProgress.bringToFront();
        bitmapFromForth = bitmap;

        //webServiceSingUp();
        try {
            wscreateUser();
        } catch (Exception e) {
            containerProgress.setVisibility(View.GONE);
        }
    }

    @Override
    public void sendThirdData(String mail, String pass, String bisPass) {
        password = pass;
        email = mail;
        bisPassword = bisPass;
    }

    @Override
    public void sendSecondData(String birth, String adress, String phone, double lang, double alt) {

        birth_day = birth;
        zip = adress;
        this.phone = phone;
        aAlt = alt;
        aLang = lang;
        Log.i("alllttt", "" + aAlt + aLang);
    }
}
