package com.apptn.incruste.explorer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.apptn.incruste.General.FootMenu;
import com.apptn.incruste.GolbalUtilities.Utiles;
import com.apptn.incruste.Models.Party;
import com.apptn.incruste.Models.User;
import com.apptn.incruste.R;
import com.apptn.incruste.myPartys.PartyDetails;
import com.bumptech.glide.Glide;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import static com.apptn.incruste.GolbalUtilities.Utiles.DOCUMENT_PARTY;
import static com.apptn.incruste.GolbalUtilities.Utiles.DOCUMENT_PROFILE;
import static com.apptn.incruste.GolbalUtilities.Utiles.distance;

public class AllPartys extends AppCompatActivity {

    public final int CAME_FROM_NEAR = 100;
    public final int CAME_FROM_FAR = 200;
    SharedPreferences sharedPreferences;
    private EndlessScrollListener scrollListener;
    JSONObject userObject = null;
    private FusedLocationProviderClient mFusedLocationClient;

    private Location currentBestLocation = null;

    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        super.onCreate(savedInstanceState);


        if (getIntent().getExtras().getString("cameFrom").equals("ambiance")) {
            setContentView(R.layout.party_from_ambiance);

            ImageView arrowBAck = findViewById(R.id.backToolbar);

            arrowBAck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        } else
            setContentView(R.layout.activity_all_partys);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        scrollListener = new EndlessScrollListener(1) {
            @Override
            public boolean onLoadMore(int page, int totalItemsCount) {
                Log.i("", "loaaaaaaaaaaad");
                return false;
            }
        };
        FootMenu menu = FootMenu.newInstance(0);
        getSupportFragmentManager().beginTransaction().add(R.id.all_partys_footer, menu, "menu").commit();

        RecyclerView recyclerView = findViewById(R.id.recyclerAll);

        try {
            userObject = new JSONObject(sharedPreferences.getString("user", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        final RecyclerAdapter adapter = new RecyclerAdapter(this);
        FusedLocationProviderClient mFusedLocationClient = mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        try {
            switch (getIntent().getExtras().getString("cameFrom")) {
                case "ambiance":
                    try {
                        JSONArray objectData = new JSONArray(getIntent().getExtras().getString("jsonData"));
                        Log.e("obkeectData", getIntent().getExtras().getString("jsonData"));
                        TextView ambianceTitle = findViewById(R.id.partyNameToolbar);
                        ambianceTitle.setText(new JSONObject(getIntent().getExtras().getString("ambiance")).getString("nom"));
                        recyclerView.setLayoutManager(manager);
                        recyclerView.setAdapter(adapter);
                        adapter.fetchDataCom(objectData);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;
                case "partys":
                    mFusedLocationClient.getLastLocation()
                            .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    adapter.webServicePartys(location);
                                }
                            });
                    recyclerView.setLayoutManager(manager);
                    recyclerView.setAdapter(adapter);
                    break;
                case "research":
                    try {
                        recyclerView.setLayoutManager(manager);
                        recyclerView.setAdapter(adapter);
                        JSONObject objectData = new JSONObject(getIntent().getExtras().getString("data"));
                        adapter.fetchDataCom(objectData.getJSONArray("result"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;
                case "addition":
                    JSONObject data = new JSONObject(getIntent().getExtras().getString("dataJson"));
                    if (getIntent().getExtras().getInt("request") == 100) {
                        recyclerView.setLayoutManager(manager);
                        recyclerView.setAdapter(adapter);
                        adapter.fetchDataCom(data.getJSONArray("result"));
                    } else if (getIntent().getExtras().getInt("request") == 200) {
                        recyclerView.setLayoutManager(manager);
                        recyclerView.setAdapter(adapter);
                        recyclerView.addOnScrollListener(scrollListener);
                        adapter.fetchDataCom(data.getJSONArray("result2"));
                    }

                    break;
            }

        } catch (NullPointerException | JSONException nullEx) {

            nullEx.printStackTrace();
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(AllPartys.this, FirstActivty.class));
        finish();
    }


    private class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.myViewHolder> {


        private JSONArray data;
        private Context context;


        RecyclerAdapter(Context context) {

            this.context = context;


            data = new JSONArray();

        }

        public void fetchDataCom(JSONArray data) {

            this.data = data;
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).getPath() + "/log.json");
            try {
                FileWriter fileWriter = new FileWriter(file);
                fileWriter.write(data.toString());
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.notifyDataSetChanged();


        }


        @Override
        public RecyclerAdapter.myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = getLayoutInflater().inflate(R.layout.inflation_my_partys, parent, false);

            return new myViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final myViewHolder holder, int position) {


            holder.gradient.bringToFront();
            holder.gradient.setBackground(Utiles.getColor());
            holder.likefavs.setVisibility(View.VISIBLE);
            holder.likefavs.bringToFront();

            try {

                JSONArray arrayFav = Utiles.getFavoriteFromPrefrences(context);
                for (int i = 0; i < arrayFav.length(); i++) {
                    if (data.getJSONObject(position).getString("id").equals(arrayFav.getString(i))) {
                        holder.likefavs.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.like_fill));
                        holder.likefavs.bringToFront();
                        holder.likefavs.setVisibility(View.VISIBLE);
                    }
                }


                if (data.length() > 0) {
                    holder.layoutPersons.bringToFront();
                    Log.e("datajson",data.toString());
                    if (data.getJSONObject(position).has("remain") && data.getJSONObject(position).has("nb_places"))
                        holder.participant.setText(String.valueOf(data.getJSONObject(position).getInt("nb_places")
                                - data.getJSONObject(position).getInt("remain")));
                    else
                        holder.participant.setText("0");


                    Glide.with(context).load(new JSONObject(getIntent().getExtras()
                            .getString("ambiance")).getString("description")).into(holder.ambImg);


                    holder.partyName.setText("#" + data.getJSONObject(position).getString("name"));
                    holder.dateParty.setText(Utiles.getTimeAndDate(data.getJSONObject(position).getString("date_soiree")).get(0));
                    holder.dateParty.bringToFront();
                    holder.timeParty.setText(Utiles.getTimeAndDate(data.getJSONObject(holder.getAdapterPosition()).getString("date_soiree")).get(1));
                    holder.timeParty.bringToFront();
                    Glide.with(context).load(data.getJSONObject(holder.getAdapterPosition()).getString("photo")
                    ).into(holder.imgParty);
                    FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
                    firebaseFirestore.collection(DOCUMENT_PROFILE).document(data.getJSONObject(holder.getAdapterPosition())
                            .getString("user")).get()
                            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    User user = task.getResult().toObject(User.class);
                                    holder.partyCreator.setText(user.getNom() + " " + user.getPrenom());
                                }
                            });
                    holder.partyLocation.setText(data.getJSONObject(holder.getAdapterPosition()).getString("adr_public"));
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        @Override
        public int getItemCount() {

            return data.length();
        }

        class myViewHolder extends RecyclerView.ViewHolder {

            TextView partyName, dateParty, timeParty, partyCreator, partyLocation, participant;
            ImageView imgParty, ambImg, likefavs;
            FrameLayout layoutPersons;
            RoundedImageView gradient;


            public myViewHolder(View itemView) {
                super(itemView);

                layoutPersons = itemView.findViewById(R.id.layoutPersonsMy);
                participant = itemView.findViewById(R.id.participant);
                likefavs = itemView.findViewById(R.id.like);
                ambImg = itemView.findViewById(R.id.ambImg);
                gradient = itemView.findViewById(R.id.gradientMyPartys);
                partyName = itemView.findViewById(R.id.partyName);
                dateParty = itemView.findViewById(R.id.dateParty);
                timeParty = itemView.findViewById(R.id.timeParty);
                partyCreator = itemView.findViewById(R.id.partyCreator);
                partyLocation = itemView.findViewById(R.id.partyLocation);
                imgParty = itemView.findViewById(R.id.imgParty);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, PartyDetails.class);
                        try {
                            intent.putExtra("extraInfo", data.getJSONObject(getAdapterPosition()).toString());
                            Log.e("extraInfo", data.getJSONObject(getAdapterPosition()).toString());
                            intent.putExtra("cameFrom", "myParty");
                            context.startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });

                likefavs.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            JSONArray arrayFav = Utiles.getFavoriteFromPrefrences(context);
                            if (arrayFav.length() > 0) {

                                boolean duplicated = false;
                                for (int i = 0; i < arrayFav.length(); i++) {

                                    if (data.getJSONObject(getAdapterPosition()).getString("id").equals(arrayFav.get(i))) {
                                        Utiles.deleteFromSharedPrefrences(context, data.getJSONObject(getAdapterPosition()).getString("id"));
                                        likefavs.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.like_white));
                                        duplicated = true;
                                    }
                                }

                                if (!duplicated) {
                                    likefavs.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.like_fill));
                                    Utiles.addFavoriteToPrefrences(context, data.getJSONObject(getAdapterPosition()).getString("id"));
                                }

                            } else {
                                likefavs.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.like_fill));
                                Utiles.addFavoriteToPrefrences(context, data.getJSONObject(getAdapterPosition()).getString("id"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }

        LocationManager mLocationManager;


        /**
         * @return the last know best location
         */
        private Location getLastBestLocation() {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return null;
            }
            Location locationGPS = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return null;
            }
            Location locationNet = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            long GPSLocationTime = 0;
            if (null != locationGPS) {
                GPSLocationTime = locationGPS.getTime();
            }

            long NetLocationTime = 0;

            if (null != locationNet) {
                NetLocationTime = locationNet.getTime();
            }

            if (0 < GPSLocationTime - NetLocationTime) {
                return locationGPS;
            } else {
                return locationNet;
            }
        }
        final List<Party> closePartyList = new ArrayList<>();
        final List<Party> farPartyList = new ArrayList<>();
        private void webServicePartys(final Location location) {
            FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
            firebaseFirestore.collection(DOCUMENT_PARTY).addSnapshotListener(AllPartys.this, new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                    closePartyList.clear();
                    farPartyList.clear();
                    if (queryDocumentSnapshots != null && !queryDocumentSnapshots.isEmpty())
                        for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments()) {
                            Party party = snapshot.toObject(Party.class);
                            if (party.getConfidentiale().equals(""))
                                if (location != null) {
                                    if (distance(location.getLatitude(), location.getLongitude(), party.getLatitude(), party.getLongitude()) < 10) {
                                        closePartyList.add(party);
                                        Log.e("party", "close");
                                        Log.e("party latit/longit", party.getLatitude() + "/" + party.getLongitude());
                                        Log.e("user latitude/longitude", location.getLatitude() + "/" + location.getLongitude());
                                        Log.e("distance", distance(location.getLatitude(), location.getLongitude(), party.getLatitude(), party.getLongitude()) + "");
                                    } else {
                                        farPartyList.add(party);
                                        Log.e("party", "far");
                                        Log.e("party latit/longit", party.getLatitude() + "/" + party.getLongitude());
                                        Log.e("user latitude/longitude", location.getLatitude() + "/" + location.getLongitude());
                                        Log.e("distance", distance(location.getLatitude(), location.getLongitude(), party.getLatitude(), party.getLongitude()) + "");
                                    }
                                } else if (distance(GpsListener.latitude, GpsListener.longitude, party.getLatitude(), party.getLongitude()) < 10) {
                                    closePartyList.add(party);
                                    Log.e("party", "close");
                                    Log.e("party latit/longit", party.getLatitude() + "/" + party.getLongitude());
                                    Log.e("user latitude/longitude", GpsListener.latitude + "/" + GpsListener.longitude);
                                    Log.e("distance", distance(GpsListener.latitude, GpsListener.longitude, party.getLatitude(), party.getLongitude()) + "");
                                } else {
                                    farPartyList.add(party);
                                    Log.e("party", "far");
                                    Log.e("party latit/longit", party.getLatitude() + "/" + party.getLongitude());
                                    Log.e("user latitude/longitude", GpsListener.latitude + "/" + GpsListener.longitude);
                                    Log.e("distance", distance(GpsListener.latitude, GpsListener.longitude, party.getLatitude(), party.getLongitude()) + "");
                                }
                        }
                    try {
                        if (getIntent().getExtras().getInt("cameFrom") == CAME_FROM_NEAR)
                            fetchDataCom(new JSONArray(new Gson().toJson(closePartyList)));
                        if (getIntent().getExtras().getInt("cameFrom") == CAME_FROM_FAR)
                            fetchDataCom(new JSONArray(new Gson().toJson(farPartyList)));
                    } catch (JSONException ez) {
                        ez.printStackTrace();
                    }
                }
            });
        }
    }
}
