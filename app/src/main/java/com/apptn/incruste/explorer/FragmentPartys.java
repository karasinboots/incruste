package com.apptn.incruste.explorer;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apptn.incruste.GolbalUtilities.Utiles;
import com.apptn.incruste.Models.Ambiance;
import com.apptn.incruste.Models.Party;
import com.apptn.incruste.Models.User;
import com.apptn.incruste.R;
import com.apptn.incruste.myPartys.PartyDetails;
import com.bumptech.glide.Glide;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedImageView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import static com.apptn.incruste.GolbalUtilities.Utiles.COLLECTION_PUBLIC;
import static com.apptn.incruste.GolbalUtilities.Utiles.DOCUMENT_PARTY;
import static com.apptn.incruste.GolbalUtilities.Utiles.DOCUMENT_PROFILE;
import static com.apptn.incruste.GolbalUtilities.Utiles.distance;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentPartys extends Fragment {

    HorizontalRcyclerAdapter adapter;
    HorizontalRcyclerAdapter adapter2;
    SharedPreferences sharedPreferences;
    User userObject;
    RecyclerView closeRecycler, farRecycler;
    RelativeLayout layoutNoParty, layoutNoPartysFar;
    int countApproxPartys;
    int countFurtherPartys;
    Typeface neutra2, neutraDemi;
    SwipeRefreshLayout swipePartys;
    boolean addition = false; // see if the call of the webService is for the first time or for asking more partys
    int cameFrom = 0;
    LocationManager mLocationManager;
    private LocationManager locationManager;


    public FragmentPartys() {

    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragment_partys, container, false);
        firebaseFirestore = FirebaseFirestore.getInstance();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        swipePartys = view.findViewById(R.id.swipePartys);
        TextView plus1 = view.findViewById(R.id.plus1);
        TextView plus2 = view.findViewById(R.id.plus2);
        neutra2 = Typeface.createFromAsset(getActivity().getAssets(), "Neutraface2Text-Book.otf");
        neutraDemi = Typeface.createFromAsset(getActivity().getAssets(), "Neutra Text Demi.otf");
        plus1.setTypeface(neutra2);
        plus2.setTypeface(neutra2);
        //if (!isLocationEnabled()) showAlert();
        Log.e("user", sharedPreferences.getString("user", "{}"));
        userObject = new Gson().fromJson(sharedPreferences.getString("user", "{}"), User.class);
        TextView text1 = view.findViewById(R.id.text1);
        TextView text2 = view.findViewById(R.id.text2);


        text1.setTypeface(neutraDemi);

        text2.setTypeface(neutraDemi);

        closeRecycler = view.findViewById(R.id.closeRecycler);
        layoutNoParty = view.findViewById(R.id.layoutNoPartys);
        layoutNoPartysFar = view.findViewById(R.id.layoutNoPartysFar);

        farRecycler = view.findViewById(R.id.farRecycler);

        LinearLayoutManager managerClose = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager managerFar = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        farRecycler.setLayoutManager(managerFar);
        closeRecycler.setLayoutManager(managerClose);
        adapter = new HorizontalRcyclerAdapter(getActivity(), 1);
        adapter2 = new HorizontalRcyclerAdapter(getActivity(), 2);
        closeRecycler.setAdapter(adapter);
        farRecycler.setAdapter(adapter2);

        FrameLayout near = view.findViewById(R.id.moreNear);
        FrameLayout far = view.findViewById(R.id.moreFar);
        webServicePartys(userObject);
        swipePartys.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                webServicePartys(userObject);
            }
        });
        near.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (countApproxPartys < 4) {
                } else {
                    cameFrom = 1;
                    addition = true;
                    webServicePartys(userObject);
                }

            }
        });

        far.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (countFurtherPartys < 4) {
                } else {
                    cameFrom = 2;
                    addition = true;
                    webServicePartys(userObject);
                }
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        webServicePartys(userObject);
    }

    final List<Party> closePartyList = new ArrayList<>();
    final List<Party> farPartyList = new ArrayList<>();

    @SuppressLint("MissingPermission")
    public void webServicePartys(final User user) {
        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(final Location location) {
                        // Got last known location. In some rare situations this can be null.
                        firebaseFirestore.collection(DOCUMENT_PARTY).addSnapshotListener(getActivity(), new EventListener<QuerySnapshot>() {
                            @Override
                            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                                closePartyList.clear();
                                farPartyList.clear();
                                List<Party> snapshots = new ArrayList<>();
                                if (queryDocumentSnapshots != null) {
                                    snapshots = queryDocumentSnapshots.toObjects(Party.class);
                                }
                                List<Party> actualPartys = new ArrayList<>();
                                if (snapshots.size() > 0) {
                                    for (Party party : snapshots) {
                                        if (Utiles.millisFromDate(party.getDateSoiree()) > System.currentTimeMillis()) {
                                            actualPartys.add(party);
                                        }
                                    }
                                }
                                if (actualPartys.size() > 0)
                                    for (Party party : actualPartys) {
                                        if (party.getConfidentiale().equals(""))
                                            if (location != null) {
                                                if (distance(location.getLatitude(), location.getLongitude(), party.getLatitude(), party.getLongitude()) < 10) {
                                                    closePartyList.add(party);
                                                } else {
                                                    farPartyList.add(party);
                                                }
                                            }
                                    }
                                try {
                                    if (swipePartys.isRefreshing())
                                        swipePartys.setRefreshing(false);
                                    if (addition) {
                                        if (cameFrom == 1) {
                                            if (closePartyList.size() == 0) {
                                                Toast.makeText(getActivity(), getString(R.string.noPartyToDisplay), Toast.LENGTH_LONG).show();
                                                addition = false;
                                            } else {
                                                Intent intent = new Intent(getActivity(), AllPartys.class);
                                                intent.putExtra("cameFrom", "addition");
                                                intent.putExtra("request", 100);
                                                intent.putExtra("dataJson", new Gson().toJson(closePartyList));
                                                if (getActivity() != null)
                                                    getActivity().startActivity(intent);

                                                addition = false;
                                            }
                                        } else if (cameFrom == 2) {

                                            if (farPartyList.size() == 0) {
                                                Toast.makeText(getActivity(), getString(R.string.noPartyToDisplay), Toast.LENGTH_LONG).show();
                                                addition = false;
                                            } else {
                                                Intent intent = new Intent(getActivity(), AllPartys.class);
                                                intent.putExtra("cameFrom", "addition");
                                                intent.putExtra("request", 200);
                                                intent.putExtra("dataJson", new Gson().toJson(farPartyList));
                                                Log.e("dataJson", new Gson().toJson(farPartyList));
                                                if (getActivity() != null)
                                                    getActivity().startActivity(intent);
                                                addition = false;
                                            }
                                        }

                                    } else {
                                        if (closePartyList.size() > 0) {
                                            countApproxPartys = closePartyList.size();
                                            adapter.fetchData(new JSONArray(new Gson().toJson(closePartyList)));
                                            closeRecycler.setVisibility(View.VISIBLE);
                                            layoutNoParty.setVisibility(View.GONE);
                                        } else {
                                            closeRecycler.setVisibility(View.GONE);
                                            layoutNoParty.setVisibility(View.VISIBLE);
                                        }

                                        if (farPartyList.size() > 0) {
                                            countFurtherPartys = farPartyList.size();
                                            adapter2.fetchData(new JSONArray(new Gson().toJson(farPartyList)));
                                            farRecycler.setVisibility(View.VISIBLE);
                                            layoutNoPartysFar.setVisibility(View.GONE);
                                        } else {
                                            farRecycler.setVisibility(View.GONE);
                                            layoutNoPartysFar.setVisibility(View.VISIBLE);
                                        }
                                    }

                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });

                    }
                });

    }

    FirebaseFirestore firebaseFirestore;

    //todo wtf?
    private void webServiceGetPartyAmb(final int ambID) {
        firebaseFirestore.collection(COLLECTION_PUBLIC).document("activities")
                .collection("ambiance").addSnapshotListener(getActivity(), new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                ambiances.addAll(queryDocumentSnapshots.toObjects(Ambiance.class));
                if (ambiances.size() == 0) {
                    Toast.makeText(getActivity(), getString(R.string.noPartysAmb), Toast.LENGTH_LONG).show();
                } else {
                    Intent intent = new Intent(getActivity(), AllPartys.class);
                    intent.putExtra("cameFrom", "ambiance");
                    intent.putExtra("jsonData", new Gson().toJson(ambiances));
                    if (getActivity() != null)
                        getActivity().startActivity(intent);
                }
            }
        });
    }

    final List<Ambiance> ambiances = new ArrayList<>();

    public class HorizontalRcyclerAdapter extends RecyclerView.Adapter<HorizontalRcyclerAdapter.myViewHolder> {

        private JSONArray data;
        int pos;
        Context ctx;


        HorizontalRcyclerAdapter(Context context, int position) {
            this.ctx = context;
            this.data = new JSONArray();
            this.pos = position;
        }


        void fetchData(JSONArray data1) {
            this.data = data1;
            this.notifyDataSetChanged();
        }


        @Override
        public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.inflation_partys_approx, parent, false);
            return new myViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final myViewHolder holder, int position) {

            try {
                System.out.println(data + "firasssss");
                holder.layoutPersons.bringToFront();
                holder.incustreHeart.bringToFront();
                holder.gradient.setBackground(Utiles.getColor());
                holder.gradient.bringToFront();
                Glide.with(ctx).load(data.getJSONObject(holder.getAdapterPosition()).getString("photo")).into(holder.imageParty);

                String nameParty = data.getJSONObject(holder.getAdapterPosition()).getString("name")
                        .substring(0, 1).toUpperCase() + data.getJSONObject(holder.getAdapterPosition()).getString("name").substring(1);
                holder.partyName.setText("#" + nameParty);
                FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
                firebaseFirestore.collection(DOCUMENT_PROFILE).document(data.getJSONObject(position).getString("user"))
                        .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                            @Override
                            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                                try {
                                    User user = documentSnapshot.toObject(User.class);
                                    String nameCreator = user.getNom();
                                    String lastNameCreator = user.getPrenom();
                                    holder.creator.setText(nameCreator.substring(0, 1).toUpperCase() + nameCreator.substring(1) + " " +
                                            lastNameCreator.substring(0, 1).toUpperCase() + lastNameCreator.substring(1));
                                } catch (Exception e1) {
                                    e.printStackTrace();
                                }
                            }
                        });
                String addr = data.getJSONObject(holder.getAdapterPosition()).getString("adr_public");
                holder.partyLocation.setText(addr.substring(0, 1).toUpperCase() + addr.substring(1));


                JSONArray arrayIncrusters = new JSONArray(String.valueOf(data.getJSONObject(holder.getAdapterPosition()).getJSONArray("incruster")));
                int count = 0;
                for (int i = 0; i < arrayIncrusters.length(); i++) {
                    try {
                        if (arrayIncrusters.getJSONObject(i).getInt("is_accepted") == 1) {
                            ++count;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                holder.nb_places.setText(String.valueOf(count));


                //I know that a bull shit
                List<String> tempAmb = new ArrayList<>();
                tempAmb.add(0, "https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fambiance%2F8eba541ca1d3e38032988e9c36c65ef2.jpg?alt=media&token=5fdd69c5-be52-486b-a3cd-34a6f6ca9ed5");
                tempAmb.add(1,"https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fambiance%2F4687bf6a1cd6a7babfce3a4ff6a7d155.jpg?alt=media&token=766c62fd-9485-4a2d-937d-51e62eb220e2");
                tempAmb.add(2,"https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fambiance%2F5777c4c0c59583cc32098b8ddc726b25.jpg?alt=media&token=d60316bd-2cea-4bd1-84ca-57eaa5611a61");
                tempAmb.add(3,"https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fambiance%2Fd57403b30d602547920a1fd16be7c0cf.jpg?alt=media&token=b0b9bb00-ab26-4630-bca3-2210742e4371");
                tempAmb.add(4,"https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fambiance%2Fd4f883eff93ba567fbe039c63b0e650e.jpg?alt=media&token=4b925fcd-8440-4f72-b3c9-5d8b4b45a912");
                tempAmb.add(5,"https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fambiance%2F6b8e18bbd41161986373600cb5b89714.jpg?alt=media&token=d546396f-9302-4b79-9343-326c638c9bbd");
                tempAmb.add(6,"https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fambiance%2Fsofa.png?alt=media&token=eafdc994-7ea2-4d6c-a800-2023bca01f86");


                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                JSONArray ambiancesPref = null;
                try {
                    ambiancesPref = new JSONArray(sharedPreferences.getString("JsonAmb", ""));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (ambiances.size() > 0) {
                    Glide.with(ctx).load(ambiancesPref.getJSONObject(data.getJSONObject(holder.getAdapterPosition())
                            .getInt("ambiance") - 1).getString("description")).into(holder.partyAmb);
                } else {
                    Glide.with(ctx).load(tempAmb.get(data.getJSONObject(holder.getAdapterPosition())
                            .getInt("ambiance") - 1)).into(holder.partyAmb);
                }

                //data.getJSONObject(holder.getAdapterPosition())
                //                            .getInt("ambiance") - 1).getString("description").toString()


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return data.length();
        }

        class myViewHolder extends RecyclerView.ViewHolder {

            Typeface proLigh = Typeface.createFromAsset(ctx.getAssets(), "SourceSansPro-Light.otf");
            Typeface proDemi = Typeface.createFromAsset(ctx.getAssets(), "SourceSansPro-Semibold.otf");

            ImageView partyAmb;
            ImageView incustreHeart;
            RoundedImageView imageParty, gradient;
            TextView partyName, creator, partyLocation, participant, nb_places, par;
            LinearLayout layoutPersons;


            public myViewHolder(View itemView) {
                super(itemView);
                participant = itemView.findViewById(R.id.participant);
                nb_places = itemView.findViewById(R.id.nb_places);
                imageParty = itemView.findViewById(R.id.imagePartyProx);
                partyAmb = itemView.findViewById(R.id.partyAmb);
                partyName = itemView.findViewById(R.id.partyName);
                nb_places.setTypeface(neutra2);
                partyName.setTypeface(neutra2);
                par = itemView.findViewById(R.id.par);
                par.setTypeface(proLigh);
                creator = itemView.findViewById(R.id.partyCreator);
                creator.setTypeface(proDemi);
                partyLocation = itemView.findViewById(R.id.partyLocation);
                partyLocation.setTypeface(proLigh);
                layoutPersons = itemView.findViewById(R.id.layoutPersons);
                gradient = itemView.findViewById(R.id.gradientAprox);
                incustreHeart = itemView.findViewById(R.id.incustreHeart);

                partyAmb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            webServiceGetPartyAmb(data.getJSONObject(getAdapterPosition()).getJSONObject("ambiance").getInt("id"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                incustreHeart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        try {
                            JSONArray arrayFav = Utiles.getFavoriteFromPrefrences(ctx);
                            if (arrayFav.length() > 0) {

                                boolean duplicated = false;
                                for (int i = 0; i < arrayFav.length(); i++) {

                                    if (data.getJSONObject(getAdapterPosition()).getString("id").equals(arrayFav.get(i))) {
                                        Utiles.deleteFromSharedPrefrences(ctx, data.getJSONObject(getAdapterPosition()).getString("id"));
                                        incustreHeart.setImageDrawable(ContextCompat.getDrawable(ctx, R.drawable.like_white));
                                        duplicated = true;
                                    }
                                }

                                if (!duplicated) {
                                    incustreHeart.setImageDrawable(ContextCompat.getDrawable(ctx, R.drawable.like_fill));
                                    Utiles.addFavoriteToPrefrences(ctx, data.getJSONObject(getAdapterPosition()).getString("id"));
                                }


                            } else {
                                incustreHeart.setImageDrawable(ContextCompat.getDrawable(ctx, R.drawable.like_fill));
                                Utiles.addFavoriteToPrefrences(ctx, data.getJSONObject(getAdapterPosition()).getString("id"));
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                imageParty.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(ctx, PartyDetails.class);

                        intent.putExtra("cameFrom", "partys");
                        try {
                            intent.putExtra("extraInfo", data.getJSONObject(getAdapterPosition()).toString());
                            Log.e("extraInfo", data.getJSONObject(getAdapterPosition()).toString());
                            ctx.startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
            }


        }


    }
}

