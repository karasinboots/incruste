package com.apptn.incruste.explorer;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.apptn.incruste.General.FootMenu;
import com.apptn.incruste.General.LoginActivity;
import com.apptn.incruste.GolbalUtilities.Utiles;
import com.apptn.incruste.Models.User;
import com.apptn.incruste.R;
import com.apptn.incruste.SearchPackage.SearchParty;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import org.json.JSONException;
import org.json.JSONObject;

import static com.apptn.incruste.GolbalUtilities.Utiles.DOCUMENT_PROFILE;
import static com.apptn.incruste.explorer.GpsListener.latitude;
import static com.apptn.incruste.explorer.GpsListener.longitude;


public class FirstActivty extends AppCompatActivity implements LocationListener {

    SharedPreferences preferences;
    JSONObject user;
    private LocationManager locationManager;
    private Location location;


    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_activity);
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                FirebaseFirestore.getInstance().collection(DOCUMENT_PROFILE)
                        .document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                final User user = task.getResult().toObject(User.class);
                                if (user == null) {
                                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().clear().commit();
                                    startActivity(new Intent(FirstActivty.this, LoginActivity.class));
                                    finish();
                                    return;
                                }
                                user.setToken(FirebaseInstanceId.getInstance().getToken());
                                FirebaseFirestore.getInstance().collection(DOCUMENT_PROFILE)
                                        .document(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                        .set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        Log.e("token", FirebaseInstanceId.getInstance().getToken());
                                        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("user", new Gson().toJson(user)).commit();
                                    }
                                });
                            }
                        });
            }
        });
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            showAlert();
            return;
        }
        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        if (isLocationEnabled()) {
            onLocationChanged(location);
        } else {
            final Handler handler = new Handler();

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                }
            }, 3000);
            showAlert();
        }

        Log.i("version", "" + Build.VERSION.SDK_INT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        try {
            user = new JSONObject(preferences.getString("user", "{}"));
            Utiles.showMessageLog("userData", "" + user.toString());
        } catch (JSONException jsx) {
            jsx.printStackTrace();
        }
        init();

    }

    @Override
    protected void onResume() {
        super.onResume();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

            }
        }, 3000);
        if (isLocationEnabled()) {
            /*if (Math.sqrt(longitude) < 0.0000001 || Math.sqrt(latitude) < 0.0000001)
                this.recreate();*/
        } else {
            //showAlert();
        }

    }


    private void init() {

        final ViewPager viewPager = findViewById(R.id.pager);
        viewPager.setAdapter(new SectionPagerAdapter(getSupportFragmentManager()));

        final SmartTabLayout tabLayout = findViewById(R.id.tab);
        tabLayout.setViewPager(viewPager);

        FootMenu menu = FootMenu.newInstance(0);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.containerHolder, menu);
        transaction.commit();


        LinearLayout trigger = findViewById(R.id.triggerSearch);
        trigger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FirstActivty.this, SearchParty.class));
                finish();
            }
        });


    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            longitude = location.getLongitude();
            latitude = location.getLatitude();
            //Toast.makeText(this, "" + latitude + "___" + longitude, Toast.LENGTH_LONG).show();
        } catch (Exception e) {

        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // Toast.makeText(this,""+status,Toast.LENGTH_LONG).show();

    }

    @Override
    public void onProviderEnabled(String provider) {
        onLocationChanged(location);

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);

        dialog.setTitle("Activer la géolocalisation")
                .setMessage("Vos paramètres de localisation sont désactivés.Veuillez activer l'emplacement pour " +
                        "utilisez cette application")
                .setPositiveButton("Paramètre de localisation", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                });

        dialog.show();
    }


    private class SectionPagerAdapter extends FragmentPagerAdapter {


        SectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            Fragment fragment = new Fragment();

            if (position == 0) {
                fragment = new FragmentPartys();
            } else if (position == 1) {
                fragment = new FragmentCategorys();
            }


            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Recommandé";
                case 1:
                    return "Ambiance";
            }
            return "";
        }
    }
}


