package com.apptn.incruste.explorer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apptn.incruste.GolbalUtilities.Utiles;
import com.apptn.incruste.Models.Ambiance;
import com.apptn.incruste.Models.Party;
import com.apptn.incruste.Models.User;
import com.apptn.incruste.R;
import com.bumptech.glide.Glide;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import static com.apptn.incruste.GolbalUtilities.Utiles.COLLECTION_PUBLIC;
import static com.apptn.incruste.GolbalUtilities.Utiles.DOCUMENT_PARTY;
import static com.apptn.incruste.GolbalUtilities.Utiles.distance;
import static com.apptn.incruste.GolbalUtilities.Utiles.millisFromDate;


public class FragmentCategorys extends Fragment {

    RecyclerView gridView;
    JSONArray object;
    User user;
    int position;
    SharedPreferences preferences;
    RecyclerGridAmb adapter;

    public FragmentCategorys() {
        // Required empty public constructor
    }

    @SuppressLint("MissingPermission")
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_categorys, container, false);
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        gridView = view.findViewById(R.id.grid);
        object = new JSONArray();
        adapter = new RecyclerGridAmb(getActivity());
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 2);
        gridView.setLayoutManager(manager);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getActivity(), R.dimen.gridSpacing);
        gridView.addItemDecoration(itemDecoration);
        gridView.setAdapter(adapter);
        user = new Gson().fromJson(preferences.getString("user", "{}"), User.class);
        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        adapter.getAmbWithPartysCounts(location);
                    }
                });
        return view;
    }

    FirebaseFirestore firebaseFirestore;
    final List<Party> partyList = new ArrayList<>();

    //todo wtf?
    @SuppressLint("MissingPermission")
    private void webServiceGetPartyAmb(final String ambID) {
        final List<Ambiance> ambiances = new ArrayList<>();
        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(final Location location) {
                        firebaseFirestore.collection(COLLECTION_PUBLIC).document("activities")
                                .collection("ambiance").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                ambiances.addAll(task.getResult().toObjects(Ambiance.class));
                                if (ambiances.size() == 0) {
                                    Toast.makeText(getActivity(), getString(R.string.noPartysAmb), Toast.LENGTH_LONG).show();
                                } else {
                                    firebaseFirestore.collection(DOCUMENT_PARTY).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                            partyList.clear();
                                            for (Party party : task.getResult().toObjects(Party.class)) {
                                                if (Utiles.millisFromDate(party.getDateSoiree()) > System.currentTimeMillis())
                                                    if (party.getAmbiance().equals(ambID))
                                                        if (location != null) {
                                                            if (distance(location.getLatitude(), location.getLongitude(), party.getLatitude(), party.getLongitude()) < 10) {
                                                                partyList.add(party);
                                                            }
                                                        } else if (distance(GpsListener.latitude, GpsListener.longitude, party.getLatitude(), party.getLongitude()) < 10) {
                                                            partyList.add(party);
                                                        }
                                            }
                                            for (Ambiance ambiance : ambiances) {
                                                if (ambiance.getId() == Integer.parseInt(ambID)) {
                                                    Log.e("ambiance", new Gson().toJson(partyList));
                                                    Intent intent = new Intent(getActivity(), AllPartys.class);
                                                    intent.putExtra("cameFrom", "ambiance");
                                                    intent.putExtra("jsonData", new Gson().toJson(partyList));
                                                    intent.putExtra("ambiance", new Gson().toJson(ambiance));
                                                    if (getActivity() != null)
                                                        getActivity().startActivity(intent);
                                                }
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
    }

    public class ItemOffsetDecoration extends RecyclerView.ItemDecoration {

        private int mItemOffset;

        public ItemOffsetDecoration(int itemOffset) {
            mItemOffset = itemOffset;
        }

        public ItemOffsetDecoration(@NonNull Context context, @DimenRes int itemOffsetId) {
            this(context.getResources().getDimensionPixelSize(itemOffsetId));
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            outRect.set(mItemOffset, mItemOffset, mItemOffset, mItemOffset);
        }
    }

    private class RecyclerGridAmb extends RecyclerView.Adapter<RecyclerGridAmb.ViewHolder> {

        Context ctx;
        JSONArray array;


        RecyclerGridAmb(Context context) {
            this.ctx = context;
            this.array = new JSONArray();
        }


        private void fetchData(JSONArray array) {

            this.array = array;
            notifyDataSetChanged();
        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.inflation_amb, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int i) {
            Typeface font = null;
            if (getActivity() != null) {
                font = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Regular.ttf");
            }
            holder.nameAmb.setTypeface(font);
            try {
                Glide.with(ctx).load(array.getJSONObject(i).getString("description")).into(holder.imageView);
                holder.nameAmb.setText(array.getJSONObject(i).getString("nom"));

                if (array.getJSONObject(i).getString("count").length() > 20)
                    holder.nb_party_byAmb.setText("20 +");
                else
                    holder.nb_party_byAmb.setText(array.getJSONObject(i).getString("count"));
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        @Override
        public int getItemCount() {
            return array.length();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            ImageView imageView;
            TextView nameAmb, nb_party_byAmb;

            public ViewHolder(View itemView) {
                super(itemView);
                imageView = itemView.findViewById(R.id.imgAmb);
                imageView.setColorFilter(Color.WHITE);
                nameAmb = itemView.findViewById(R.id.nameAmb);
                nb_party_byAmb = itemView.findViewById(R.id.nb_party_byAmb);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            webServiceGetPartyAmb(object.getJSONObject(getAdapterPosition()).getString("id"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }

        final List<Party> closePartyList = new ArrayList<>();

        private void getAmbWithPartysCounts(final Location location) {
            firebaseFirestore = FirebaseFirestore.getInstance();
            final List<Ambiance> ambiances = new ArrayList<>();
            firebaseFirestore.collection(COLLECTION_PUBLIC).document("activities")
                    .collection("ambiance")
                    .addSnapshotListener(getActivity(), new EventListener<QuerySnapshot>() {
                        @Override
                        public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                            ambiances.addAll(queryDocumentSnapshots.toObjects(Ambiance.class));
                            firebaseFirestore.collection(DOCUMENT_PARTY).addSnapshotListener(new EventListener<QuerySnapshot>() {
                                @Override
                                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                                    closePartyList.clear();
                                    if (queryDocumentSnapshots != null && !queryDocumentSnapshots.isEmpty())
                                        for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments()) {
                                            Party party = snapshot.toObject(Party.class);
                                            if (party.getConfidentiale().equals(""))
                                                if (millisFromDate(party.getDateSoiree()) > System.currentTimeMillis() && location != null) {
                                                    if (distance(location.getLatitude(), location.getLongitude(), party.getLatitude(), party.getLongitude()) < 10) {
                                                        closePartyList.add(party);
                                                    }
                                                }
                                        }
                                    for (Ambiance ambiance : ambiances) {
                                        for (Party party : closePartyList) {
                                            if (String.valueOf(ambiance.getId()).equals(party.getAmbiance())) {
                                                ambiance.setCounter(ambiance.getCounter() + 1);
                                            }
                                        }
                                    }
                                    try {
                                        fetchData(new JSONArray(new Gson().toJson(ambiances)));
                                        object = new JSONArray(new Gson().toJson(ambiances));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            });
                        }
                    });
        }
    }
}
