package com.apptn.incruste.Messages;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.apptn.incruste.CreationNewParty.NewParty;
import com.apptn.incruste.FireBase.FirebaseMessageSender;
import com.apptn.incruste.General.FootMenu;
import com.apptn.incruste.GolbalUtilities.Utiles;
import com.apptn.incruste.Models.Extra;
import com.apptn.incruste.Models.FirebaseMessage;
import com.apptn.incruste.Models.Group;
import com.apptn.incruste.Models.Incruster;
import com.apptn.incruste.Models.Message;
import com.apptn.incruste.Models.NotifyData;
import com.apptn.incruste.Models.Party;
import com.apptn.incruste.Models.User;
import com.apptn.incruste.R;
import com.apptn.incruste.explorer.FirstActivty;
import com.apptn.incruste.retrofit.NotifyKeyResult;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.apptn.incruste.GolbalUtilities.Utiles.COLLECTION_CONVERSATIONS;
import static com.apptn.incruste.GolbalUtilities.Utiles.COLLECTION_MESSAGES;
import static com.apptn.incruste.GolbalUtilities.Utiles.DOCUMENT_PARTY;
import static com.apptn.incruste.GolbalUtilities.Utiles.DOCUMENT_PROFILE;

public class Messages extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {

    TextView nameIncruste, nameParty, userDetails;
    CircleImageView userImage;
    RelativeLayout acceptReq, refuseReq;
    LinearLayout acceptProcess;
    RecyclerView itemsExtra, msgRecycler;
    EditText etMessage;
    ScrollView scrollView;
    AdapterMessages msgAdapter;
    User userLogged;
    SharedPreferences preferences;
    String recipient;
    IncrusterAdapter adapter;
    RelativeLayout dataFound;
    LinearLayout noDataFound;
    PopupMenu popup;
    RecyclerView recyclerIncruster;
    String demandeID;
    JSONArray arrayNotif;
    LinearLayoutManager incrustLayoutManager;
    Typeface neutraText;
    Typeface roboto;
    ImageView menu;
    boolean isRefreshing = false; // to know whether the refresh message is needed or not
    int positionAdapter = -1; // to refresh MessageAdapter
    List<Incruster> searchedIncrusts = new ArrayList<>();

    List<Incruster> incrusterList = new ArrayList<>();

    FirebaseFirestore firebaseFirestore;
    Incruster user_selected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        firebaseFirestore = FirebaseFirestore.getInstance();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        FootMenu menu = FootMenu.newInstance(4);
        View frameSendMSg = findViewById(R.id.frameSendMSg);
        frameSendMSg.bringToFront();
        neutraText = Typeface.createFromAsset(getAssets(), "Neutraface2Text-Book.otf");
        roboto = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        dataFound = findViewById(R.id.dataFound);
        noDataFound = findViewById(R.id.noDataFound);
        TextView vosIncruste = findViewById(R.id.vosIncruste);
        //vosIncruste.setTypeface(neutraText);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.containerMsgFooter, menu);
        transaction.commit();

        init();
    }

    private void init() {

        arrayNotif = new JSONArray();
        preferences = PreferenceManager.getDefaultSharedPreferences(Messages.this);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        nameIncruste = findViewById(R.id.nameIncruste);
        nameIncruste.setTypeface(neutraText);
        etMessage = findViewById(R.id.etMsg);
        etMessage.setTypeface(neutraText);
        scrollView = findViewById(R.id.message_scroll);
        acceptReq = findViewById(R.id.acceptReq);
        refuseReq = findViewById(R.id.refuseReq);
        userDetails = findViewById(R.id.incrusterDetails);
        userDetails.setTypeface(typeface);
        userImage = findViewById(R.id.imgUserIncruster);
        nameParty = findViewById(R.id.nameParty);
        menu = findViewById(R.id.menu_btn);
        nameParty.setTypeface(neutraText);

        acceptProcess = findViewById(R.id.acceptProcess);
        msgAdapter = new AdapterMessages(this);
        msgRecycler = findViewById(R.id.reyclerview_message_list);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        msgRecycler.setLayoutManager(manager);
        msgRecycler.setAdapter(msgAdapter);
        userLogged = new Gson().fromJson(preferences.getString("user", "{}"), User.class);

        adapter = new IncrusterAdapter(this);
        adapter.webServiceGetMyIncruster();
        recyclerIncruster = findViewById(R.id.incrusterRecycler);
        incrustLayoutManager = new LinearLayoutManager(this, LinearLayout.HORIZONTAL, false);

        recyclerIncruster.setLayoutManager(incrustLayoutManager);
        recyclerIncruster.setAdapter(adapter);

        menu.setVisibility(View.INVISIBLE);

        itemsExtra = findViewById(R.id.itemsExtrasRec);
        SearchView searchIncrust = findViewById(R.id.searchIncrust);

        searchIncrust.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.filter(newText);
                return true;
            }
        });

        searchIncrust.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                adapter.filter("");
                return false;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actions, menu);
        return true;
    }

    public void showPopup(View v) {
        popup = new PopupMenu(this, v);
        final MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.actions, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.supprimer:
                        webServiceDeleteIncruster(adapter.getIncrustID(positionAdapter));
                        return true;

                    default:
                        return false;
                }
            }
        });
        popup.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.supprimer:
                return true;
            case R.id.signaler:
                return true;
            default:
                return false;
        }
    }

    FirebaseMessageSender firebaseMessageSender;

    public void sendMessage(View view) {
        try {
            if (etMessage.getText().length() > 0)
                webServiceSendMessage(recipient, false, etMessage.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        etMessage.setText("");
    }

    public void scrollToEnd(){
        msgRecycler.scrollToPosition(msgAdapter.getItemCount() -1);
        scrollView.smoothScrollTo(0,scrollView.getBottom());
    }

    public void goToCreation(View view) {
        startActivity(new Intent(Messages.this, NewParty.class));
    }

    public void goToHome(View view) {
        startActivity(new Intent(Messages.this, FirstActivty.class));
    }

    public void showDetails(boolean broughtSomething, List<Integer> ExtraID) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(Messages.this, LinearLayoutManager.HORIZONTAL, false);
        itemsExtra.setLayoutManager(layoutManager);
        AdapterItems adapterItems = new AdapterItems(this, broughtSomething, ExtraID);
        try {
            adapterItems.fetchData(new JSONArray(PreferenceManager.getDefaultSharedPreferences(this).getString("arrayExtra", "[]")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        adapterItems.webServiceGetExtraType();
        itemsExtra.setAdapter(adapterItems);
    }

    private void webServiceSendMessage(final String userTO, final boolean accept, final String message) {
        Log.e("message text", message);
        final Message message1 = new Message();
        long dateCreation = System.currentTimeMillis();
        String id = String.valueOf(dateCreation);
        message1.setDatecreation(dateCreation);
        message1.setId(id);
        message1.setText(message);
        message1.setUserTo(userTO);
        if (accept)
            message1.setMtype(2);
        else message1.setMtype(5);
        message1.setIsRead(0);
        message1.setUserFrom(userLogged.getId());
        firebaseFirestore.collection(COLLECTION_CONVERSATIONS)
                .document(user_selected.getParty())
                .collection(COLLECTION_MESSAGES)
                .add(message1)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        firebaseFirestore.collection(COLLECTION_CONVERSATIONS)
                                .document(user_selected.getParty())
                                .collection(COLLECTION_MESSAGES)
                                .get()
                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                        msgAdapter.fetchData(task.getResult().toObjects(Message.class));//todo: to add sorting!
                                        msgAdapter.notifyItemInserted(msgAdapter.getItemCount());
                                        final FirebaseMessageSender firebaseMessageSender = new FirebaseMessageSender();
                                        firebaseMessageSender.getGroupKey("party_" + user_selected.getParty(), new NotifyKeyResult() {
                                            @Override
                                            public void onSuccess(final String notifyKey) {
                                                firebaseFirestore.collection(DOCUMENT_PROFILE)
                                                        .document(user_selected.getUser())
                                                        .get()
                                                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                                Group group = new Group();
                                                                group.setNotificationKeyName("party_" + user_selected.getParty());
                                                                group.setNotificationKey(notifyKey);
                                                                List<String> list = new ArrayList<>();
                                                                list.add(task.getResult().toObject(User.class).getToken());
                                                                group.setRegistrationIds(list);
                                                                if (accept)
                                                                    firebaseMessageSender.joinGroup(group, new NotifyKeyResult() {
                                                                        @Override
                                                                        public void onSuccess(final String notifyKey1) {
                                                                            new Handler().postDelayed(new Runnable() {
                                                                                @Override
                                                                                public void run() {
                                                                                    firebaseMessageSender.sendPush(new FirebaseMessage(notifyKey,
                                                                                            new NotifyData("message", new Gson().toJson(message1))));
                                                                                }
                                                                            }, 500);
                                                                        }

                                                                        @Override
                                                                        public void onFailure(String errorMsg) {

                                                                        }
                                                                    });
                                                                else
                                                                    firebaseMessageSender.sendPush(new FirebaseMessage(notifyKey,
                                                                            new NotifyData("message", new Gson().toJson(message1))));
                                                            }
                                                        });
                                            }

                                            @Override
                                            public void onFailure(String errorMsg) {
                                                Log.e("onFailure", errorMsg);
                                            }
                                        });
                                        scrollToEnd();
                                    }
                                });
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
            }
        });
    }

    void webServiceDeleteIncruster(final String incrust_id) {
        firebaseFirestore.collection(DOCUMENT_PARTY).document(user_selected.getParty()).get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        final Party party = task.getResult().toObject(Party.class);
                        if (!party.getUser().equals(userLogged.getId())) {
                            Toast.makeText(Messages.this, R.string.pas_votre, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        List<Incruster> incrusters = party.getIncruster();
                        final List<Incruster> incrustersFinal = new ArrayList<>();
                        for (Incruster incruster : incrusters) {
                            if (!incruster.getUser().equals(user_selected.getUser())) {
                                incrustersFinal.add(incruster);
                                Log.e("incruster", incruster.getUser() + "not equals" + userLogged.getId());
                            }
                        }
                        party.setRemain(party.getRemain() + 1);
                        party.setIncruster(incrustersFinal);
                        firebaseFirestore.collection(DOCUMENT_PARTY).document(party.getId())
                                .set(party).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                /*if (positionAdapter != -1) {
                                    adapter.notifyItemRemoved(positionAdapter);
                                }
                                updateMessages(null);*/
                                init();
                            }
                        });
                    }
                });
    }

    public class DateComparator implements Comparator<Message> {
        public int compare(Message left, Message right) {
            return left.getDatecreation().compareTo(right.getDatecreation());
        }
    }

    private class AdapterMessages extends RecyclerView.Adapter {

        final int MSG_SENT = 1;
        final int MSG_RECIEVED = 2;

        List<Message> messages;
        Party soireLoc;
        Context ctx;

        AdapterMessages(Context context) {
            this.ctx = context;
            this.messages = new ArrayList<>();
        }

        void soireeLocation(Party data) {
            this.soireLoc = data;
        }

        void fetchData(List<Message> msgs) {
            Log.e("fetchData", "fetch");
            if (msgs != null && msgs.size() > 0) {
                Collections.sort(msgs, new DateComparator());
                this.messages = msgs;
                notifyDataSetChanged();
            }
        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view;
            if (viewType == MSG_SENT) {
                view = getLayoutInflater().inflate(R.layout.inflation_message_sent, parent, false);
                return new SentMessageHolder(view);
            } else if (viewType == MSG_RECIEVED) {
                view = getLayoutInflater().inflate(R.layout.inflation_message_recived, parent, false);
                return new RecievedMessageHolder(view);
            }
            return null;
        }

        @Override
        public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
            final String userId;
            final String sender;
            userId = userLogged.getId();
            sender = messages.get(position).getUserFrom();
            try {
                switch (holder.getItemViewType()) {
                    case MSG_RECIEVED:
                        ((RecievedMessageHolder) holder).bind(messages.get(position).getText(),
                                Utiles.getTimeAndDate(messages.get(position).getDatecreation()));
                        break;
                    case MSG_SENT:
                        ((SentMessageHolder) holder).bind(messages.get(position).getText(),
                                Utiles.getTimeAndDate(messages.get(position).getDatecreation()));
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            firebaseFirestore.collection(DOCUMENT_PROFILE).document(sender)
                    .get()
                    .addOnCompleteListener(Messages.this, new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            final User userFrom = task.getResult().toObject(User.class);
                            try {
                                firebaseFirestore.collection(DOCUMENT_PROFILE)
                                        .document(messages.get(position).getUserTo())
                                        .get()
                                        .addOnCompleteListener(Messages.this, new OnCompleteListener<DocumentSnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                String imageSender = "";
                                                String imageReciever = "";
                                                User userTo = task.getResult().toObject(User.class);
                                                if (userId.equals(sender)) {
                                                    if (userTo.getAvatar().equals("facebook"))
                                                        imageReciever = "http://graph.facebook.com/" + userTo.getFacebookId() + "/picture?type=normal";
                                                    else imageReciever = userTo.getAvatar();
                                                    if (userFrom.getAvatar().equals("facebook"))
                                                        imageSender = "http://graph.facebook.com/" + userFrom.getFacebookId() + "/picture?type=normal";
                                                    else imageSender = userFrom.getAvatar();
                                                } else {
                                                    if (userTo.getAvatar().equals("facebook"))
                                                        imageSender = "http://graph.facebook.com/" + userTo.getFacebookId() + "/picture?type=normal";
                                                    else imageSender = userTo.getAvatar();
                                                    if (userFrom.getAvatar().equals("facebook"))
                                                        imageReciever = "http://graph.facebook.com/" + userFrom.getFacebookId() + "/picture?type=normal";
                                                    else imageReciever = userFrom.getAvatar();
                                                }
                                                try {
                                                    switch (holder.getItemViewType()) {
                                                        case MSG_RECIEVED:
                                                            ((RecievedMessageHolder) holder).bind(imageReciever);
                                                            break;
                                                        case MSG_SENT:
                                                            ((SentMessageHolder) holder).bind(imageSender);
                                                            break;
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
        }

        @Override
        public int getItemCount() {
            return messages.size();
        }


        @Override
        public int getItemViewType(int position) {
            int state = 0;
            String userId = "";
            String sender = "";
            userId = userLogged.getId();
            sender = messages.get(position).getUserFrom();
            if (messages.size() > 0) {
                if (userId.equals(sender))
                    state = MSG_SENT;
                else
                    state = MSG_RECIEVED;
            }
            return state;
        }

        class RecievedMessageHolder extends RecyclerView.ViewHolder {

            TextView msgRecieved, timeMsg;
            CircleImageView imageUserR;

            public RecievedMessageHolder(View itemView) {
                super(itemView);

                msgRecieved = itemView.findViewById(R.id.msgRecieved);
                msgRecieved.setTypeface(neutraText);
                imageUserR = itemView.findViewById(R.id.imageUserR);
                timeMsg = itemView.findViewById(R.id.timeMsgReci);
                timeMsg.setTypeface(roboto);

                msgRecieved.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        double lang = soireLoc.getLongitude();
                        double alt = soireLoc.getLatitude();

                        if (getAdapterPosition() == messages.size() - 1) {
                            Uri gmmIntentUri = Uri.parse("geo:" + alt + "," + lang);
                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                            mapIntent.setPackage("com.google.android.apps.maps");
                            startActivity(mapIntent);
                        }

                    }
                });
            }


            void bind(String imgUrl) {
                Log.e("imgUrl", imgUrl);
                Glide.with(ctx).load(imgUrl).into(imageUserR);
            }

            void bind(String message, String time) {
                msgRecieved.setText(message);
                timeMsg.setText(time);

            }
        }

        class SentMessageHolder extends RecyclerView.ViewHolder {

            TextView msgSent, timeMsg;
            CircleImageView imageUserS;
            Typeface neutraText = Typeface.createFromAsset(getAssets(), "Neutraface2Text-Book.otf");
            Typeface roboto = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");

            public SentMessageHolder(View itemView) {
                super(itemView);

                msgSent = itemView.findViewById(R.id.msgSent);
                msgSent.setTypeface(neutraText);
                imageUserS = itemView.findViewById(R.id.imageUserS);
                timeMsg = itemView.findViewById(R.id.timeMsgSent);
                timeMsg.setTypeface(roboto);




                if (messages.size() == 1) {
                    msgSent.setPaintFlags(msgSent.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                } else {
                    msgSent.setPaintFlags(0);
                    Log.d("TAG", messages.get(0).getText());
                }



                msgSent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        double lang = soireLoc.getLongitude();
                        double alt = soireLoc.getLatitude();



                        if (getAdapterPosition() == 0) {
                            Uri gmmIntentUri = Uri.parse("geo:" + alt + "," + lang);
                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                            mapIntent.setPackage("com.google.android.apps.maps");
                            startActivity(mapIntent);
                        }

                    }
                });
            }

            void bind(String imgUrl) {
                Glide.with(ctx).load(imgUrl).into(imageUserS);
            }

            void bind(String message, String time) {
                msgSent.setText(StringEscapeUtils.unescapeJava(message));
                timeMsg.setText(time);
            }
        }
    }

    private class IncrusterAdapter extends RecyclerView.Adapter<IncrusterAdapter.myViewHolder> {

        List<Incruster> data;
        Context context;
        User userSelf;
        List<Incruster> copyData;

        IncrusterAdapter(Context ctx) {
            this.data = new ArrayList<>();
            this.context = ctx;
        }

        private String getIncrustID(int position) {
            String incrusterID = "";
            incrusterID = data.get(position).getId();
            return incrusterID;
        }

        void fetchData(List<Incruster> array) {
            copyData = array;
            this.data = array;
            for (int i = 0; i < array.size(); i++) {
                JSONObject singleObject = new JSONObject();
                try {
                    singleObject.put("position", i);
                    singleObject.put("id", array.get(i).getId());
                    singleObject.put("is_accepted", array.get(i).getIsAccepted());//array.getJSONObject(i).getString("is_accepted"));
                    arrayNotif.put(singleObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            if (getIntent().hasExtra("cameFrom")) {
                if (getIntent().getExtras().getString("cameFrom").equals("notification")) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            openIncruste();
                        }
                    }, 100);
                }
            }
            notifyDataSetChanged();
        }

        private void openIncruste() {
            String id = getIntent().getExtras().getString("id");
            for (int i = 0; i < incrusterList.size(); i++) {
                if (incrusterList.get(i).getUser().equals(id)) {
                    performTheClick(i);
                }
            }
        }

        private void performTheClick(final int position) {
            try {
                recyclerIncruster.smoothScrollToPosition(arrayNotif.getJSONObject(position).getInt("position"));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            recyclerIncruster.findViewHolderForAdapterPosition(arrayNotif.getJSONObject(position).getInt("position")).itemView.performClick();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, 100);
            } catch (JSONException jsx) {
                jsx.printStackTrace();
            }
        }

        void filter(String name) {
            if (name.isEmpty()) {
                searchedIncrusts.clear();
                data = incrusterList;
                notifyDataSetChanged();
            } else {
                searchedIncrusts.clear();
                final String searchName = name.toLowerCase();
                for (final Incruster incruster : incrusterList) {
                    firebaseFirestore.collection(DOCUMENT_PROFILE)
                            .document(incruster.getUser())
                            .get()
                            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    try {
                                        User user = task.getResult().toObject(User.class);
                                        if (user.getNom().toLowerCase().contains(searchName)
                                                || user.getPrenom().toLowerCase().contains(searchName)) {
                                            searchedIncrusts.add(incruster);
                                        }
                                        data = searchedIncrusts;
                                        notifyDataSetChanged();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                }
            }
        }

        boolean hasElelent(JSONArray array, JSONObject obj) {
            boolean flag = false;
            for (int i = 0; i < array.length(); i++) {
                try {
                    if (array.getJSONObject(i).equals(obj)) {
                        flag = true;
                        break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return flag;
        }

        @Override
        public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.inflation_incruster_mesages, parent, false);

            return new myViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final myViewHolder holder, final int position) {
            userSelf = new Gson().fromJson(PreferenceManager.getDefaultSharedPreferences(context).getString("user", "{}"), User.class);
            if (data.size() > 0)
                firebaseFirestore.collection(DOCUMENT_PROFILE).document(data.get(position).getUser()).get()
                        .addOnCompleteListener(Messages.this, new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                User user = task.getResult().toObject(User.class);
                                if (user.getId().equals(userSelf.getId())) {
                                    holder.textView.setText(userSelf.getPrenom());
                                    holder.layout.setBackground(ContextCompat.getDrawable(context, R.drawable.creator_bouch));
                                    if (userSelf.getAvatar().equals("facebook"))
                                        Glide.with(context).load("http://graph.facebook.com/" + userSelf.getFacebookId() + "/picture?type=normal").into(holder.image);
                                    else
                                        Glide.with(context).load(userSelf.getAvatar()).into(holder.image);
                                } else {
                                    holder.layout.setBackground(null);
                                    holder.textView.setText(user.getPrenom());
                                    try {
                                        if (user.getAvatar().equals("facebook"))
                                            Glide.with(context).load("http://graph.facebook.com/" + user.getFacebookId() + "/picture?type=normal").into(holder.image);
                                        else
                                            Glide.with(context).load(user.getAvatar()).into(holder.image);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        private void webServiceSetMsgLu(final String demandID, final CircleImageView imageView) {
            /*StringRequest request = new StringRequest(Request.Method.POST, Utiles.GLOBAL_LINK_URL + "setMSGLu", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Utiles.showMessageLog("response", response);
                    notifyDataSetChanged();
                    // imageView.setVisibility(View.GONE);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utiles.showMessageLog("error", error.toString());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> header = new HashMap<>();
                    header.put("Authorization", "Bearer " + preferences.getString("token", ""));
                    return header;
                }

                @Override
                protected Map<String, String> getParams() {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("msg_id", "" + demandID);
                    return params;
                }
            };

            MySingleton.getInstance(context).addToRequestQueue(request);*/
        }

        private void webServiceGetMyIncruster() {
            FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
            firebaseFirestore.collection(DOCUMENT_PARTY).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    List<Party> partyList = task.getResult().toObjects(Party.class);
                    List<Party> myParties = new ArrayList<>();
                    Incruster selfIncruster = null;
                    List<Incruster> myIncrusters = new ArrayList<>();
                    for (Party party : partyList) {
                        for (Incruster incruster : party.getIncruster()) {
                            if (incruster.getUser().equals(userLogged.getId())) {
                                myParties.add(party);
                            }
                        }
                    }
                    if (myParties.size() > 0)
                        for (Party party1 : myParties) {
                            for (Incruster incruster : party1.getIncruster()) {
                                if (incruster.getUser().equals(userLogged.getId()))
                                    selfIncruster = incruster;
                            }
                            for (Incruster incruster : party1.getIncruster()) {
                                if ((!incruster.getUser().equals(userLogged.getId())
                                        && !incruster.getUser().equals(party1.getUser()))
                                        ||
                                        (!incruster.getUser().equals(userLogged.getId())
                                                && incruster.getUser().equals(party1.getUser())
                                                && selfIncruster != null
                                                && selfIncruster.getIsAccepted() == 1)) {
                                    Log.e("incruster", incruster.getUser() + " " + party1.getUser());
                                    myIncrusters.add(incruster);
                                }
                            }
                        }
                    if (myIncrusters.size() > 0) {
                        if (isRefreshing) {
                            acceptProcess.setVisibility(View.GONE);
                            msgRecycler.setVisibility(View.VISIBLE);
                            isRefreshing = false;
                        }
                        dataFound.setVisibility(View.VISIBLE);
                        noDataFound.setVisibility(View.GONE);
                        fetchData(myIncrusters);
                        incrusterList = myIncrusters;
                    } else {
                        noDataFound.setVisibility(View.VISIBLE);
                        dataFound.setVisibility(View.GONE);
                    }
                }
            });
        }

        private void webServiceAcceptAndDecline(final int status, final String id, final boolean isRefused, final int indexRow) {
            firebaseFirestore.collection(DOCUMENT_PARTY)
                    .document(user_selected.getParty())
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            final Party party = task.getResult().toObject(Party.class);
                            List<Incruster> newList = new ArrayList<>();
                            if (isRefused) {
                                adapter.notifyItemRemoved(indexRow);
                                acceptProcess.setVisibility(View.GONE);
                                nameIncruste.setVisibility(View.GONE);
                                nameParty.setVisibility(View.GONE);
                                for (Incruster incruster : party.getIncruster()) {
                                    if (!incruster.getUser().equals(user_selected.getUser()))
                                        newList.add(incruster); //todo check if conversation is exist
                                }
                                party.setRemain(party.getRemain() + 1);
                                party.setIncruster(newList);
                                firebaseFirestore.collection(DOCUMENT_PARTY)
                                        .document(user_selected.getParty())
                                        .set(party)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                webServiceGetMyIncruster();
                                            }
                                        });
                            } else {
                                for (Incruster incruster : party.getIncruster()) {
                                    if (incruster.getUser().equals(user_selected.getUser()))
                                        incruster.setIsAccepted(1);
                                }
                                firebaseFirestore.collection(DOCUMENT_PARTY)
                                        .document(user_selected.getParty())
                                        .set(party)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                webServiceGetMyIncruster();

                                                webServiceSendMessage(user_selected.getUser(), true,
                                                        getString(R.string.privatePlace)
                                                        + " " + party.getAdrComplete());
                                            }
                                        });
                            }
                        }
                    });
        }

        public class myViewHolder extends RecyclerView.ViewHolder {
            ImageView image;
            TextView textView;
            CoordinatorLayout layout;
            CircleImageView notficationIcon;
            Typeface neutra = Typeface.createFromAsset(getAssets(), "Neutraface2Text-Book.otf");

            myViewHolder(final View itemView) {
                super(itemView);

                image = itemView.findViewById(R.id.image);
                notficationIcon = itemView.findViewById(R.id.notficationIcon);
                textView = itemView.findViewById(R.id.name);
                textView.setTypeface(neutra);
                layout = itemView.findViewById(R.id.holderCoor);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //msgAdapter.fetchData(null);
                        positionAdapter = getAdapterPosition();
                        user_selected = data.get(getAdapterPosition());
                        menu.setVisibility(View.VISIBLE);
                        recipient = data.get(getAdapterPosition()).getUser();
                        for (int i = 0; i < recyclerIncruster.getChildCount(); i++) {
                            TextView name = recyclerIncruster.getChildAt(i).findViewById(R.id.name);
                            if (name.getTypeface() != null) {
                                if (name.getTypeface().getStyle() == Typeface.BOLD) {
                                    name.setTypeface(null, Typeface.NORMAL);
                                    name.setTextColor(ContextCompat.getColor(context, R.color.unSelectedTxt));
                                }
                            }
                        }
                        textView.setTextColor(ContextCompat.getColor(context, R.color.blueLight));
                        textView.setTypeface(null, Typeface.BOLD);
                        demandeID = data.get(getAdapterPosition()).getUser();
                        firebaseFirestore.collection(DOCUMENT_PARTY).document(data.get(getAdapterPosition()).getParty())
                                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                final Party party = task.getResult().toObject(Party.class);
                                firebaseFirestore.collection(DOCUMENT_PROFILE)
                                        .document(data.get(getAdapterPosition()).getUser()).get()
                                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                final User userObject = task.getResult().toObject(User.class);
                                                firebaseFirestore.collection(COLLECTION_CONVERSATIONS)
                                                        .document(user_selected.getParty())
                                                        .collection(COLLECTION_MESSAGES)
                                                        .get()
                                                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                                List<Message> messageList = task.getResult().toObjects(Message.class);
                                                                List<Message> currentMessages = new ArrayList<>();//todo: sorted messages
                                                                for (Message message : messageList) {
                                                                    if ((message.getUserTo().equals(userLogged.getId())
                                                                            && message.getUserFrom().equals(user_selected.getUser()))
                                                                            || (message.getUserTo().equals(user_selected.getUser())
                                                                            && message.getUserFrom().equals(userLogged.getId()))) {
                                                                        currentMessages.add(message);
                                                                    }
                                                                }
                                                                user_selected = data.get(getAdapterPosition());
                                                                boolean has_msg_unread = false;
                                                                if (currentMessages.size() > 0)
                                                                    for (int i = 0; i < currentMessages.size(); i++) {
                                                                        Message obj = currentMessages.get(i);
                                                                        if (obj.getIsRead() == 0) {
                                                                            has_msg_unread = true;
                                                                            break;
                                                                        }
                                                                    }
                                                                if (has_msg_unread) {
                                                                    //TODO wtf
                                                                    webServiceSetMsgLu(demandeID, notficationIcon);
                                                                }
                                                                String nameIn = userObject.getNom() + " " + userObject.getPrenom();
                                                                nameIncruste.setText(nameIn);
                                                                String namePar = party.getName();
                                                                nameParty.setText("#" + namePar.substring(0, 1).toUpperCase() + namePar.substring(1));

                                                                if (data.get(getAdapterPosition()).getIsAccepted() == 0) {
                                                                    List<Integer> itemID = new ArrayList<>();
                                                                    acceptProcess.setVisibility(View.VISIBLE);
                                                                    msgRecycler.setVisibility(View.GONE);
                                                                    List<Extra> array = data.get(getAdapterPosition()).getExtras();
                                                                    if (array.size() > 0) {
                                                                        for (int i = 0; i < array.size(); i++) {
                                                                            itemID.add(array.get(i).getId());
                                                                        }
                                                                        showDetails(true, itemID);
                                                                    } else
                                                                        showDetails(false, itemID);
                                                                    try {
                                                                        String timeForArrive = data.get(getAdapterPosition()).getTime();
                                                                        String newTime = timeForArrive.replace(':', 'h');
                                                                        String text = "<font color=#5ac8fa>" + newTime + "</font>";
                                                                        String string = userObject.getDateNaissance();
                                                                        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.FRANCE);
                                                                        Date date = format.parse(string);
                                                                        DateFormat ff = new SimpleDateFormat("dd-MM-yyyy", Locale.FRANCE);
                                                                        String new_sstr = ff.format(date);
                                                                        userDetails.setText(Html.fromHtml(userObject.getNom()
                                                                                + " " + userObject.getPrenom()
                                                                                + ", " + String.valueOf(Utiles.getAge(new_sstr)) + " "
                                                                                + getString(R.string.age) + ", "
                                                                                + "<br />" + getString(R.string.wantSub) +
                                                                                "<br />" + getString(R.string.forTime) + " "
                                                                                + text));
                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                    if (userObject.getAvatar().equals("facebook"))
                                                                        Glide.with(context).load("http://graph.facebook.com/" + userObject.getFacebookId() + "/picture?type=normal").into(userImage);
                                                                    else
                                                                        Glide.with(context).load(userObject.getAvatar()).into(userImage);
                                                                    userDetails.setTypeface(neutra);
                                                                    refuseReq.setOnClickListener(new View.OnClickListener() {
                                                                        @Override
                                                                        public void onClick(View view) {
                                                                            webServiceAcceptAndDecline(-1, data.get(getAdapterPosition()).getId(), true, getAdapterPosition());
                                                                        }
                                                                    });
                                                                    acceptReq.setOnClickListener(new View.OnClickListener() {
                                                                        @Override
                                                                        public void onClick(View view) {
                                                                            isRefreshing = true;
                                                                            webServiceAcceptAndDecline(1, data.get(getAdapterPosition()).getId(), false, getAdapterPosition());
                                                                        }
                                                                    });
                                                                } else if (data.get(getAdapterPosition()).getIsAccepted() == 1) {
                                                                    acceptProcess.setVisibility(View.GONE);
                                                                    msgRecycler.setVisibility(View.VISIBLE);
                                                                    msgAdapter.soireeLocation(party);
                                                                    msgAdapter.fetchData(currentMessages);
                                                                }
                                                            }
                                                        });
                                            }
                                        });
                            }
                        });
                    }
                });
            }
        }
    }

    private class AdapterItems extends RecyclerView.Adapter<AdapterItems.ViewHolder> {

        Context ctx;
        JSONArray allIems;
        JSONArray userItems;
        List<Integer> ids;
        boolean broughtExtra;

        private AdapterItems(Context ctx, boolean broughtSomething, List<Integer> ExtraID) {
            this.ctx = ctx;
            this.allIems = new JSONArray();
            this.userItems = new JSONArray();
            this.ids = ExtraID;
            this.broughtExtra = broughtSomething;
        }

        private void fetchData(JSONArray all) {

            this.allIems = all;
            notifyDataSetChanged();

        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.inflation_item_incruster, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            try {
                if (!broughtExtra)
                    Glide.with(ctx).load(allIems.getJSONObject(position).getString("description"))
                            .into(holder.imageView);
                else {
                    for (int i = 0; i < ids.size(); i++) {
                        if (ids.get(i) == allIems.getJSONObject(position).getInt("id")) {
                            holder.holderExtraBrought.setCardBackgroundColor(ContextCompat.getColor(ctx, R.color.blueLight));
                            Glide.with(ctx).load(allIems.getJSONObject(position).getString("description"))
                                    .into(holder.imageView);
                            holder.imageView.setColorFilter(ContextCompat.getColor(ctx, R.color.white));
                        } else {
                            Glide.with(ctx).load(allIems.getJSONObject(position).getString("description"))
                                    .into(holder.imageView);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return allIems.length();
        }

        public void webServiceGetExtraType() {

            /*final JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, Utiles.GLOBAL_LINK_URL + "getExtrasType", new JSONObject(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    try {
                        fetchData(response.getJSONArray("result"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("ErrorExtraID", "" + error.toString());
                }
            }) {

                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> header = new HashMap<>();
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);
                    header.put("Authorization", "Bearer " + sharedPreferences.getString("token", ""));
                    return header;
                }

            };

            MySingleton.getInstance(ctx).addToRequestQueue(request);*/
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            ImageView imageView;
            CardView holderExtraBrought;

            public ViewHolder(View itemView) {
                super(itemView);
                imageView = itemView.findViewById(R.id.imageItem);
                holderExtraBrought = itemView.findViewById(R.id.holderExtraBrought);
            }
        }
    }

}
