package com.apptn.incruste.ProfilePac;


import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.apptn.incruste.GolbalUtilities.Utiles;
import com.apptn.incruste.Models.Comment;
import com.apptn.incruste.Models.User;
import com.apptn.incruste.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import me.gujun.android.taggroup.TagGroup;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

import static com.apptn.incruste.GolbalUtilities.Utiles.DOCUMENT_PROFILE;

/**
 * A simple {@link Fragment} subclass.
 */


public class DialogAproposAndInteret extends DialogFragment {

    GetAboutAndInterst activiy;
    final int SET_ABOUT = 1;
    final int SET_INTREST = 2;
    final int ADD_COMMENT = 3;
    Profile profile;
    EditText description;
    TagGroup tagGroup;
    JSONObject dataUser;
    SharedPreferences sharedPreferences;
    List<String> tags;
    JSONArray dataArrived;
    List<String> partyNames;
    User user;
    TextView title;
    MaterialRatingBar materialRatingBar;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            activiy = (GetAboutAndInterst) context;
            profile = (Profile) context;
        } catch (ClassCastException cce) {
            cce.printStackTrace();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    public static DialogAproposAndInteret newInstance(int requestCode, JSONObject data, String cameFrom, JSONObject party) {
        DialogAproposAndInteret fragment = new DialogAproposAndInteret();
        Bundle bundle = new Bundle();
        bundle.putInt("requestCode", requestCode);
        bundle.putString("data", data.toString());
        bundle.putString("party", party.toString());
        bundle.putString("cameFrom", cameFrom);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static DialogAproposAndInteret newSelfProfileInstance(int requestCode, String cameFrom, JSONObject myPartys) {
        DialogAproposAndInteret fragment = new DialogAproposAndInteret();
        Bundle bundle = new Bundle();
        bundle.putInt("requestCode", requestCode);
        bundle.putString("cameFrom", cameFrom);
        bundle.putString("myPartys", myPartys.toString());
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_desc, container, false);
        description = v.findViewById(R.id.description);
        materialRatingBar = v.findViewById(R.id.ratingCom);


        title = v.findViewById(R.id.textDialog);
        tagGroup = v.findViewById(R.id.tagGroup);
//        Utiles.showMessageLog("dataArrived", "" + getArguments().getString("data"));

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        final Spinner spinnerPartys = v.findViewById(R.id.spinnerPartys);
        FrameLayout holderSpinnerParty = v.findViewById(R.id.holderSpinnerParty);
        final TextView namePartySpinner = v.findViewById(R.id.namePartySpinner);

        if (getArguments().getInt("requestCode") == ADD_COMMENT && getActivity() != null) {


            if (getArguments().getString("cameFrom").equals("selfProfile")) {
                try {
                    JSONObject myPartys = new JSONObject(getArguments().getString("myPartys"));
                    partyNames = new ArrayList<>();
                    namePartySpinner.setVisibility(View.GONE);
                    title.setText(getActivity().getString(R.string.selectParty));
                    spinnerPartys.setVisibility(View.VISIBLE);
                    materialRatingBar.setVisibility(View.VISIBLE);
                    dataArrived = myPartys.getJSONArray("result2");
                    for (int i = 0; i < dataArrived.length(); i++) {
                        partyNames.add("#" + dataArrived.getJSONObject(i).getString("name"));
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                            R.layout.costume_spinner, partyNames);

                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spinnerPartys.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            } else if (getArguments().getString("cameFrom").equals("checkIncruster")) {
                title.setText(getActivity().getString(R.string.selectParty));
                try {
                    JSONObject party = new JSONObject(getArguments().getString("party"));
                    Utiles.showMessageLog("party", "" + party.toString());
                    namePartySpinner.setText(party.getString("name"));
                    materialRatingBar.setVisibility(View.VISIBLE);

                } catch (JSONException jsx) {
                    jsx.printStackTrace();
                }

            }
            tagGroup.setVisibility(View.GONE);
            description.setVisibility(View.VISIBLE);
        }
        user = new Gson().fromJson(sharedPreferences.getString("user", ""), User.class);
        tags = user.getInteret();

        spinnerPartys.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    namePartySpinner.setText(dataArrived.getJSONObject(i).getString("name"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        RelativeLayout btnValider = v.findViewById(R.id.btnValider);

        if (getArguments().getInt("requestCode") == SET_ABOUT && getActivity() != null) {
            description.setVisibility(View.VISIBLE);
            spinnerPartys.setVisibility(View.GONE);
            namePartySpinner.setVisibility(View.GONE);
            title.setText(getActivity().getString(R.string.about));
            description.setText(user.getAbout());
        } else if (getArguments().getInt("requestCode") == SET_INTREST) {
            spinnerPartys.setVisibility(View.GONE);
            namePartySpinner.setVisibility(View.GONE);
            title.setText(getActivity().getString(R.string.intrest));
            if (tags != null && tags.size() > 0) {
                tagGroup.setTags(tags);
            }
            tagGroup.setVisibility(View.VISIBLE);
        }


        btnValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (getArguments().getInt("requestCode") == SET_ABOUT) {

                    webServiceAbout();
                    activiy.getAbout(description.getText().toString());
                    dismiss();
                } else if (getArguments().getInt("requestCode") == SET_INTREST) {
                    webServiceIntrest();
                } else if (getArguments().getInt("requestCode") == ADD_COMMENT) {
                    try {

                        if (getArguments().getString("cameFrom").equals("checkIncruster")) {
                            dataUser = new JSONObject(getArguments().getString("data"));
                            JSONObject party = new JSONObject(getArguments().getString("party"));
                            String user = dataUser.getString("user");
                            webServiceAddComment(user
                                    , materialRatingBar.getRating()
                                    , description.getText().toString()
                                    , party.getString("id")
                            );
                        } else if (getArguments().getString("cameFrom").equals("selfProfile")) {
                            webServiceAddComment(user.getId(), materialRatingBar.getRating()
                                    , description.getText().toString()
                                    , dataArrived.getJSONObject(spinnerPartys.getSelectedItemPosition()).getString("id")
                            );

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    dismiss();
                }


            }
        });
        return v;
    }

    private void webServiceAbout() {
        final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        final FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseFirestore.collection(DOCUMENT_PROFILE)
                .document(firebaseAuth.getCurrentUser().getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                final User user = task.getResult().toObject(User.class);
                if (description.getText().length() > 0)
                    user.setAbout(description.getText().toString());
                firebaseFirestore.collection(DOCUMENT_PROFILE).document(firebaseAuth.getCurrentUser().getUid())
                        .set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("user", new Gson().toJson(user));
                        editor.apply();

                    }
                });
            }
        });
    }


    private void webServiceIntrest() {
        final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        final FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseFirestore.collection(DOCUMENT_PROFILE)
                .document(firebaseAuth.getCurrentUser().getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                final User user = task.getResult().toObject(User.class);
                if (tagGroup.getTags().length > 0)
                    user.setInteret(new ArrayList<>(Arrays.asList(tagGroup.getTags())));
                firebaseFirestore.collection(DOCUMENT_PROFILE).document(firebaseAuth.getCurrentUser().getUid())
                        .set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("user", new Gson().toJson(user));
                        editor.apply();
                        activiy.getIntrest(new ArrayList<>(Arrays.asList(tagGroup.getTags())));
                        dismiss();
                    }
                });
            }
        });

    }

    private void webServiceAddComment(final String userTO, final float rate, final String comment, final String partyID) {
        Long id = System.currentTimeMillis();
        Comment comment1 = new Comment();
        comment1.setSoiree(partyID);
        comment1.setDatecreation(System.currentTimeMillis());
        comment1.setId(id);
        comment1.setRate((int) rate);
        comment1.setText(comment);
        comment1.setUserFrom(user.getId());
        comment1.setUserTo(userTO);
        FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseFirestore.collection("comments").add(comment1).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
            @Override
            public void onComplete(@NonNull Task<DocumentReference> task) {
                activiy.commentAdded();
                dismiss();
            }
        });
    }
}


