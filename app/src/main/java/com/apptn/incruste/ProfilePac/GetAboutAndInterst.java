package com.apptn.incruste.ProfilePac;

import java.util.List;

/**
 * Created by Administrateur on 21/11/2017.
 */

public interface GetAboutAndInterst {

    void getAbout(String about);

    void commentAdded();

    void getIntrest(List<String> tags);
}
