package com.apptn.incruste.ProfilePac;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.apptn.incruste.R;

import me.gujun.android.taggroup.TagGroup;

/**
 * Created by Administrateur on 27/11/2017.
 */

public class TagEditor extends AppCompatActivity{


    private TagGroup mTagGroup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag_edidtor);

        Bundle bundle = getIntent().getExtras();
        String[]tags = bundle.getStringArray("tags");
        mTagGroup = findViewById(R.id.tag_group);
        mTagGroup.setTags(tags);

        mTagGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTagGroup.submitTag();
            }
        });

    }





}
