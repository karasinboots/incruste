package com.apptn.incruste.ProfilePac;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apptn.incruste.General.FootMenu;
import com.apptn.incruste.GolbalUtilities.Utiles;
import com.apptn.incruste.Models.Comment;
import com.apptn.incruste.Models.Party;
import com.apptn.incruste.Models.User;
import com.apptn.incruste.R;
import com.apptn.incruste.Settings.Settings;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.Nullable;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

import static com.apptn.incruste.GolbalUtilities.Utiles.CHILD_IMAGES;
import static com.apptn.incruste.GolbalUtilities.Utiles.DOCUMENT_PARTY;
import static com.apptn.incruste.GolbalUtilities.Utiles.DOCUMENT_PROFILE;

public class Profile extends AppCompatActivity implements GetAboutAndInterst {

    TextView apropos, addComments;
    ImageView editIntrest, imageBig;
    GridView tagGroup;
    SharedPreferences preferences;
    List<String> tags = new ArrayList<>();
    String userId;
    User user, userLogged = null;
    JSONObject userAndParty;
    TextView loadMoreComments;
    CommentsAdapter adapter;
    IntrestRecycler intrestAdapter;
    MaterialRatingBar ratingBar;
    RecyclerView commentsRecycle;
    RoundedImageView imageSMall;
    int counterComments = 0;

    final int REQUEST_PROFILE = 1;
    final int REQUEST_COUVERTURE = 2;
    TextView noCommentsText;
    final int SET_ABOUT = 1;
    final int SET_INTREST = 2;
    final int ADD_COMMENT = 3;
    ImageView editApropos;
    ImageView editPenPic;
    ImageView settings;
    Typeface proReg, helvatica;
    FirebaseFirestore firebaseFirestore;
    FirebaseAuth firebaseAuth;
    FirebaseStorage firebaseStorage;
    /*
     * JsonObjects user and UserArray are the data for the this class "user" in case the user it self is visiting
     * his own profile
     * and the " userAndParty" in case a user is visiting another user Profile;
     *
     */

    public TextView getLoadMoreComments() {
        return loadMoreComments;
    }

    public TextView getAddComments() {
        return addComments;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_profile);
        FrameLayout frame = findViewById(R.id.frame);
        frame.bringToFront();
        frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseStorage = FirebaseStorage.getInstance();
        proReg = Typeface.createFromAsset(getAssets(), "SourceSansPro-Regular.otf");
        helvatica = Typeface.createFromAsset(getAssets(), "HelveticaNeue-Roman.otf");
        settings = findViewById(R.id.settings);
        settings.setColorFilter(ContextCompat.getColor(Profile.this, R.color.iconsColor));
        editPenPic = findViewById(R.id.editPenPic);
        editApropos = findViewById(R.id.editApropos);
        editIntrest = findViewById(R.id.editIntrest);
        addComments = findViewById(R.id.txtAddComment);

        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        loadMoreComments = findViewById(R.id.loadMoreComments);
        try {
            if (getIntent().getExtras().getString("cameFrom").equals("selfProfile")) {
                userId = new Gson().fromJson(preferences.getString("user", "{}"), User.class).getId();
                Log.e("userId", new Gson().fromJson(preferences.getString("user", "{}"), User.class).getId());
                if (getIntent().getExtras().getString("permission").equals("read")) {
                } else if (getIntent().getExtras().getString("permission").equals("write")) {
                    settings.setVisibility(View.VISIBLE);
                    editIntrest.setVisibility(View.VISIBLE);
                    editApropos.setVisibility(View.VISIBLE);
                    editPenPic.setVisibility(View.VISIBLE);
                    editApropos.bringToFront();
                    editIntrest.bringToFront();
                    settings.bringToFront();
                }
            } else if (getIntent().getExtras().getString("cameFrom").equals("checkIncruster")) {
                Log.e("extraInfo", getIntent().getExtras().getString("jsonData"));
                try {
                    if (getIntent().getExtras().getString("permission").equals("read")) {
                        userAndParty = new JSONObject(getIntent().getExtras().getString("party"));
                        try {
                            userId = new JSONObject(getIntent().getExtras().getString("jsonData")).getString("user");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        addComments.setVisibility(View.GONE);
                        editPenPic.setVisibility(View.GONE);
                    } else if (getIntent().getExtras().getString("permission").equals("write")) {
                        addComments.setVisibility(View.VISIBLE);
                        userAndParty = new JSONObject(getIntent().getExtras().getString("party"));
                        try {
                            userId = new JSONObject(getIntent().getExtras().getString("jsonData")).getString("user");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    Utiles.showMessageLog("logfromData", userAndParty.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        init();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            Uri selectedImageUri = data.getData();
            if (requestCode == REQUEST_PROFILE) {
                final String url = "setImage";

                Glide.with(this).asBitmap().load(selectedImageUri).into(new SimpleTarget<Bitmap>(100, 100) {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        changeProfileImages(url, resource);
                        imageSMall.setImageBitmap(resource);
                    }

                });

            } else if (requestCode == REQUEST_COUVERTURE) {
                final String url = "setCouverture";

                Glide.with(this).asBitmap().load(selectedImageUri).into(new SimpleTarget<Bitmap>(500, 300) {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {

                        changeProfileImages(url, resource);
                        imageBig.setImageBitmap(resource);
                    }

                });
            }

        }
    }

    public void init() {
        FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseFirestore.collection(DOCUMENT_PROFILE).document(userId)
                .addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                        user = documentSnapshot.toObject(User.class);
                        userLogged = documentSnapshot.toObject(User.class);
                        webserviceGetMyPartys();
                        populateProfile();
                        editIntrest.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                DialogAproposAndInteret fragment = DialogAproposAndInteret.newInstance(SET_INTREST, new JSONObject(), "edit", new JSONObject());
                                fragment.show(getSupportFragmentManager(), "");
                            }
                        });
                        editApropos.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                DialogFragment newFragment = DialogAproposAndInteret.newInstance(SET_ABOUT, new JSONObject(), "edit", new JSONObject());
                                newFragment.show(getSupportFragmentManager(), "");
                            }
                        });
                    }
                });


        editPenPic.setColorFilter(ContextCompat.getColor(Profile.this, R.color.white));

        TextView text1 = findViewById(R.id.text1);
        noCommentsText = findViewById(R.id.noCommentsText);

        TextView text2 = findViewById(R.id.text2);
        TextView text3 = findViewById(R.id.text3);

        text1.setTypeface(proReg);
        text2.setTypeface(proReg);
        text3.setTypeface(proReg);

        addComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i("comments Array", adapter.arrayComments.toString());

                for (int i = 0; i < adapter.arrayComments.size(); i++) {
                    if (userLogged.getId().equals(adapter.arrayComments.get(i).getUserFrom())) {

                        new android.app.AlertDialog.Builder(Profile.this)
                                .setTitle("OK")
                                .setMessage("Vous avez déja commenté.")
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                        return;
                    }
                }
                if (getIntent().getExtras().getString("cameFrom").equals("checkIncruster")) {
                    try {
                        DialogAproposAndInteret fragment = DialogAproposAndInteret.newInstance(ADD_COMMENT,
                                userAndParty, getIntent().getExtras().getString("cameFrom")
                                , new JSONObject(getIntent().getExtras().getString("party")));
                        fragment.show(getSupportFragmentManager(), "");
                    } catch (JSONException jsx) {
                        jsx.printStackTrace();
                    }

                } else if (getIntent().getExtras().getString("cameFrom").equals("selfProfile")) {

                    if (getIntent().getExtras().getString("permission").equals("write")) {

                        DialogAproposAndInteret fragment = DialogAproposAndInteret.newSelfProfileInstance(ADD_COMMENT,
                                getIntent().getExtras().getString("cameFrom")
                                , userAndParty);
                        fragment.show(getSupportFragmentManager(), "");

                    }
                }

            }
        });

        FootMenu menu = FootMenu.newInstance(3);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.containerProfile, menu);
        transaction.commit();


        settings.setColorFilter(ContextCompat.getColor(Profile.this, R.color.fullBlack));
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Profile.this, Settings.class));
            }
        });


    }

    private void populateProfile() {

        commentsRecycle = findViewById(R.id.commentsRecycle);
        tagGroup = findViewById(R.id.recyclerIntrest);

        imageBig = findViewById(R.id.imageBig);
        TextView age = findViewById(R.id.userAge);
        age.setTypeface(helvatica);
        imageSMall = findViewById(R.id.imageSMall);
        TextView nameLsUser = findViewById(R.id.nameLastName);
        nameLsUser.setTypeface(helvatica);
        ratingBar = findViewById(R.id.ratingProfile);
        apropos = findViewById(R.id.apropos);
        apropos.setTypeface(proReg);
        GridLayoutManager manager = new GridLayoutManager(this, 3);
        commentsRecycle.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CommentsAdapter(this);
        intrestAdapter = new IntrestRecycler(this, user.getInteret());
        tagGroup.setAdapter(intrestAdapter);
        commentsRecycle.setAdapter(adapter);
        loadMoreComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                counterComments++;
                if (getIntent().getExtras().getString("cameFrom").equals("selfProfile"))
                    adapter.webServiceGetComments();
                else if (getIntent().getExtras().getString("cameFrom").equals("checkIncruster"))
                    adapter.webServiceGetComments();
            }
        });

        if (getIntent().getExtras().getString("cameFrom").equals("selfProfile")) {

            try {
                //nameLsUser.setText(userLogged.getString("nom") + " " + userLogged.getString("prenom").toUpperCase().charAt(0) + ".");
                nameLsUser.setText(userLogged.getPrenom() + " " + userLogged.getNom());
                if (userLogged.getAvatar().equals("facebook"))
                    Glide.with(this).load("http://graph.facebook.com/" + userLogged.getFacebookId() + "/picture?type=normal").into(imageSMall);
                else Glide.with(this).load(userLogged.getAvatar()).into(imageSMall);
                String string = userLogged.getDateNaissance();
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.FRANCE);
                Date date = format.parse(string);
                DateFormat ff = new SimpleDateFormat("dd-MM-yyyy", Locale.FRANCE);
                String new_sstr = ff.format(date);
                age.setText(String.valueOf(Utiles.getAge(new_sstr)) + " ans");
                Glide.with(Profile.this).load(userLogged.getCouverture()).into(imageBig);
                adapter.webServiceGetComments();
                if (userLogged.getAbout() != null)
                    apropos.setText(userLogged.getAbout());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else if (getIntent().getExtras().getString("cameFrom").equals("checkIncruster")) {
            try {
                nameLsUser.setText(user.getPrenom() + " " + user.getNom());
                if (user.getAvatar().equals("facebook"))
                    Glide.with(this).load("http://graph.facebook.com/" + user.getFacebookId() + "/picture?type=normal").into(imageSMall);
                else Glide.with(this).load(user.getAvatar()).into(imageSMall);
                String string = user.getDateNaissance();
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.FRANCE);
                Date date = format.parse(string);
                DateFormat ff = new SimpleDateFormat("dd-MM-yyyy", Locale.FRANCE);
                String new_sstr = ff.format(date);
                age.setText(String.valueOf(Utiles.getAge(new_sstr)) + " ans");
                // age.setText(String.valueOf(Utiles.getAge(user.getString("date_naissance")) + getString(R.string.years)));
                Glide.with(Profile.this).load(user.getCouverture()).into(imageBig);
                adapter.webServiceGetComments();
                if (user.getAbout() != null)
                    apropos.setText(user.getAbout());
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

    }


    @Override
    public void getAbout(String about) {
        apropos.setText(about);
    }

    @Override
    public void commentAdded() {
        init();
    }

    @Override
    public void getIntrest(List<String> tags) {
        this.tags = tags;
        intrestAdapter = new IntrestRecycler(this, tags);
        tagGroup.setAdapter(intrestAdapter);
    }

    public void editProfileCover(View view) {


        if (!getIntent().getExtras().getString("cameFrom").equals("checkIncruster")) {

            int permissionCheck = ContextCompat.checkSelfPermission(Profile.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE);
            if (permissionCheck == PackageManager.PERMISSION_DENIED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(Profile.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {


                } else {

                    ActivityCompat.requestPermissions(Profile.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            1);


                }
            } else {
                Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                getIntent.setType("image/*");

                Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");

                Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});

                startActivityForResult(chooserIntent, REQUEST_COUVERTURE);
            }

        }

    }

    public void fetchData(JSONObject object) {
        userAndParty = object;
    }

    public void webserviceGetMyPartys() {
        firebaseFirestore.collection(DOCUMENT_PARTY).addSnapshotListener(this, new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (queryDocumentSnapshots != null && !queryDocumentSnapshots.isEmpty()) {
                    for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments()) {
                        Party party = snapshot.toObject(Party.class);
                        if (party.getUser().equals(userLogged.getId())) {
                            try {
                                fetchData(new JSONObject(new Gson().toJson(party)));
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        }
                    }

                }
            }
        });
    }

    public void editProfilePic(View view) {

        if (!getIntent().getExtras().getString("cameFrom").equals("checkIncruster")) {


            int permissionCheck = ContextCompat.checkSelfPermission(Profile.this,

                    Manifest.permission.READ_EXTERNAL_STORAGE);
            if (permissionCheck == PackageManager.PERMISSION_DENIED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(Profile.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {


                } else {

                    ActivityCompat.requestPermissions(Profile.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            1);


                }
            } else {
                Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                getIntent.setType("image/*");

                Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");

                Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});

                startActivityForResult(chooserIntent, REQUEST_PROFILE);
            }
        }

    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;

            // Add top margin only for the first item to avoid double space between items
            if (parent.getChildLayoutPosition(view) == 0) {
                outRect.top = space;
            } else {
                outRect.top = 0;
            }
        }
    }

    private void changeProfileImages(String link, final Bitmap bitmap) {
        if (link.equals("setCouverture"))
            firebaseFirestore.collection(DOCUMENT_PROFILE).document(firebaseAuth.getCurrentUser().getUid())
                    .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    final User user = task.getResult().toObject(User.class);
                    firebaseStorage.getReference().child(CHILD_IMAGES).child(firebaseAuth.getUid()).child("couverture").putBytes(getByteArray(bitmap))
                            .addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                                    task.getResult().getMetadata().getReference().getDownloadUrl()
                                            .addOnCompleteListener(new OnCompleteListener<Uri>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Uri> task) {
                                                    user.setCouverture(task.getResult().toString());
                                                    firebaseFirestore.collection(DOCUMENT_PROFILE)
                                                            .document(firebaseAuth.getCurrentUser().getUid()).set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(Profile.this).edit();
                                                            editor.putString("user", new Gson().toJson(user));
                                                            editor.apply();
                                                            Log.e("couverture", user.getCouverture());
                                                        }
                                                    });
                                                }
                                            });

                                }
                            });
                }
            });
        else if (link.equals("setImage"))
            firebaseFirestore.collection(DOCUMENT_PROFILE).document(firebaseAuth.getCurrentUser().getUid())
                    .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    final User user = task.getResult().toObject(User.class);
                    firebaseStorage.getReference().child(CHILD_IMAGES).child(firebaseAuth.getUid()).child("avatar").putBytes(getByteArray(bitmap))
                            .addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                                    task.getResult().getMetadata().getReference().getDownloadUrl()
                                            .addOnCompleteListener(new OnCompleteListener<Uri>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Uri> task) {
                                                    user.setAvatar(task.getResult().toString());
                                                    firebaseFirestore.collection(DOCUMENT_PROFILE)
                                                            .document(firebaseAuth.getCurrentUser().getUid()).set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(Profile.this).edit();
                                                            editor.putString("user", new Gson().toJson(user));
                                                            editor.apply();
                                                        }
                                                    });
                                                }
                                            });

                                }
                            });
                }
            });
    }

    private class IntrestRecycler extends BaseAdapter {


        private Context mContext;

        List<String> Tags;


        Typeface proReg = Typeface.createFromAsset(getAssets(), "SourceSansPro-Regular.otf");

        public IntrestRecycler() {

        }

        public IntrestRecycler(Context c, List<String> array) {
            mContext = c;
            Tags = array;

        }

   /*     public void getIntrest(String[] userTags) {

            this.intrestUser = userTags;
            notifyDataSetChanged();
        }*/

        @Override
        public int getCount() {
            int size;
            if (Tags != null)
                size = Tags.size();
            else
                size = 0;
            return size;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View vi = convertView;

            final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            vi = layoutInflater.inflate(R.layout.inflation_intrest, null);

            TextView tt = vi.findViewById(R.id.txtIntrest);
            tt.setTypeface(proReg);
            tt.setText(Tags.get(position));

            return vi;
        }

    }

    private class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.myViewHolder> {


        List<Comment> arrayComments = new ArrayList<>();

        Context ctx;
        List<Comment> array;
        JSONObject partysRating;
        Typeface proReg = Typeface.createFromAsset(getAssets(), "SourceSansPro-Regular.otf");
        Typeface neutra2 = Typeface.createFromAsset(getAssets(), "Neutraface2Text-Book.otf");
        FirebaseFirestore firebaseFirestore;

        public CommentsAdapter(Context context) {

            this.ctx = context;
            this.array = new ArrayList<>();
            this.partysRating = new JSONObject();

        }


        @Override
        public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.inflation_comment, parent, false);

            return new myViewHolder(view);
        }

        private void fetchData(List<Comment> data) {

            array = data;
            Utiles.showMessageLog("Ratring", "" + array.toString());
            notifyDataSetChanged();

        }

        @Override
        public void onBindViewHolder(final myViewHolder holder, final int pos) {

            User userObject = new User();
            if (getIntent().getExtras().getString("cameFrom").equals("selfProfile"))
                userObject = userLogged;
            else if (getIntent().getExtras().getString("cameFrom").equals("checkIncruster"))
                userObject = user;
            if (userObject.getId().equals(array.get(pos).getUserFrom())) {
                if (holder.tagOrganisatuer.getVisibility() != View.VISIBLE)
                    holder.tagOrganisatuer.setVisibility(View.VISIBLE);
                if (holder.tagIncruste.getVisibility() != View.GONE)
                    holder.tagIncruste.setVisibility(View.GONE);
            } else {
                if (holder.tagOrganisatuer.getVisibility() != View.GONE)
                    holder.tagOrganisatuer.setVisibility(View.GONE);
                if (holder.tagIncruste.getVisibility() != View.VISIBLE)
                    holder.tagIncruste.setVisibility(View.VISIBLE);
            }
            firebaseFirestore.collection(DOCUMENT_PROFILE).document(array.get(pos).getUserFrom()).get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            User user = task.getResult().toObject(User.class);
                            holder.nameEditor.setText(user.getNom() + " " + user.getPrenom());
                        }
                    });
            try {
                holder.nameParty.setText(userAndParty.getString("name"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            holder.textComment.setText(array.get(pos).getText());
            float rate = array.get(pos).getRate();
            holder.ratingComment.setRating(rate);
        }

        @Override
        public int getItemCount() {
            return array.size();
        }

        private void webServiceGetComments() {
            firebaseFirestore = FirebaseFirestore.getInstance();
            firebaseFirestore.collection("comments")
                    .whereEqualTo("userTo", userId)
                    .addSnapshotListener(Profile.this, new EventListener<QuerySnapshot>() {
                        @Override
                        public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                            List<Comment> comments = queryDocumentSnapshots.toObjects(Comment.class);
                            if (comments.size() > 0) {
                                getLoadMoreComments().setVisibility(View.VISIBLE);
                                if (getIntent().getExtras().getString("cameFrom").equals("checkIncruster")
                                        && getIntent().getExtras().getString("permission").equals("write"))
                                    getAddComments().setVisibility(View.VISIBLE);
                                int totalRate = 0;
                                try {
                                    for (int i = 0; i < comments.size(); i++) {
                                        totalRate += comments.get(i).getRate();
                                    }
                                    float rate = totalRate / comments.size();
                                    ratingBar.setRating(rate);
                                    ratingBar.setVisibility(View.VISIBLE);
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }
                                fetchData(comments);
                            } else {
                                noCommentsText.setVisibility(View.VISIBLE);
                                commentsRecycle.setVisibility(View.GONE);
                                getLoadMoreComments().setVisibility(View.GONE);
                                if (getIntent().getExtras().getString("cameFrom").equals("checkIncruster")
                                        && getIntent().getExtras().getString("permission").equals("write"))
                                    addComments.setVisibility(View.VISIBLE);
                                else
                                    addComments.setVisibility(View.GONE);
                            }

                        }
                    });
            if (getIntent().getExtras().getString("cameFrom").equals("checkIncruster")
                    && getIntent().getExtras().getString("permission").equals("write"))
                addComments.setVisibility(View.VISIBLE);
            else
                addComments.setVisibility(View.GONE);
        }

        class myViewHolder extends RecyclerView.ViewHolder {

            TextView nameParty, nameEditor, textComment;
            RelativeLayout tagIncruste, tagOrganisatuer;
            RatingBar ratingComment;


            myViewHolder(View itemView) {
                super(itemView);
                nameParty = itemView.findViewById(R.id.nameParty);
                nameParty.setTypeface(proReg);
                nameEditor = itemView.findViewById(R.id.nameEditor);
                nameEditor.setTypeface(neutra2);
                textComment = itemView.findViewById(R.id.textComment);
                textComment.setTypeface(proReg);
                tagIncruste = itemView.findViewById(R.id.tagIncruste);
                tagOrganisatuer = itemView.findViewById(R.id.tagOrganisatuer);
                ratingComment = itemView.findViewById(R.id.ratingComment);
//                Drawable progress = ratingComment.getProgressDrawable();
//                DrawableCompat.setTint(progress, ContextCompat.getColor(ctx, R.color.starYellow));

            }
        }
    }

    private byte[] getByteArray(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();

        return byteArray;
    }

}
