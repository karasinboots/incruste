package com.apptn.incruste.GolbalUtilities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.util.Log;

import com.apptn.incruste.General.LoginActivity;
import com.apptn.incruste.Models.Ambiance;
import com.apptn.incruste.Models.ExtrasId;
import com.apptn.incruste.R;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

import javax.annotation.Nullable;

import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;


public class Utiles {

    /*
        public static String GLOBAL_LINK_URL = "http://incruste.app.tn/ws/";
        public static String GLOBAL_IMAGES_URL = "http://www.incruste.app.tn/assets/party/";
        public static String GLOBAL_IMG_PROFILE_URL = "http://www.incruste.app.tn/assets/avatars/";
        public static String GLOBAL_LINK_TOKEN = "http://incruste.app.tn/oauth/v2/token?grant_type=client_credentials&client_id=1_3bcbxd9e24g0gk4swg0kwgcwg4o8k8g4g888kwc44gcc0gwwk4&client_secret=4ok2x70rlfokc8g0wws8c8kwcokw80k44sg48goc0ok4w0so0k";
    */
    public static String DOCUMENT_PROFILE = "profile";
    public static String DOCUMENT_PARTY = "party";
    public static String COLLECTION_CONVERSATIONS = "conversations";
    public static String COLLECTION_MESSAGES = "messages";
    public static String COLLECTION_PUBLIC = "public";
    public static String CHILD_IMAGES = "images";
    //   public static String GLOBAL_IMG_PROFILE_URL = "";


    public static void getAmbiance(final Context context) {
        FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseFirestore.collection(COLLECTION_PUBLIC)
                .document("activities")
                .collection("ambiance")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        try {
                            JSONArray array = new JSONArray(new Gson().toJson(queryDocumentSnapshots.toObjects(Ambiance.class)));
                            Log.d("MyNewThemeIDsFromList", "" + array.toString());
                            List<JSONObject> list = new ArrayList<>();
                            for (int i = 0; i < array.length(); i++) {
                                list.add(array.getJSONObject(i));
                            }
                            JSONObject ThemeIds = new JSONObject();
                            for (int i = 0; i < list.size(); i++) {
                                ThemeIds.put(list.get(i).getString("nom"), list.get(i).getString("id"));
                            }
                            editor.putString("arrayIDs", ThemeIds.toString());
                            editor.putString("JsonAmb", array.toString());
                            editor.apply();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            PreferenceManager.getDefaultSharedPreferences(context).edit().clear().commit();
                            context.startActivity(new Intent(context, LoginActivity.class));
                        }
                    }
                });
    }

    public static void getExtraType(final Context context) {
        FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
        final List<ExtrasId> extra = new ArrayList<>();
        firebaseFirestore.collection(COLLECTION_PUBLIC).document("themes")
                .collection("themes").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                try {
                    extra.addAll(queryDocumentSnapshots.toObjects(ExtrasId.class));
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    JSONArray array = new JSONArray(new Gson().toJson(extra));
                    Log.d("array", "" + array.toString());
                    JSONObject extra = new JSONObject();
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject temp = array.getJSONObject(i);
                        extra.put(temp.getString("name"), temp.getString("id"));
                    }
                    editor.putString("extraIDs", extra.toString());
                    editor.putString("arrayExtra", array.toString());
                    editor.apply();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    PreferenceManager.getDefaultSharedPreferences(context).edit().clear().commit();
                    context.startActivity(new Intent(context, LoginActivity.class));
                }
            }
        });
    }

    public static double distance(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371; // in miles, change to 6371 for kilometer output
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = earthRadius * c;
        return dist;
    }

    public static void showMessageLog(String tag, String message) {
        Log.i(tag, message);
    }

    public static GradientDrawable getColor() {
        GradientDrawable gradientDrawable;


        Random rnd = new Random();
        int color = Color.rgb(rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));


        gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM
                , new int[]{Color.TRANSPARENT, color});

        return gradientDrawable;
    }


    public static String formatDate(String date) {


        SimpleDateFormat inputFmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        TimeZone timeZone = TimeZone.getTimeZone("GMT");
        inputFmt.setTimeZone(timeZone);

        String newDate = "";
        try {
            Date date1 = inputFmt.parse(date);

            SimpleDateFormat output = new SimpleDateFormat("d MMM yyyy'T'HH:mm:ss Z");
            newDate = output.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return newDate;

    }

    public static long millisFromDate(String date) {
        SimpleDateFormat inputFmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        TimeZone timeZone = TimeZone.getTimeZone("GMT");
        inputFmt.setTimeZone(timeZone);
        try {
            Date date1 = inputFmt.parse(date);
            return date1.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static String formatDateNew(String date) {


        SimpleDateFormat inputFmt = new SimpleDateFormat("dd-MM-yyyy HH:mm a", Locale.FRANCE);
//        TimeZone timeZone = TimeZone.getTimeZone("GMT");
//        inputFmt.setTimeZone(timeZone);

        String newDate = "";
        try {
            Date date1 = inputFmt.parse(date);
            SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
            newDate = output.format(date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return newDate;

    }

    public static void addFavoriteToPrefrences(Context ctx, String party_ID) {

//        List<String> idsToSend = new ArrayList<>();
//        String[] partysGet = {};
//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
//        SharedPreferences.Editor editor = preferences.edit();
//        String strPartys = preferences.getString("partysFav","empty");
//
//        if (!strPartys.equals("empty")){
//
//            partysGet = strPartys.split(",");
//        }
//
//        StringBuilder sb = new StringBuilder();
//
//        if (partysGet.length>0){
//            for (int i = 0; i < partysGet.length; i++) {
//                sb.append(partysGet[i]).append(",");
//
//            }
//        }
//
//
//        sb.append(party_ID).append(",");
//
//        editor.putString("partysFav",sb.toString());
//        editor.apply();
//        partysGet = sb.toString().split(",");
//        idsToSend.addAll(Arrays.asList(partysGet));
//
//        showMessageLog("FavStored",""+idsToSend.toString());
//
//
//        List<String> favs = new ArrayList<>();
        boolean duplicated = false;

        try {


            JSONArray favs = getFavoriteFromPrefrences(ctx);
            JSONObject object = new JSONObject();
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
            SharedPreferences.Editor editor = preferences.edit();

            if (favs.length() > 0) {

                for (int i = 0; i < favs.length(); i++) {

                    if (favs.get(i).equals(party_ID)) {
                        favs.remove(i);
                        duplicated = true;
                    }
                }

                if (!duplicated)
                    favs.put(party_ID);


                object.put("favoris", favs);
                showMessageLog("favvvv", object.toString());
                editor.putString("partysFav", object.toString());
                editor.apply();

            } else {

                favs.put(party_ID);
                object.put("favoris", favs);
                showMessageLog("favvvv", object.toString());
                editor.putString("partysFav", object.toString());
                editor.apply();
            }


        } catch (JSONException jsx) {
            jsx.printStackTrace();
        }


    }

    public static JSONArray getFavoriteFromPrefrences(Context ctx) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        JSONArray idsToSend = new JSONArray();
        String favTest = preferences.getString("partysFav", "empty");
        showMessageLog("favTest", favTest);
        if (!favTest.equals("empty")) {
            try {
                JSONObject object = new JSONObject(favTest);
                idsToSend = object.getJSONArray("favoris");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return idsToSend;
    }

    public static void deleteFromSharedPrefrences(Context ctx, String idPartyFav) {

        JSONArray idsToSend = new JSONArray();
        JSONObject object = new JSONObject();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        String strPartys = preferences.getString("partysFav", "empty");

        showMessageLog("String", "emp...?" + strPartys);
        try {
            if (!strPartys.equals("empty")) {
                idsToSend = new JSONObject(strPartys).getJSONArray("favoris");
            }

            for (int i = 0; i < idsToSend.length(); i++) {

                if (idsToSend.getString(i).equals(idPartyFav))
                    idsToSend.remove(i);
            }


            object.put("favoris", idsToSend);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("partysFav", "" + object.toString());
        editor.apply();
    }

    public static List<String> getTimeAndDate(String date) {
        List<String> timeDate = new ArrayList<>();
        String formattedDate = formatDate(date);

        String[] parts = formattedDate.split("T");
        timeDate.add(parts[0]);
        timeDate.add(parts[1].substring(0, Math.min(parts[1].length(), 5)));
        return timeDate;
    }

    public static String getTimeAndDate(Long date) {
        return DateFormat.format("HH:mm", new Date(date)).toString();
    }
  /*  public static int getAge(String dateOfBirth) {

        SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy,MM,dd");
        Date date = null;
        try {

            String reformattedStr = myFormat.format(fromUser.parse(dateOfBirth));
            date = myFormat.parse(reformattedStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        Calendar today = Calendar.getInstance();
        Calendar birthDate = Calendar.getInstance();
        birthDate.setTime(date);
        if (birthDate.after(today)) {
            throw new IllegalArgumentException("You don't exist yet");
        }
        int todayYear = today.get(YEAR);
        int birthDateYear = birthDate.get(YEAR);
        int todayDayOfYear = today.get(Calendar.DAY_OF_YEAR);
        int birthDateDayOfYear = birthDate.get(Calendar.DAY_OF_YEAR);
        int todayMonth = today.get(MONTH);
        int birthDateMonth = birthDate.get(MONTH);
        int todayDayOfMonth = today.get(Calendar.DAY_OF_MONTH);
        int birthDateDayOfMonth = birthDate.get(Calendar.DAY_OF_MONTH);
        int age = todayYear - birthDateYear;

        // If birth date is greater than todays date (after 2 days adjustment of leap year) then decrement age one year
        if ((birthDateDayOfYear - todayDayOfYear > 3) || (birthDateMonth > todayMonth)) {
            age--;

            // If birth date and todays date are of same month and birth day of month is greater than todays day of month then decrement age
        } else if ((birthDateMonth == todayMonth) && (birthDateDayOfMonth > todayDayOfMonth)) {
            age--;
        }
        return age;
    }*/

    public static int getAge(String birthDate) {
        Calendar today = Calendar.getInstance();

        String myFormat = "dd-MM-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        Calendar b = null;
        try {
            b = getCalendar(sdf.parse(birthDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int age = today.get(Calendar.YEAR) - b.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < b.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();

        return age;
    }

    /*public static int getAge(String birthDate) {
        String myFormat = "dd-MM-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        Calendar today = Calendar.getInstance();
        Calendar b = null;
        try {
            b = getCalendar(sdf.parse(birthDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int diff =  today.get(YEAR) - b.get(YEAR);
        if (today.get(MONTH) > b.get(MONTH) ||
                (today.get(MONTH) == b.get(MONTH) && today.get(DATE) > b.get(DATE))) {
            diff--;
        }
        return diff;
    }
*/
    public static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.FRANCE);
        cal.setTime(date);
        return cal;
    }

    public static boolean checkAge(String dateOfBirth) {

        boolean result = false;
        SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy,MM,dd");
        Date date = null;
        try {

            String reformattedStr = myFormat.format(fromUser.parse(dateOfBirth));
            date = myFormat.parse(reformattedStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        Calendar today = Calendar.getInstance();
        Calendar birthDate = Calendar.getInstance();
        birthDate.setTime(date);
        if (birthDate.after(today)) {
            throw new IllegalArgumentException("You don't exist yet");
        }
        int todayYear = today.get(YEAR);
        int birthDateYear = birthDate.get(YEAR);
        int todayDayOfYear = today.get(Calendar.DAY_OF_YEAR);
        int birthDateDayOfYear = birthDate.get(Calendar.DAY_OF_YEAR);
        int todayMonth = today.get(MONTH);
        int birthDateMonth = birthDate.get(MONTH);
        int todayDayOfMonth = today.get(Calendar.DAY_OF_MONTH);
        int birthDateDayOfMonth = birthDate.get(Calendar.DAY_OF_MONTH);
        int age = todayYear - birthDateYear;

        // If birth date is greater than todays date (after 2 days adjustment of leap year) then decrement age one year
        if ((birthDateDayOfYear - todayDayOfYear > 3) || (birthDateMonth > todayMonth)) {
            age--;

            // If birth date and todays date are of same month and birth day of month is greater than todays day of month then decrement age
        } else if ((birthDateMonth == todayMonth) && (birthDateDayOfMonth > todayDayOfMonth)) {
            age--;
        }

        showMessageLog("age ", "age is " + age);
        if (age >= 18)
            result = true;
        else if (age < 18)
            result = false;

        return result;
    }


    public static void webServiceAddDevice(Context context, String userId, final SharedPreferences sharedPreferences) {
        //TODO make push messaging

    }


    public static void showVolleyError(Context context) {

        if (context != null) {

            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(context.getResources().getString(R.string.oups))
                    .setMessage(context.getResources().getString(R.string.error))
                    .setPositiveButton(context.getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.dismiss();
                        }
                    }).setNegativeButton(context.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.setCancelable(false);
            dialog.show();

        }
    }


    public static String saveToInternalStorage(Context context, Bitmap bitmapImage) {
        ContextWrapper cw = new ContextWrapper(context);
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("shareTemp", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, "imgShare.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }

    public static String getDateFromMs(long milliSeconds, String dateFormat) {

        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static void longLogShow(String string, String TAG) {

        int maxLogSize = 1000;
        for (int i = 0; i <= string.length() / maxLogSize; i++) {
            int start = i * maxLogSize;
            int end = (i + 1) * maxLogSize;
            end = end > string.length() ? string.length() : end;
            Utiles.showMessageLog(TAG, "" + string.substring(start, end));
        }
    }
}
