package com.apptn.incruste.myPartys;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.apptn.incruste.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MsgAlertFragment extends Fragment {

    final  int MSG_ERROR = 1;
    final  int MSG_SUCCESS = 2;
    final  int MSG_ACCPTED = 3;
    final  int MSG_REQUEST = 4;
    ImageView closeDialog;
    KillFragment killFragment ;


    public MsgAlertFragment() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            killFragment = (PartyDetails) context;
        }catch (ClassCastException cce){
            cce.printStackTrace();
        }
    }

    public static  MsgAlertFragment newInstance (int wich){

        MsgAlertFragment fragment = new MsgAlertFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("wich",wich);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_msg_alert, container, false);
        FrameLayout containerAlert = view.findViewById(R.id.containerAlert);
        TextView alert = view.findViewById(R.id.msgAlert);
        closeDialog = view.findViewById(R.id.closeDialog);

        switch (getArguments().getInt("wich")){

            case MSG_ERROR:
                if (getActivity()!= null) {
                    containerAlert.setBackgroundColor(getActivity().getResources().getColor(R.color.pinky));
                    alert.setText(getActivity().getResources().getString(R.string.errorProfileImage));
                }
                break;
            case MSG_SUCCESS:
                containerAlert.setBackgroundColor(getActivity().getResources().getColor(R.color.blueLight));
                alert.setText(getActivity().getResources().getString(R.string.incrusteDone));
                break;
        }

        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null){
                    killFragment.removeFragment();

                }
            }
        });

        return view;
    }

}
