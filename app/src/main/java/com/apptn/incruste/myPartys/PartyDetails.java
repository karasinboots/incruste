package com.apptn.incruste.myPartys;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apptn.incruste.General.FootMenu;
import com.apptn.incruste.GolbalUtilities.Utiles;
import com.apptn.incruste.Messages.Messages;
import com.apptn.incruste.Models.Themes;
import com.apptn.incruste.Models.User;
import com.apptn.incruste.ProfilePac.Profile;
import com.apptn.incruste.R;
import com.bumptech.glide.Glide;
import com.facebook.share.model.ShareOpenGraphAction;
import com.facebook.share.model.ShareOpenGraphContent;
import com.facebook.share.model.ShareOpenGraphObject;
import com.facebook.share.widget.ShareDialog;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.hannesdorfmann.swipeback.Position;
import com.hannesdorfmann.swipeback.SwipeBack;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.Nullable;

import static com.apptn.incruste.GolbalUtilities.Utiles.DOCUMENT_PARTY;
import static com.apptn.incruste.GolbalUtilities.Utiles.DOCUMENT_PROFILE;

//import android.support.v7.app.AlertDialog;

public class PartyDetails extends AppCompatActivity implements ResultIncrust, KillFragment {
    RecyclerView gridExtra;
    RelativeLayout container;
    ExtraAdapterClass adapterClass;
    JSONObject user;
    AdapterIncruster adapterIncruster;
    JSONObject extraInfo = null;
    View footer;
    ImageView imageShare;
    AlertDialog.Builder ad;
    LinearLayout incruster;
    JSONArray incrusters;
    FrameLayout msgAlertLayout;
    boolean canIncruste = true;
    JSONArray jsonExtra = null;
    int position = -1;
    FrameLayout containerAlerNotification;
    public static boolean active = false;
    TextView msgAlert;
    BroadcastReceiver receiver;
    PartyDetails activity;
    boolean isNotified = false;
    ImageView closeDialog;
    final int REQUEST = 1;
    final int ACCEPT = 2;
    JSONObject notificationData;
    String type = "";

    @Override
    protected void onStart() {
        super.onStart();
        active = true;

        LocalBroadcastManager.getInstance(PartyDetails.this).registerReceiver((receiver),
                new IntentFilter("notification"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        active = false;
        LocalBroadcastManager.getInstance(PartyDetails.this).unregisterReceiver(receiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    notificationData = new JSONObject(intent.getExtras().getString("data"));
                    if (intent.getExtras().getString("type").equals("request"))
                        type = "request";
                    else if (intent.getExtras().getString("type").equals("accept"))
                        type = "accept";
                    triggerNotification(intent.getExtras().getInt("Key"), notificationData);
                    isNotified = true;
                } catch (JSONException | NullPointerException e) {
                    e.printStackTrace();
                }
            }
        };

        firebaseFirestore = FirebaseFirestore.getInstance();
        SwipeBack.attach(this, Position.LEFT)
                .setContentView(R.layout.activity_party_details)
                .setSwipeBackView(R.layout.swipeback_default);
        containerAlerNotification = findViewById(R.id.containerAlert);
        containerAlerNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNotified) {
                    Intent intent = new Intent(PartyDetails.this, Messages.class);

                    intent.putExtra("cameFrom", "notification");
                    intent.putExtra("type", type);
                    try {

                        intent.putExtra("id", notificationData.getInt("demande_id"));
//                        if (notificationData.has("incruste_id"))
//                        intent.putExtra("id",notificationData.getString("incruste_id"));
//                        else
//                            intent.putExtra("id",notificationData.getString("demande_id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    startActivity(intent);
                }
            }
        });
        msgAlert = findViewById(R.id.msgAlert);
        activity = new PartyDetails();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        try {
            user = new JSONObject(preferences.getString("user", "{}"));
        } catch (JSONException jsx) {
            jsx.printStackTrace();
        }

        init();
        // ATTENTION: This was auto-generated to handle app links.
//        Intent appLinkIntent = getIntent();
//        String appLinkAction = appLinkIntent.getAction();
//        Uri appLinkData = appLinkIntent.getData();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            canIncruste = savedInstanceState.getBoolean("canIncruste");
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        outState.putBoolean("canIncruste", canIncruste);
        super.onSaveInstanceState(outState, outPersistentState);

    }

    FirebaseFirestore firebaseFirestore;

    private void getPartyTheme() {

        Themes theme = new Themes();
        theme.setId(1);
        theme.setNom("Alcool et soft");
        theme.setDescription("https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fthemes%2F3e4c2596e052d3d1d5976ba18545d1cb.jpg?alt=media&token=77fce531-4608-42bc-8c71-b39d10fd2359");
        themes.add(0, theme);
        Themes theme1 = new Themes();
        theme1.setId(2);
        theme1.setNom("Jeux");
        theme1.setDescription("https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fthemes%2F0bd89cdaaeac33d3bdd93cfd3c481265.jpg?alt=media&token=6e3ea51c-c051-4533-9c85-82826d2fa9af");
        themes.add(1, theme1);
        Themes theme2 = new Themes();
        theme2.setId(3);
        theme2.setNom("Nourriture");
        theme2.setDescription("https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fthemes%2Fab45762b9121e73116c5c52eb8538bef.jpg?alt=media&token=187dc118-fced-4ab6-998d-7f0cfdd2dae0");
        themes.add(2, theme2);
        Themes theme3 = new Themes();
        theme3.setId(4);
        theme3.setNom("Autres");
        theme3.setDescription("https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fthemes%2F38b39862ede38cb6a4ffc53b9f97afb9.jpg?alt=media&token=904b47a3-e06d-4b68-b5c8-1dc1ae146741");
        themes.add(3, theme3);
    }

    final List<Themes> themes = new ArrayList<>();

    @SuppressLint("ResourceAsColor")
    public void init() {
        closeDialog = findViewById(R.id.closeDialog);
        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                containerAlerNotification.setVisibility(View.GONE);
                msgAlert.setText("");
            }
        });
        imageShare = findViewById(R.id.imageShare);
        imageShare.bringToFront();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        final ImageView favsIcns = findViewById(R.id.favsIcns);
        favsIcns.bringToFront();
        favsIcns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONArray arrayFav = Utiles.getFavoriteFromPrefrences(PartyDetails.this);
                try {
                    if (arrayFav.length() > 0) {

                        boolean duplicated = false;
                        for (int i = 0; i < arrayFav.length(); i++) {

                            if (extraInfo.getString("id").equals(arrayFav.get(i))) {
                                Utiles.deleteFromSharedPrefrences(PartyDetails.this, extraInfo.getString("id"));
                                favsIcns.setImageDrawable(ContextCompat.getDrawable(PartyDetails.this, R.drawable.like_white));
                                duplicated = true;
                            }
                        }

                        if (!duplicated) {
                            favsIcns.setImageDrawable(ContextCompat.getDrawable(PartyDetails.this, R.drawable.like_fill));
                            Utiles.addFavoriteToPrefrences(PartyDetails.this, extraInfo.getString("id"));
                        }


                    } else {
                        favsIcns.setImageDrawable(ContextCompat.getDrawable(PartyDetails.this, R.drawable.like_fill));
                        Utiles.addFavoriteToPrefrences(PartyDetails.this, extraInfo.getString("id"));
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        try {

            if (getIntent().getExtras().getString("cameFrom").equals("partys")) {
                extraInfo = new JSONObject(getIntent().getExtras().getString("extraInfo"));
                if (extraInfo.has("incruster"))
                    incrusters = extraInfo.getJSONArray("incruster");
            } else if (getIntent().getExtras().getString("cameFrom").equals("myParty")) {
                position = getIntent().getExtras().getInt("position");
                extraInfo = new JSONObject(getIntent().getExtras().getString("extraInfo"));
                if (extraInfo.has("incruster"))
                    incrusters = extraInfo.getJSONArray("incruster");
            }

            try {
                jsonExtra = new JSONArray(preferences.getString("arrayExtra", ""));

            } catch (Exception e) {
                e.printStackTrace();
            }

            if(jsonExtra == null){

                getPartyTheme();
                jsonExtra = new JSONArray();


                for(int i = 0; i < themes.size(); i++){

                    jsonExtra.put(themes.get(i));

                }

//                jsonExtra = ;
            }

            JSONArray arrayFavs = null;

            try {
                arrayFavs = Utiles.getFavoriteFromPrefrences(PartyDetails.this);

            } catch (Exception e){
                e.printStackTrace();
            }

            if(arrayFavs!= null) {
                for (int i = 0; i < arrayFavs.length(); i++) {
                    if (extraInfo.getString("id").equals(arrayFavs.getString(i)))
                        favsIcns.setImageDrawable(ContextCompat.getDrawable(PartyDetails.this, R.drawable.like_fill));
                }
            }


        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
        }

        adapterClass = new ExtraAdapterClass(PartyDetails.this);
        msgAlertLayout = findViewById(R.id.containerMsg);
        footer = findViewById(R.id.detailsPartyMenu);
        container = findViewById(R.id.containerDetails);
        Utiles.showMessageLog("extras", "" + getIntent().getExtras().getString("extraInfo"));
        ImageView arrowBAck = findViewById(R.id.backToolbar);
        TextView partyName = findViewById(R.id.partyNameToolbar);
        final ImageView imageParty = findViewById(R.id.imagePartyProx);
        View gradient = findViewById(R.id.gradient);
        gradient.bringToFront();
        LinearLayout linearHeader = findViewById(R.id.linearHeader);
        FootMenu menu = FootMenu.newInstance(1);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.detailsPartyMenu, menu);
        ft.commit();


        linearHeader.bringToFront();
        ImageView abmImg = findViewById(R.id.abmImg);
        TextView dateSoiree = findViewById(R.id.dateSoiree);

        Typeface neutra = Typeface.createFromAsset(getAssets(), "Neutraface2Text-Book.otf");
        Typeface neutraBold = Typeface.createFromAsset(getAssets(), "Neutra Text Bold Alt.otf");
        dateSoiree.setTypeface(neutra);
        TextView hourSoiree = findViewById(R.id.hourSoiree);
        hourSoiree.setTypeface(neutraBold);
        TextView placePublic = findViewById(R.id.placePublic);
        final TextView nameCreator = findViewById(R.id.nameCreator);
        final TextView ageCreator = findViewById(R.id.ageCreator);
        TextView descriptionParty = findViewById(R.id.descriptionParty);
        incruster = findViewById(R.id.incruster);

        boolean isIncrust = false;

        try {
            if (incrusters != null && incrusters.length() > 0)
                for (int i = 0; i < incrusters.length(); i++) {
                    if (user.getString("id").equals(incrusters.getJSONObject(i).getString("user"))) {
                        isIncrust = true;
                        break;
                    }
                }
            if (isMyParty()) {
                incruster.setVisibility(View.VISIBLE);
                incruster.setBackground(ContextCompat.getDrawable(PartyDetails.this, R.drawable.shape_fill_red));
                ((TextView) findViewById(R.id.textIncruster)).setText(R.string.delete_party);
                incruster.bringToFront();

            } else if (extraInfo.has("incruster") && extraInfo.getJSONArray("incruster").length() > 0) {
                Log.e("incruster", "not null and > 0 ");
                if (extraInfo.getInt("remain") == extraInfo.getInt("nb_places")) {
                    incruster.setBackground(ContextCompat.getDrawable(PartyDetails.this, R.drawable.shape_fill_extra));
                    ((TextView) findViewById(R.id.textIncruster)).setText(getString(R.string.finished));
                    incruster.setVisibility(View.VISIBLE);
                    incruster.bringToFront();
                    canIncruste = false;
                } else if (isIncrust) {
                    incruster.setBackground(ContextCompat.getDrawable(PartyDetails.this, R.drawable.shape_fill_extra));
                    ((TextView) findViewById(R.id.textIncruster)).setText(getString(R.string.alreadyIn));
                    incruster.setVisibility(View.VISIBLE);
                    incruster.bringToFront();
                    canIncruste = false;
                } else {
                    incruster.setVisibility(View.VISIBLE);
                    incruster.bringToFront();
                }

            } else {
                incruster.setVisibility(View.VISIBLE);
                incruster.bringToFront();
            }
        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
        }
        incruster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (isMyParty()) {


                        ad = new AlertDialog.Builder(PartyDetails.this);
                        ad.setMessage("Êtes-vous sûr de vouloir supprimer la partie?");
                        ad.setPositiveButton("oui", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                final FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();

                                try {
                                    firebaseFirestore.collection(DOCUMENT_PARTY)
                                            .document(extraInfo.get("id").toString()).delete();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                startActivity(new Intent(PartyDetails.this, MyPartys.class));

                            }
                        });

                        ad.setNegativeButton("annulation", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        ad.show();




                    } else {
                        if (canIncruste) {
                            if (user.getString("avatar").equals("avatar.png")) {
                                MsgAlertFragment fragment = MsgAlertFragment.newInstance(1);
                                FragmentTransaction ft = getSupportFragmentManager().beginTransaction()
                                        .add(R.id.containerMsg, fragment, "alert");
                                ft.commit();
                                msgAlertLayout.bringToFront();
                            } else {
                                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                                DetailsFragment subFrag = DetailsFragment.newInstance(extraInfo, 0, "incruste");
                                transaction.add(R.id.containerPartyDetail, subFrag);
                                transaction.addToBackStack("back");
                                footer.setVisibility(View.GONE);
                                transaction.commit();
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        gridExtra = findViewById(R.id.gridExtraPartyDetail);
        final ImageView imgCreator = findViewById(R.id.imgCreator);
        RecyclerView recyclerView = findViewById(R.id.recyclerIncruster);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        LinearLayout infoCreator = findViewById(R.id.infoCreator);
        infoCreator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isMyParty()) {
                    Log.e("isMyparty", isMyParty() + "");
                    Intent intent = new Intent(PartyDetails.this, Profile.class);
                    intent.putExtra("cameFrom", "selfProfile");
                    intent.putExtra("permission", "read");
                    startActivity(intent);
                } else {
                    if (position == 0) {
                        Log.e("isMyparty", isMyParty() + "");
                        Intent intent = new Intent(PartyDetails.this, Profile.class);
                        intent.putExtra("cameFrom", "checkIncruster");
                        intent.putExtra("party", extraInfo.toString());
                        intent.putExtra("jsonData", extraInfo.toString());
                        intent.putExtra("permission", "read");
                        startActivity(intent);
                    } else if (position == 1) {
                        Log.e("isMyparty", isMyParty() + "");
                        Intent intent = new Intent(PartyDetails.this, Profile.class);
                        intent.putExtra("cameFrom", "checkIncruster");
                        intent.putExtra("party", extraInfo.toString());
                        intent.putExtra("permission", "write");
                        intent.putExtra("jsonData", extraInfo.toString());
                        startActivity(intent);

                    } else {
                        Log.e("isMyparty", isMyParty() + "");
                        Intent intent = new Intent(PartyDetails.this, Profile.class);
                        intent.putExtra("cameFrom", "checkIncruster");
                        intent.putExtra("party", extraInfo.toString());
                        intent.putExtra("permission", "write");
                        intent.putExtra("jsonData", extraInfo.toString());
                        startActivity(intent);
                    }
                }

            }
        });

        adapterIncruster = new AdapterIncruster(this);

        if (getIntent().getExtras() != null) {

            try {

                partyName.setText("#" + StringEscapeUtils.unescapeJava(extraInfo.getString("name")));
                abmImg.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
                gradient.setBackground(Utiles.getColor());
                dateSoiree.setText(Utiles.getTimeAndDate(extraInfo.getString("date_soiree")).get(0).toUpperCase());
                hourSoiree.setText(Utiles.getTimeAndDate(extraInfo.getString("date_soiree")).get(1));
                placePublic.setText(extraInfo.getString("adr_public").substring(0, 1).toUpperCase() + extraInfo.getString("adr_public").substring(1));

                //set owner's data
                FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
                firebaseFirestore.collection(DOCUMENT_PROFILE)
                        .document(new JSONObject(getIntent().getExtras().getString("extraInfo"))
                                .getString("user"))
                        .addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
                            @Override
                            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                                User user = documentSnapshot.toObject(User.class);
                                nameCreator.setText(user.getNom() + " " + user.getPrenom().toUpperCase().charAt(0) + ".");
                                if (user.getAvatar().equals("facebook"))
                                    Glide.with(getApplicationContext()).load("http://graph.facebook.com/" + user.getFacebookId() + "/picture?type=normal").into(imgCreator);
                                else
                                    Glide.with(getApplicationContext()).load(user.getAvatar()).into(imgCreator);
                                String string = user.getDateNaissance();
                                DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.FRANCE);
                                Date date = null;
                                try {
                                    date = format.parse(string);
                                } catch (ParseException e1) {
                                    e1.printStackTrace();
                                }
                                DateFormat ff = new SimpleDateFormat("dd-MM-yyyy", Locale.FRANCE);
                                String new_sstr = ff.format(date);
                                ageCreator.setText(String.valueOf(Utiles.getAge(new_sstr)) + " ans");
                            }
                        });
                descriptionParty.setText(StringEscapeUtils.unescapeJava(extraInfo.getString("description")));

                recyclerView.setAdapter(adapterIncruster);
                if (extraInfo.has("incruster"))
                    adapterIncruster.fetchData(extraInfo.getJSONArray("incruster"));
                Glide.with(this).load(extraInfo.getString("photo")).into(imageParty);

                //I know that a bull shit
                List<String> tempAmb = new ArrayList<>();
                tempAmb.add(0, "https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fambiance%2F8eba541ca1d3e38032988e9c36c65ef2.jpg?alt=media&token=5fdd69c5-be52-486b-a3cd-34a6f6ca9ed5");
                tempAmb.add(1,"https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fambiance%2F4687bf6a1cd6a7babfce3a4ff6a7d155.jpg?alt=media&token=766c62fd-9485-4a2d-937d-51e62eb220e2");
                tempAmb.add(2,"https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fambiance%2F5777c4c0c59583cc32098b8ddc726b25.jpg?alt=media&token=d60316bd-2cea-4bd1-84ca-57eaa5611a61");
                tempAmb.add(3,"https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fambiance%2Fd57403b30d602547920a1fd16be7c0cf.jpg?alt=media&token=b0b9bb00-ab26-4630-bca3-2210742e4371");
                tempAmb.add(4,"https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fambiance%2Fd4f883eff93ba567fbe039c63b0e650e.jpg?alt=media&token=4b925fcd-8440-4f72-b3c9-5d8b4b45a912");
                tempAmb.add(5,"https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fambiance%2F6b8e18bbd41161986373600cb5b89714.jpg?alt=media&token=d546396f-9302-4b79-9343-326c638c9bbd");
                tempAmb.add(6,"https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fambiance%2Fsofa.png?alt=media&token=eafdc994-7ea2-4d6c-a800-2023bca01f86");


                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
//                String ambianceJson = sharedPreferences.getString("JsonAmb", "");
//                Glide.with(this).load(new JSONArray(ambianceJson)
//                        .getJSONObject(Integer.parseInt(extraInfo.getString("ambiance")) - 1).getString("description"))
//                        .into(abmImg);

                JSONArray ambiancesPref = null;
                try {
                    ambiancesPref = new JSONArray(sharedPreferences.getString("JsonAmb", ""));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (ambiancesPref != null) {
                    Glide.with(this).load(ambiancesPref.getJSONObject(Integer.parseInt(extraInfo.getString("ambiance")) - 1)
                            .getString("description")).into(abmImg);
                } else {
                    Glide.with(this).load(tempAmb.get(Integer.parseInt(extraInfo.getString("ambiance")) - 1))
                            .into(abmImg);
                }

            } catch (JSONException js) {
                js.printStackTrace();
            }
        }

        RecyclerView.LayoutManager gridManager = new GridLayoutManager(PartyDetails.this, 2);
        gridExtra.setLayoutManager(gridManager);
        gridExtra.setAdapter(adapterClass);
        try {
            adapterClass.fetchData(jsonExtra, extraInfo.getJSONArray("extra"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        arrowBAck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void triggerNotification(int witch, JSONObject data) {

        containerAlerNotification.setBackgroundColor(ContextCompat.getColor(PartyDetails.this, R.color.blueLight));
        containerAlerNotification.setVisibility(View.VISIBLE);
        containerAlerNotification.bringToFront();
        String party_name = null;
        try {
            party_name = StringEscapeUtils.unescapeJava(data.getString("soiree_name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        switch (witch) {
            case REQUEST:
                try {
                    String name = data.getString("incruster_name") + " " + data.getString("incruster_prenom");
                    String nameHtml = "<font color=#000>" + name.substring(0, 1).toUpperCase() + name.substring(1) + "</font>";
                    String partyName = StringEscapeUtils.unescapeJava(party_name) + ".";
                    String partyNameHtml = "<font color=#000>" + "#" + partyName.substring(0, 1).toUpperCase() + partyName.substring(1) + "</font>";
                    msgAlert.setText(Html.fromHtml(nameHtml + " " + getString(R.string.wantsSub) + " <br />" + partyNameHtml + " " + getString(R.string.verifyMessage)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;
            case ACCEPT:
                String nameParty = "#" + party_name.substring(0, 1).toUpperCase() + party_name.substring(1) + ".";
                msgAlert.setText(Html.fromHtml(getString(R.string.yourRequestAccepted) + " <br />" + nameParty + " " + getString(R.string.verifyMessage)));
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.swipeback_stack_to_front,
                R.anim.swipeback_stack_right_out);
        if (footer.getVisibility() == View.GONE)
            footer.setVisibility(View.VISIBLE);
    }

    @Override
    public void overridePendingTransition(int enterAnim, int exitAnim) {
        super.overridePendingTransition(enterAnim, exitAnim);
    }

    @Override
    public void sendBackResult(int resultCode) {
        if (resultCode == 1) {

            MsgAlertFragment fragment = MsgAlertFragment.newInstance(2);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction()
                    .add(R.id.containerMsg, fragment, "alert");
            ft.commit();
            msgAlertLayout.bringToFront();

            incruster.setBackground(ContextCompat.getDrawable(PartyDetails.this, R.drawable.shape_fill_extra));
            ((TextView) findViewById(R.id.textIncruster)).setText("Demande envoyée");
            incruster.setVisibility(View.VISIBLE);
            incruster.bringToFront();
            canIncruste = false;

        }
    }

    @Override
    public void removeFragment() {
        killFragment();
    }


    private class ExtraAdapterClass extends RecyclerView.Adapter<ExtraAdapterClass.ViewHolder> {

        JSONArray arrayExtra;
        Context ctx;
        JSONArray userExtra;

        public ExtraAdapterClass(Context context) {
            this.ctx = context;
            this.arrayExtra = new JSONArray();
            this.userExtra = new JSONArray();
        }

        public void fetchData(JSONArray array, JSONArray userUx) {
            this.arrayExtra = array;
            this.userExtra = userUx;
            Utiles.showMessageLog("aaaaaaa", arrayExtra.toString());
            notifyDataSetChanged();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.inflation_grid_extra, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            try {

                int idSearch;
                if (themes.size() > 0) {
                    Glide.with(ctx).load(themes.get(position).getDescription()).into(holder.imageExtra);
                    holder.textView.setText(themes.get(position).getNom());
                    idSearch = themes.get(position).getId();
                } else {
                    Glide.with(ctx).load(arrayExtra.getJSONObject(position).getString("description")).into(holder.imageExtra);
                    holder.textView.setText(arrayExtra.getJSONObject(position).getString("name"));
                    idSearch = arrayExtra.getJSONObject(position).getInt("id");
                }


                for (int k = 0; k < userExtra.length(); k++) {
                    Log.e("userExtra", userExtra.getJSONObject(k).toString());
                    if (idSearch == userExtra.getJSONObject(k).getInt("type")) {
                        holder.linearLayout.setBackground(ContextCompat.getDrawable(ctx, R.drawable.blue_light_btn));
                        holder.imageExtra.setColorFilter(Color.WHITE);
                        holder.textView.setTextColor(Color.WHITE);
                        holder.moreInfo.setTextColor(Color.WHITE);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return arrayExtra.length();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            ImageView imageExtra;
            TextView textView, moreInfo;
            LinearLayout linearLayout;

            public ViewHolder(View view) {
                super(view);
                imageExtra = view.findViewById(R.id.imageExtra);
                textView = view.findViewById(R.id.nameExtra);
                moreInfo = view.findViewById(R.id.moreInfo);
                linearLayout = view.findViewById(R.id.containerTag);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        for (int i = 0; i < userExtra.length(); i++) {
                            try {
                                if (arrayExtra.getJSONObject(getAdapterPosition()).getInt("id") == userExtra.getJSONObject(i).getInt("type")) {
                                    android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                                    DetailsFragment fragment = DetailsFragment.showDescInstance(userExtra.getJSONObject(i), "grid");
                                    transaction.add(R.id.containerPartyDetail, fragment);
                                    transaction.addToBackStack("back");
                                    footer.setVisibility(View.GONE);
                                    transaction.commit();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                });
            }


        }
    }

    private boolean isMyParty() {

        boolean isMine = false;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(PartyDetails.this);
        try {
            JSONObject user = new JSONObject(preferences.getString("user", "{}"));
            JSONObject data = new JSONObject(getIntent().getExtras().getString("extraInfo"));
            isMine = user.getString("id").equals(data.getString("user"));
        } catch (JSONException jsx) {
            jsx.printStackTrace();
        }

        return isMine;
    }

    public void doNothing(View view) {
    }

    private class AdapterIncruster extends RecyclerView.Adapter<AdapterIncruster.ViewHolder> {

        Context ctx;
        JSONArray array;

        AdapterIncruster(Context context) {
            this.ctx = context;
            this.array = new JSONArray();
        }

        void fetchData(JSONArray arrayIncrusters) {
            Log.d("c", arrayIncrusters.toString());
            JSONArray jsonArray = new JSONArray();
            for (int i = 1; i < arrayIncrusters.length(); i++) {
                try {
                    JSONObject data = new JSONObject(getIntent().getExtras().getString("extraInfo"));
                    if (arrayIncrusters.getJSONObject(i).getInt("is_accepted") == 1&&
                            !arrayIncrusters.getJSONObject(i).getString("user").equals(data.getString("user")) )
                        jsonArray.put(arrayIncrusters.getJSONObject(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            this.array = jsonArray;
            notifyDataSetChanged();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = getLayoutInflater().inflate(R.layout.inflation_incruters_details, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {

            try {
                FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
                firebaseFirestore.collection(DOCUMENT_PROFILE).document(array.getJSONObject(position).getString("user"))
                        .addSnapshotListener(activity, new EventListener<DocumentSnapshot>() {
                            @Override
                            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                                User user = documentSnapshot.toObject(User.class);
                                String nom = user.getNom();
                                String prenom = user.getPrenom();
                                String fullname = nom.substring(0, 1).toUpperCase() + nom.substring(1) + " " + prenom.substring(0, 1).toUpperCase() + ".";
                                holder.nameIncruster.setText(fullname);
                                if (user.getAvatar().equals("facebook"))
                                    Glide.with(ctx).load("http://graph.facebook.com/" + user.getFacebookId() + "/picture?type=normal").into(holder.imgCreator);
                                else
                                    Glide.with(ctx).load(user.getAvatar()).into(holder.imgCreator);

                                String string = user.getDateNaissance();
                                DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.FRANCE);
                                Date date = null;
                                try {
                                    date = format.parse(string);
                                } catch (ParseException e1) {
                                    e1.printStackTrace();
                                }
                                DateFormat ff = new SimpleDateFormat("dd-MM-yyyy", Locale.FRANCE);
                                String new_sstr = ff.format(date);
                                holder.ageCreator.setText(String.valueOf(Utiles.getAge(new_sstr)) + " ans");
                                // holder.ageCreator.setText(String.valueOf(Utiles.getAge()));
                            }
                        });


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return array.length();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            ImageView imgCreator;
            TextView nameIncruster, ageCreator;

            public ViewHolder(View itemView) {
                super(itemView);

                imgCreator = itemView.findViewById(R.id.imgCreator);
                nameIncruster = itemView.findViewById(R.id.nameIncruster);
                ageCreator = itemView.findViewById(R.id.ageCreator);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            //    Utiles.showMessageLog("needThis", array.getJSONObject(position).toString());
                            if (isMyParty()) {
                                Log.e("clicked", "here");
                                Intent intent = new Intent(PartyDetails.this, Profile.class);
                                intent.putExtra("cameFrom", "checkIncruster");
                                intent.putExtra("permission", "write");
                                intent.putExtra("party", extraInfo.toString());
                                intent.putExtra("jsonData", array.getJSONObject(getAdapterPosition()).toString());
                                startActivity(intent);
                            } else {

                                Intent intent = new Intent(PartyDetails.this, Profile.class);
                                intent.putExtra("cameFrom", "checkIncruster");
                                intent.putExtra("permission", "write");
                                intent.putExtra("party", extraInfo.toString());
                                intent.putExtra("jsonData", array.getJSONObject(getAdapterPosition()).toString());
                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    }

    private void killFragment() {
        MsgAlertFragment fragment = (MsgAlertFragment) getSupportFragmentManager().findFragmentByTag("alert");
        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
    }

    public void shareFacebook(View view) {

        ShareOpenGraphObject object = new ShareOpenGraphObject.Builder()
                .putString("og:type", "books.book")
                .putString("og:title", "A Game of Thrones")
                .putString("og:description", "In the frozen wastes to the north of Winterfell, sinister and supernatural forces are mustering.")
                .putString("books:isbn", "0-553-57340-3")
                .build();

        ShareOpenGraphAction action = new ShareOpenGraphAction.Builder()
                .setActionType("og.follows")
                .putObject("book", object)
                .build();

        ShareOpenGraphContent content = new ShareOpenGraphContent.Builder()
                .setPreviewPropertyName("book")
                .setAction(action)
                .setContentUrl(Uri.parse("http://app.tn"))
                .build();

        ShareDialog.show(PartyDetails.this, content);


    }
}
