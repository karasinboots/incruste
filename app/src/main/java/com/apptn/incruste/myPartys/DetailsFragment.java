package com.apptn.incruste.myPartys;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apptn.incruste.FireBase.FirebaseMessageSender;
import com.apptn.incruste.GolbalUtilities.Utiles;
import com.apptn.incruste.Models.Extra;
import com.apptn.incruste.Models.FirebaseMessage;
import com.apptn.incruste.Models.Incruster;
import com.apptn.incruste.Models.Message;
import com.apptn.incruste.Models.NotifyData;
import com.apptn.incruste.Models.Party;
import com.apptn.incruste.Models.Type;
import com.apptn.incruste.Models.User;
import com.apptn.incruste.R;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.apptn.incruste.GolbalUtilities.Utiles.COLLECTION_PUBLIC;
import static com.apptn.incruste.GolbalUtilities.Utiles.DOCUMENT_PARTY;
import static com.apptn.incruste.GolbalUtilities.Utiles.DOCUMENT_PROFILE;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailsFragment extends Fragment {

    SharedPreferences sharedPreferences;
    JSONObject dataRecieved, userObject;
    String timeToGo = "0";
    RelativeLayout progSubs;
    ResultIncrust toActivity;
    Activity activity;
    TextView ten, tenHalf, eleven, elevenHalf, midnight;
    HashMap<Integer, Boolean> selectedExtra;
    TextView titleExtra, descExtra;
    List<String> times = new ArrayList<>();
    private Context context;

    public DetailsFragment() {
        // Required empty public constructor
    }


    /*
    RcyclerView is for the detail in the extra requested buy the creator he choose some.// wich turns in the end and i dont need it so i changed it to simple text
    GridView is for the extra selected buy the creator to allow the incruster to choose between if
    there is none it wll be "au choix"
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            toActivity = (ResultIncrust) context;
            this.context = context;
        } catch (ClassCastException cce) {
            cce.printStackTrace();
        }
    }

    @SuppressLint("UseSparseArrays")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        selectedExtra = new HashMap<>();
        try {
            userObject = new JSONObject(sharedPreferences.getString("user", "{}"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void createTimeOptionForPary(String myTime) {

        times.add(myTime);
        for (int i = 0; i < 4; i++) {
            SimpleDateFormat df = new SimpleDateFormat("HH:mm");
            Date d = null;
            try {
                d = df.parse(myTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(d);
            cal.add(Calendar.MINUTE, 30);
            String newTime = df.format(cal.getTime());
            Utiles.showMessageLog("newAddedTime", newTime);
            myTime = newTime;
            times.add(newTime);
        }

    }

    public static DetailsFragment newInstance(JSONObject extra, int position, String cameFrom) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("extra", extra.toString());
        bundle.putString("cameFrom", cameFrom);
        bundle.putInt("position", position);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static DetailsFragment showDescInstance(JSONObject object, String cameFrom) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("JsonObject", object.toString());
        bundle.putString("cameFrom", cameFrom);
        fragment.setArguments(bundle);
        return fragment;

    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        titleExtra = view.findViewById(R.id.extraTitle);
        descExtra = view.findViewById(R.id.descExtra);
        progSubs = view.findViewById(R.id.progSubs);
        progSubs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        RelativeLayout holderTwo = view.findViewById(R.id.holderTwo);
        final TextView descExtra = view.findViewById(R.id.descExtra);
        RelativeLayout holderOne = view.findViewById(R.id.holderOne);
        RelativeLayout validerDemande = view.findViewById(R.id.validerDemande);
        LinearLayout layout = view.findViewById(R.id.closeL);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        RelativeLayout headRelative = view.findViewById(R.id.headRelative);
        headRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        holderTwo.bringToFront();
        headRelative.bringToFront();
        TextView textView = view.findViewById(R.id.noDataFoundExtra);

        ImageView imgClose = view.findViewById(R.id.imgClose);
        ImageView imgClose1 = view.findViewById(R.id.imgClose1);
        ten = view.findViewById(R.id.oneH);
        tenHalf = view.findViewById(R.id.two);
        eleven = view.findViewById(R.id.threeH);
        elevenHalf = view.findViewById(R.id.four);
        midnight = view.findViewById(R.id.five);


        ten.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                timeToGo = times.get(0);
                changeTextColor(ten);
                return false;


            }


        });
        tenHalf.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                timeToGo = times.get(1);
                changeTextColor(tenHalf);
                return false;
            }


        });
        eleven.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                timeToGo = times.get(2);
                changeTextColor(eleven);
                return false;
            }


        });
        elevenHalf.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                timeToGo = times.get(3);
                changeTextColor(elevenHalf);
                return false;
            }


        });
        midnight.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                timeToGo = times.get(4);
                changeTextColor(midnight);
                return false;
            }


        });


        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() != null)
                    getActivity().onBackPressed();
            }
        });
        imgClose1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getActivity() != null)
                    getActivity().onBackPressed();
            }
        });

        headRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        if (getArguments().getString("cameFrom").equals("grid")) {
            holderOne.setVisibility(View.VISIBLE);
            if (holderTwo.getVisibility() == View.VISIBLE)
                holderTwo.setVisibility(View.GONE);
            FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
            try {
                firebaseFirestore.collection(COLLECTION_PUBLIC).document("themes")
                        .collection("themes").document(new JSONObject(getArguments()
                        .getString("JsonObject")).getString("type"))
                        .addSnapshotListener(getActivity(), new EventListener<DocumentSnapshot>() {
                            @Override
                            public void onEvent(@javax.annotation.Nullable DocumentSnapshot documentSnapshot, @javax.annotation.Nullable FirebaseFirestoreException e) {
                                try {
                                    Type type = documentSnapshot.toObject(Type.class);
                                    Log.e("jsonObject", getArguments().getString("JsonObject"));
                                    JSONObject object = new JSONObject(getArguments().getString("JsonObject"));
                                    String desc = StringEscapeUtils.unescapeJava(object.getString("description"));
                                    if (desc.equals(""))
                                        descExtra.setText(getString(R.string.auChoix));
                                    else
                                        descExtra.setText(desc);
                                    titleExtra.setText(type.getName());
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (getArguments().getString("cameFrom").equals("incruste")) {
            holderTwo.setVisibility(View.VISIBLE);
            if (holderOne.getVisibility() == View.VISIBLE)
                holderOne.setVisibility(View.GONE);
            GridView gridExtra = view.findViewById(R.id.gridExtra);
            try {
                dataRecieved = new JSONObject(getArguments().getString("extra"));
                Utiles.showMessageLog("dataRecieved", dataRecieved.toString());
                createTimeOptionForPary(Utiles.getTimeAndDate(dataRecieved.getString("date_soiree")).get(1));
                setTextTimeToGO();
                if (dataRecieved.getJSONArray("extra").length() == 0) {
                    textView.setVisibility(View.VISIBLE);
                    gridExtra.setVisibility(View.GONE);
                } else {
                    gridExtra.setAdapter(new ExtraAdapter(getActivity(), dataRecieved.getJSONArray("extra")));
                    textView.setVisibility(View.GONE);
                    gridExtra.setVisibility(View.VISIBLE);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            gridExtra.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Utiles.showMessageLog("extraForUser", selectedExtra.toString());
                    LinearLayout exrtaOne = view.findViewById(R.id.containerTag);
                    int id = (int) view.getTag();
                    ImageView image = view.findViewById(R.id.imageExtra);
                    TextView nameExtra = view.findViewById(R.id.nameExtra);
                    TextView plus = view.findViewById(R.id.moreInfo);
                    boolean isFound = false;
//                        JSONObject extraUser = dataRecieved.getJSONArray("extra").getJSONObject(i);
                    if (getActivity() != null) {
                        if (selectedExtra.size() > 0) {
                            for (int key : selectedExtra.keySet()) {
                                if (key == id) {
                                    isFound = true;
                                }
                            }
                            if (isFound) {
                                if (!selectedExtra.get(id)) {
                                    exrtaOne.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.blue_light_btn));
                                    image.setColorFilter(ContextCompat.getColor(getActivity(), R.color.white));
                                    nameExtra.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                                    plus.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                                    selectedExtra.put(id, true);
                                } else {
                                    exrtaOne.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.shape_fill_extra));
                                    image.setColorFilter(ContextCompat.getColor(getActivity(), android.R.color.black));
                                    nameExtra.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.black));
                                    plus.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.black));
                                    selectedExtra.put(id, false);
                                }
                            } else {
                                exrtaOne.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.blue_light_btn));
                                image.setColorFilter(ContextCompat.getColor(getActivity(), R.color.white));
                                nameExtra.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                                plus.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                                selectedExtra.put(id, true);
                            }
                        } else {
                            exrtaOne.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.blue_light_btn));
                            image.setColorFilter(ContextCompat.getColor(getActivity(), R.color.white));
                            nameExtra.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                            plus.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                            selectedExtra.put(id, true);
                        }
                    }
                }
            });
        }

        validerDemande.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                    JSONArray jsonExtra = new JSONArray(sharedPreferences.getString("arrayExtra", ""));
                    JSONArray arrayExtra = new JSONArray();
                    if (selectedExtra.size() > 0) {
                        for (Integer key : selectedExtra.keySet()) {
                            if (selectedExtra.get(key)) {
                                arrayExtra.put(jsonExtra.get(key - 1));
                            }
                        }
                    }
                    Utiles.showMessageLog("extraArray", "" + arrayExtra);
                    if (!timeToGo.equals("0")) {
                        progSubs.setVisibility(View.VISIBLE);
                        progSubs.bringToFront();
                        webServiceIncruster(arrayExtra);
                    } else {
                        Toast.makeText(getActivity(), "selectHour", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        return view;
    }

    private void changeTextColor(TextView textView) {

        if (getActivity() != null) {
            ten.setTextColor(ContextCompat.getColor(getActivity(), R.color.unSelectedTxt));
            tenHalf.setTextColor(ContextCompat.getColor(getActivity(), R.color.unSelectedTxt));
            eleven.setTextColor(ContextCompat.getColor(getActivity(), R.color.unSelectedTxt));
            elevenHalf.setTextColor(ContextCompat.getColor(getActivity(), R.color.unSelectedTxt));
            midnight.setTextColor(ContextCompat.getColor(getActivity(), R.color.unSelectedTxt));
            textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
        }
    }

    private void setTextTimeToGO() {

        ten.setText(times.get(0));
        tenHalf.setText(times.get(1));
        eleven.setText(times.get(2));
        elevenHalf.setText(times.get(3));
        midnight.setText(times.get(4));
    }

    private class RecyclerDetails extends RecyclerView.Adapter<RecyclerDetails.myViewHolder> {

        JSONArray data;
        Context ctx;


        public RecyclerDetails(Context context, JSONArray object) {
            this.data = object;
            this.ctx = context;
        }

        @Override
        public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.inflation_details_fragment, parent, false);
            return new myViewHolder(view);
        }

        @Override
        public void onBindViewHolder(myViewHolder holder, int position) {


            try {
                holder.typeName.setText(data.getJSONObject(getArguments().getInt("position")).getJSONObject("type").getString("name"));
                if (!data.getJSONObject(getArguments().getInt("position")).optString("description").equals(""))
                    holder.typeDetails.setText(data.getJSONObject(getArguments().getInt("position")).optString("description"));
                else
                    holder.typeDetails.setText(getString(R.string.auChoix));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        @Override
        public int getItemCount() {
            return 1;
        }

        class myViewHolder extends RecyclerView.ViewHolder {

            TextView typeName, typeDetails;


            public myViewHolder(View itemView) {
                super(itemView);

                typeName = itemView.findViewById(R.id.typeName);
                typeDetails = itemView.findViewById(R.id.typeDetails);
            }
        }
    }


    private class ExtraAdapter extends BaseAdapter {
        Context ctx;

        LinearLayout linearLayout;

        JSONArray arrayExtra;

        public ExtraAdapter(Context context, JSONArray array) {
            this.ctx = context;
            this.arrayExtra = array;
            Utiles.showMessageLog("Array_Extra", array.toString());
        }

        @Override
        public int getCount() {
            return arrayExtra.length();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {

            if (view == null)
                view = getLayoutInflater().inflate(R.layout.inflation_grid_extra, viewGroup, false);
            ImageView imageExtra = view.findViewById(R.id.imageExtra);
            TextView textView = view.findViewById(R.id.nameExtra);
            linearLayout = view.findViewById(R.id.containerExtra);


            try {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
                JSONArray jsonExtra = new JSONArray(sharedPreferences.getString("arrayExtra", ""));
                Log.e("arrayExtra", arrayExtra.toString());
                view.setTag(jsonExtra.getJSONObject(i).getInt("id"));
                textView.setText(jsonExtra.getJSONObject(i).getString("name"));
                Glide.with(ctx).load(jsonExtra.getJSONObject(i).getString("description"))
                        .into(imageExtra);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return view;
        }


    }


    public void webServiceIncruster(final JSONArray extra) {
        try {
            final String id = dataRecieved.getString("id");
            final FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
            firebaseFirestore.collection(DOCUMENT_PARTY).document(id)
                    .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    final Party party = task.getResult().toObject(Party.class);
                    if (party.getIncruster() != null && party.getIncruster().size() > 0) {
                        Incruster incruster = new Incruster();
                        incruster.setUser(new Gson().fromJson(userObject.toString(), User.class).getId());
                        incruster.setTime(timeToGo);
                        List<Extra> extras = new Gson().fromJson(extra.toString(), new TypeToken<List<Extra>>() {
                        }.getType());
                        incruster.setExtras(extras);
                        incruster.setIsAccepted(0);
                        incruster.setParty(id);
                        incruster.setId(String.valueOf(System.currentTimeMillis()));
                        party.setRemain(party.getRemain() - 1);
                        party.getIncruster().add(incruster);
                    } else {
                        Incruster incruster = new Incruster();
                        incruster.setUser(new Gson().fromJson(userObject.toString(), User.class).getId());
                        incruster.setTime(timeToGo);
                        Log.e("extra", extra.toString());
                        List<Extra> extras = new Gson().fromJson(extra.toString(), new TypeToken<List<Extra>>() {
                        }.getType());
                        incruster.setExtras(extras);
                        incruster.setIsAccepted(0);
                        incruster.setParty(id);
                        incruster.setId(String.valueOf(System.currentTimeMillis()));
                        List<Incruster> incrusters = new ArrayList<>();
                        incrusters.add(incruster);
                        party.setRemain(party.getRemain() - 1);
                        party.setIncruster(incrusters);
                    }
                    firebaseFirestore.collection(DOCUMENT_PARTY).document(id)
                            .set(party).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            FirebaseMessaging firebaseMessaging = FirebaseMessaging.getInstance();
                            firebaseMessaging.subscribeToTopic(id).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    Log.e("subscribed to", id + "");
                                    firebaseFirestore.collection(DOCUMENT_PROFILE)
                                            .document(party.getUser())
                                            .get()
                                            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                    User user = task.getResult().toObject(User.class);
                                                    Message message = new Message();
                                                    message.setUserFrom(new Gson().fromJson(userObject.toString(), User.class).getId());
                                                    message.setUserTo(user.getId());
                                                    message.setIsRead(0);
                                                    message.setMtype(1);
                                                    message.setDatecreation(System.currentTimeMillis());
                                                    FirebaseMessage firebaseMessage = new FirebaseMessage(user.getToken(), new NotifyData("Request", new Gson().toJson(message)));
                                                    FirebaseMessageSender firebaseMessageSender = new FirebaseMessageSender();
                                                    firebaseMessageSender.sendPush(firebaseMessage);

                                                    /*Intent intent = new Intent(context, Messages.class);//open dialog
                                                    intent.putExtra("cameFrom","footer");
                                                    context.startActivity(intent);*/
                                                }
                                            });

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    e.printStackTrace();
                                }
                            });
                            toActivity.sendBackResult(1);
                            if (getActivity() != null)
                                getActivity().onBackPressed();
                        }
                    });
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
