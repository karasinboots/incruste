package com.apptn.incruste.myPartys;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.apptn.incruste.GolbalUtilities.Utiles;
import com.apptn.incruste.Models.User;
import com.apptn.incruste.R;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.makeramen.roundedimageview.RoundedImageView;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.apptn.incruste.GolbalUtilities.Utiles.DOCUMENT_PROFILE;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.myViewHolder> {


    private JSONArray data;

    private Context context;
    private LayoutInflater inflater;
    private int position = -1;
    private Typeface font;
    private JSONArray copyData;


    RecyclerAdapter(Context context, LayoutInflater inflater, int position) {


        this.context = context;
        this.inflater = inflater;
        this.copyData = new JSONArray();
        data = new JSONArray();
        this.position = position;
        font = Typeface.createFromAsset(context.getAssets(), "NeutraText-Demi.otf");

    }

    void fetchDataCom(JSONArray data) {
        try {
            this.copyData = new JSONArray(data.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.data = data;
        this.notifyDataSetChanged();


    }


    void filter(String name) {

        JSONArray arrayCopy = new JSONArray();
        JSONArray arrayrrigine = new JSONArray();
        try {
            arrayrrigine = new JSONArray(copyData.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (name.isEmpty()) {
            arrayCopy = copyData;
            notifyDataSetChanged();

        } else {
            name = name.toLowerCase();

            for (int i = 0; i < arrayrrigine.length(); i++) {
                try {

                    String userName = arrayrrigine.getJSONObject(i).getString("name");
                    String ville = arrayrrigine.getJSONObject(i).getString("adr_public");

                    if (userName.toLowerCase().contains(name.toLowerCase()) || ville.toLowerCase().contains(name.toLowerCase())) {
                        arrayCopy.put(arrayrrigine.getJSONObject(i));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
        try {
            this.data = new JSONArray(arrayCopy.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        notifyDataSetChanged();
    }


    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.inflation_my_partys, parent, false);
        return new myViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final myViewHolder holder, final int position) {
        JSONArray listFav = Utiles.getFavoriteFromPrefrences(context);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        JSONObject object = null;
        boolean shouldColor = false;


        holder.gradient.setBackground(Utiles.getColor());
        holder.gradient.bringToFront();
        try {

            if (listFav.length() > 0) {
                for (int i = 0; i < listFav.length(); i++) {

                    if (data.getJSONObject(position).getString("id").equals(listFav.getString(i)))
                        shouldColor = true;
                }

                if (shouldColor) {
                    holder.like.setVisibility(View.VISIBLE);
                    holder.like.setBackground(ContextCompat.getDrawable(context, R.drawable.like_fill));
                    holder.like.bringToFront();
                } else
                    holder.like.setVisibility(View.GONE);
            }


            object = new JSONObject(sharedPreferences.getString("user", ""));

            if (data.length() > 0) {
                holder.layoutPersons.bringToFront();

                Utiles.showMessageLog("daraaaa", data.toString());


                JSONArray arrayIncrusters = new JSONArray(String.valueOf(data.getJSONObject(holder.getAdapterPosition()).getJSONArray("incruster")));
                int count = 0;
                for (int i = 0; i < arrayIncrusters.length(); i++) {
                    try {
                        if (arrayIncrusters.getJSONObject(i).getInt("is_accepted") == 1) {
                            ++count;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

//                if (data.getJSONObject(position).has("remain") && data.getJSONObject(position).has("nb_places"))
//                    holder.participant.setText(String.valueOf(data.getJSONObject(position).getInt("nb_places")
//                            - data.getJSONObject(position).getInt("remain")));
//                else
                    holder.participant.setText(String.valueOf(count));

//                JSONArray themes = new JSONArray(sharedPreferences.getString("JsonAmb", ""));
////                Glide.with(context).load(themes.getJSONObject(data.getJSONObject(holder.getAdapterPosition())
////                        .getInt("ambiance") - 1).getString("description")).into(holder.ambImg);

                //      Glide.with(context).load(data.getJSONObject(position).getJSONObject("ambiance").getString("description")).into(holder.ambImg);
                FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
                firebaseFirestore.collection(DOCUMENT_PROFILE)
                        .document(data.getJSONObject(position)
                                .getString("user"))
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                User user = task.getResult().toObject(User.class);
                                String partyCrea = user.getNom().substring(0, 1).toUpperCase() +
                                        user.getNom().substring(1) + " " + user.getPrenom();
                                holder.partyCreator.setText(partyCrea);
                            }
                        });
                String partyName = data.getJSONObject(position).getString("name").substring(0, 1).toUpperCase() + data.getJSONObject(position).getString("name").substring(1);
                holder.partyName.setText("#" + StringEscapeUtils.unescapeJava(partyName));
                holder.timeParty.bringToFront();
                holder.dateParty.bringToFront();
                holder.dateParty.setText(Utiles.getTimeAndDate(data.getJSONObject(position).getString("date_soiree")).get(0));
                holder.timeParty.setText(Utiles.getTimeAndDate(data.getJSONObject(holder.getAdapterPosition()).getString("date_soiree")).get(1));
                Glide.with(context).load(data.getJSONObject(holder.getAdapterPosition()).getString("photo")
                ).into(holder.imgParty);
                String partyLoc = data.getJSONObject(holder.getAdapterPosition()).getString("adr_public").substring(0, 1).toUpperCase() + data.getJSONObject(holder.getAdapterPosition()).getString("adr_public").substring(1);
                holder.like.bringToFront();
                holder.partyLocation.setText(partyLoc);

                List<String> tempAmb = new ArrayList<>();
                tempAmb.add(0, "https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fambiance%2F8eba541ca1d3e38032988e9c36c65ef2.jpg?alt=media&token=5fdd69c5-be52-486b-a3cd-34a6f6ca9ed5");
                tempAmb.add(1, "https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fambiance%2F4687bf6a1cd6a7babfce3a4ff6a7d155.jpg?alt=media&token=766c62fd-9485-4a2d-937d-51e62eb220e2");
                tempAmb.add(2, "https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fambiance%2F5777c4c0c59583cc32098b8ddc726b25.jpg?alt=media&token=d60316bd-2cea-4bd1-84ca-57eaa5611a61");
                tempAmb.add(3, "https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fambiance%2Fd57403b30d602547920a1fd16be7c0cf.jpg?alt=media&token=b0b9bb00-ab26-4630-bca3-2210742e4371");
                tempAmb.add(4, "https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fambiance%2Fd4f883eff93ba567fbe039c63b0e650e.jpg?alt=media&token=4b925fcd-8440-4f72-b3c9-5d8b4b45a912");
                tempAmb.add(5, "https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fambiance%2F6b8e18bbd41161986373600cb5b89714.jpg?alt=media&token=d546396f-9302-4b79-9343-326c638c9bbd");
                tempAmb.add(6, "https://firebasestorage.googleapis.com/v0/b/incruste-e5715.appspot.com/o/images%2Fambiance%2Fsofa.png?alt=media&token=eafdc994-7ea2-4d6c-a800-2023bca01f86");

                JSONArray ambiancesPref = null;
                try {
                    ambiancesPref = new JSONArray(sharedPreferences.getString("JsonAmb", ""));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (ambiancesPref != null) {
                    Glide.with(context).load(ambiancesPref.getJSONObject(data.getJSONObject(holder.getAdapterPosition())
                            .getInt("ambiance") - 1).getString("description")).into(holder.ambImg);
                } else {
                    Glide.with(context).load(tempAmb.get(data.getJSONObject(holder.getAdapterPosition())
                            .getInt("ambiance") - 1)).into(holder.ambImg);
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {

        return data.length();
    }

    class myViewHolder extends RecyclerView.ViewHolder {

        TextView partyName, dateParty, timeParty, partyCreator, partyLocation, participant, by;
        ImageView ambImg, like;
        RoundedImageView imgParty, gradient;
        FrameLayout layoutPersons;
        Typeface neutraDemi = Typeface.createFromAsset(context.getAssets(), "NeutraText-Demi.otf");
        Typeface proLigh = Typeface.createFromAsset(context.getAssets(), "SourceSansPro-Light.otf");
        Typeface proDemi = Typeface.createFromAsset(context.getAssets(), "SourceSansPro-Semibold.otf");

        myViewHolder(View itemView) {
            super(itemView);
            by = itemView.findViewById(R.id.by);
            by.setTypeface(proLigh);
            layoutPersons = itemView.findViewById(R.id.layoutPersonsMy);
            participant = itemView.findViewById(R.id.participant);
            ambImg = itemView.findViewById(R.id.ambImg);
            like = itemView.findViewById(R.id.like);
            gradient = itemView.findViewById(R.id.gradientMyPartys);
            partyName = itemView.findViewById(R.id.partyName);
            partyName.setTypeface(neutraDemi);
            dateParty = itemView.findViewById(R.id.dateParty);
            dateParty.setTypeface(font);
            timeParty = itemView.findViewById(R.id.timeParty);
            timeParty.setTypeface(font);
            partyCreator = itemView.findViewById(R.id.partyCreator);
            partyCreator.setTypeface(proDemi);
            partyLocation = itemView.findViewById(R.id.partyLocation);
            partyLocation.setTypeface(proLigh);
            imgParty = itemView.findViewById(R.id.imgParty);

            like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        JSONArray favs = Utiles.getFavoriteFromPrefrences(context);
                        boolean isUnselection = false;
                        for (int i = 0; i < favs.length(); i++) {

                            if (data.getJSONObject(getAdapterPosition()).getString("id").equals(favs.getString(i))) {

                                isUnselection = true;
                            }
                        }

                        if (!isUnselection) {

                            Utiles.addFavoriteToPrefrences(context, data.getJSONObject(getAdapterPosition()).getString("id"));
                            like.setBackground(ContextCompat.getDrawable(context, R.drawable.like_fill));
                            Utiles.showMessageLog("SumFavorite", Utiles.getFavoriteFromPrefrences(context).toString());

                        } else {
                            Utiles.deleteFromSharedPrefrences(context, data.getJSONObject(getAdapterPosition()).getString("id"));
                            like.setBackground(ContextCompat.getDrawable(context, R.drawable.like_white));
                            Utiles.showMessageLog("SumFavorite", Utiles.getFavoriteFromPrefrences(context).toString());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, PartyDetails.class);

                    try {
                        intent.putExtra("extraInfo", data.getJSONObject(getAdapterPosition()).toString());
                        intent.putExtra("cameFrom", "myParty");
                        intent.putExtra("position", position);
                        context.startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    }


}

