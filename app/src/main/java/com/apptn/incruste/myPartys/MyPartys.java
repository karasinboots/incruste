package com.apptn.incruste.myPartys;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apptn.incruste.General.FootMenu;
import com.apptn.incruste.Models.Incruster;
import com.apptn.incruste.Models.Party;
import com.apptn.incruste.Models.User;
import com.apptn.incruste.R;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.apptn.incruste.GolbalUtilities.Utiles.DOCUMENT_PARTY;

public class MyPartys extends AppCompatActivity {

    public View view2, view3, view4, view5;

    SharedPreferences preferences;
    JSONObject user;
    HashMap<String, String> userData;
    double lat = 0.0;
    double lng = 0.0;
    EditText searchViewMyPartys;
    RelativeLayout containerProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_partys);

        containerProgress = findViewById(R.id.containerPorog);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        userData = new HashMap<>();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        try {
            user = new JSONObject(preferences.getString("user", "{}"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final ViewPager pager = findViewById(R.id.pager);
        pager.setAdapter(new SectionPagerAdapter(getSupportFragmentManager()));
        final SmartTabLayout tabLayout = findViewById(R.id.tabPartys);
        tabLayout.setViewPager(pager);
        tabLayout.bringToFront();

        view2 = findViewById(R.id.view2);
        view3 = findViewById(R.id.view3);
        view4 = findViewById(R.id.view4);
        view5 = findViewById(R.id.view5);
        searchViewMyPartys = findViewById(R.id.searchViewMyPartys);
        searchViewMyPartys.setCursorVisible(false);



        FootMenu menu = FootMenu.newInstance(1);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.containerMyPartys, menu);
        transaction.commit();

        searchViewMyPartys.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                //  JSONArray res = getAddress(s.toString());
                searchViewMyPartys.setCursorVisible(true);

                if (pager.getCurrentItem() == 0)
                    UpComingPartys.adapter.filter(s.toString());
                else
                    PastPartys.adapter.filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

        });

        init();

    }


    @Override
    public void onBackPressed() {

    }

    public void init() {


        /*searchViewMyPartys.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                UpComingPartys.adapter.filter(newText);
                return true;
            }
        });*/


    }


    private class SectionPagerAdapter extends FragmentPagerAdapter {

        SectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {


            Fragment fragment = new Fragment();

            if (position == 0) {
                fragment = new UpComingPartys();


            } else if (position == 1) {
                fragment = new PastPartys();
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.upComing);
                case 1:
                    return getString(R.string.passed);

            }
            return null;
        }


    }


    public static class UpComingPartys extends Fragment {

        static RecyclerAdapter adapter;

        LayoutInflater inflater;
        SwipeRefreshLayout swipeRefreshLayout;

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            inflater = getLayoutInflater();
            adapter = new RecyclerAdapter(getActivity(), inflater, 0);


        }


        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_up_coming_partys, container, false);
            RecyclerView comingRecycler = view.findViewById(R.id.comingRecycler);
            swipeRefreshLayout = view.findViewById(R.id.swipeComing);
            swipeRefreshLayout.setColorSchemeResources(R.color.pink, R.color.indigo, R.color.lime);
            comingRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
            comingRecycler.setAdapter(adapter);
            webserviceGetMyPartys();
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    swipeRefreshLayout.setRefreshing(true);
                    webserviceGetMyPartys();
                }
            });
            return view;
        }

        public void webserviceGetMyPartys() {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            final User user = new Gson().fromJson(sharedPreferences.getString("user", "{}"), User.class);
            FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
            final List<Party> myParties = new ArrayList<>();
            firebaseFirestore.collection(DOCUMENT_PARTY).addSnapshotListener(getActivity(), new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                    if (queryDocumentSnapshots != null && !queryDocumentSnapshots.isEmpty()) {
                        for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments()) {
                            Party party = snapshot.toObject(Party.class);
                            if (party.getIncruster() != null && party.getIncruster().size() > 0)
                                for (Incruster incruster : party.getIncruster()) {
                                    if (incruster.getUser().equals(user.getId())
                                            && !party.getUser().equals(user.getId())) {
                                        try {
                                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
                                            Date date = sdf.parse(party.getDateSoiree());
                                            if (date.getTime() > System.currentTimeMillis())
                                                myParties.add(party);
                                        } catch (ParseException e1) {
                                            e1.printStackTrace();
                                        }
                                    }
                                }
                            if (party.getUser().equals(user.getId())) {
                                try {
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
                                    Date date = sdf.parse(party.getDateSoiree());
                                    if (date.getTime() > System.currentTimeMillis())
                                        myParties.add(party);
                                } catch (ParseException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }
                        if (swipeRefreshLayout.isRefreshing())
                            swipeRefreshLayout.setRefreshing(false);
                        try {
                            adapter.fetchDataCom(new JSONArray(new Gson().toJson(myParties)));
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            });
        }
    }

    public static class PastPartys extends Fragment {
        LayoutInflater inflater;
        static RecyclerAdapter adapter;
        SwipeRefreshLayout swipe;

        public PastPartys() {

        }


        RecyclerView recyclerView;


        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            inflater = getLayoutInflater();
            adapter = new RecyclerAdapter(getActivity(), inflater, 1);

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_past_partys, container, false);

            swipe = view.findViewById(R.id.swipePast);
            recyclerView = view.findViewById(R.id.pastRecycler);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setAdapter(adapter);
            webserviceGetMyPartys();
            swipe.setColorSchemeResources(R.color.pink, R.color.indigo, R.color.lime);
            swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    swipe.setRefreshing(true);
                    webserviceGetMyPartys();
                }
            });
            return view;
        }

        public void webserviceGetMyPartys() {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            final User user = new Gson().fromJson(sharedPreferences.getString("user", "{}"), User.class);
            FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
            final List<Party> myParties = new ArrayList<>();
            firebaseFirestore.collection(DOCUMENT_PARTY).addSnapshotListener(getActivity(), new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                    if (queryDocumentSnapshots != null && !queryDocumentSnapshots.isEmpty()) {
                        for (DocumentSnapshot snapshot : queryDocumentSnapshots.getDocuments()) {
                            Party party = snapshot.toObject(Party.class);
                            if (party.getIncruster() != null && party.getIncruster().size() > 0)
                                for (Incruster incruster : party.getIncruster()) {
                                    if (incruster.getUser().equals(user.getId())) {
                                        try {
                                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
                                            Date date = sdf.parse(party.getDateSoiree());
                                            if (date.getTime() < System.currentTimeMillis())
                                                myParties.add(party);
                                        } catch (ParseException e1) {
                                            e1.printStackTrace();
                                        }
                                    }
                                }
                            if (party.getUser().equals(user.getId())) {
                                try {
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
                                    Date date = sdf.parse(party.getDateSoiree());
                                    if (date.getTime() < System.currentTimeMillis())
                                        myParties.add(party);
                                } catch (ParseException e1) {
                                    e1.printStackTrace();
                                }

                            }
                        }
                        if (swipe.isRefreshing())
                            swipe.setRefreshing(false);
                        try {
                            adapter.fetchDataCom(new JSONArray(new Gson().toJson(myParties)));
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            });
        }
    }

    private class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder> {

        Context context;

        JSONArray dataRecieved;

        SearchAdapter(Context ctx) {

            this.context = ctx;
            dataRecieved = new JSONArray();
        }

        public void fetchData(JSONArray data) {

            this.dataRecieved = data;
            notifyDataSetChanged();
        }


        @Override
        public SearchAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.inflation_single_row_search, parent, false);
            return new SearchAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(SearchAdapter.MyViewHolder holder, int position) {

            try {
                holder.place.setText(dataRecieved.getJSONObject(holder.getAdapterPosition()).getString("name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return dataRecieved.length();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {

            TextView place;

            public MyViewHolder(View itemView) {
                super(itemView);
                place = itemView.findViewById(R.id.place);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            lat = dataRecieved.getJSONObject(getAdapterPosition()).getDouble("lat");
                            lng = dataRecieved.getJSONObject(getAdapterPosition()).getDouble("lng");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    }

}
